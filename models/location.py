from sqlalchemy import Column, String

from .base import Base, generate_uuid


class Location(Base):
    """This is model for Location

    Args:
        Base ([Object]): Base model class

    Returns:
        [Location]: Location object
    """
    __tablename__ = 'locations'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    country = Column(String(64))
    region = Column(String(64))
