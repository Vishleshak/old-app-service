import datetime

from sqlalchemy import Column, String, Boolean, DateTime

from .base import Base, generate_uuid


class ModelMonitorFieldDetails(Base):
    """This is model for Model Monitor Field Details

    Args:
        Base ([Object]): Base model class

    Returns:
        [ModelMonitorFieldDetails]: ModelMonitorFieldDetails object
    """
    __tablename__ = 'model_monitor_field_details'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    project_id = Column(String(64))
    field_name = Column(String(64))
    threshold_value = Column(String(64))
    is_sensitive_attribute = Column(Boolean, default=False)
    threshold_check_enabled = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)
