import datetime

from sqlalchemy import Column, String, JSON, DateTime

from .base import Base, generate_uuid


class AuditToolUserSetting(Base):
    """This is model for Audit Tool User Setting

    Args:
        Base ([Object]): Base model class

    Returns:
        [AuditToolUserSetting]: AuditToolUserSetting object
    """
    __tablename__ = 'audit_tool_user_settings'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    user_id = Column(String(64))
    project_id = Column(String(64))
    metrics = Column(JSON)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)
