from sqlalchemy import Column, String

from .base import Base, generate_uuid


class ComplianceJobFramework(Base):
    """This is model for ComplianceJobFramework

    Args:
        Base ([Object]): Base model class

    Returns:
        [ComplianceJobFramework]: ComplianceJobFramework object
    """
    __tablename__ = 'compliance_job_framework'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    compliance_framework_id = Column(String(64))
    compliance_job_id = Column(String(64))
