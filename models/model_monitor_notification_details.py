import datetime
from sqlalchemy import Column, String, Boolean, DateTime

from .base import Base, generate_uuid


class ModelMonitorNotificationDetails(Base):
    """This is model for Model Monitor Notification Details

    Args:
        Base ([Object]): Base model class

    Returns:
        [ModelMonitorNotificationDetails]: ModelMonitorNotificationDetails object
    """
    __tablename__ = 'model_monitor_notification_details'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    user_id = Column(String(64))
    project_id = Column(String(64))
    by_email = Column(Boolean, default=True)
    emails = Column(String(100))
    by_slack = Column(Boolean, default=True)
    slack_api_token = Column(String(200))
    slack_channel_name = Column(String(200))
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)
