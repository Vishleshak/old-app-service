import datetime

from sqlalchemy import Column, String, DateTime

from .base import Base, generate_uuid


class ComplianceJobRuleActivity(Base):
    """This is model for ComplianceJobRuleActivity

    Args:
        Base ([Object]): Base model class

    Returns:
        [ComplianceJobRuleActivity]: ComplianceJobRuleActivity object
    """
    __tablename__ = 'compliance_job_rule_activity'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    compliance_job_rule_id = Column(String(64))
    activity = Column(String(3000))
    activity_done_on = Column(DateTime, default=datetime.datetime.utcnow)
