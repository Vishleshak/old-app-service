import datetime

from sqlalchemy import Column, String, Boolean, DateTime

from .base import Base, generate_uuid


class MitigationStrategy(Base):
    """This is model for mitigation strategy

    Args:
        Base ([type]): Base model class

    Returns:
        [MitigationStrategy]: MitigationStrategy object
    """
    __tablename__ = 'mitigation_strategy'
    __table_args__ = {'extend_existing': True}
    id = Column(String, name="id", primary_key=True, default=generate_uuid)
    name = Column(String(15))
    description = Column(String)
    is_active = Column(Boolean, default=True)
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)

    def __repr__(self):
        return "<MitigationStrategy(name='%s')>" % (self.name)
