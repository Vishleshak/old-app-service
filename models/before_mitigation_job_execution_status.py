import datetime

from sqlalchemy import Column, String, Boolean, DateTime

from .base import Base, generate_uuid


class BeforeMitigationJobExecutionStatus(Base):
    """This is model for Before Mitigation Job Execution Status

    Args:
        Base ([Object]): Base model class

    Returns:
        [BeforeMitigationJobExecutionStatus]: BeforeMitigationJobExecutionStatus object
    """
    __tablename__ = 'before_mitigation_job_execution_status'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    job_id = Column(String(64))
    execution_step = Column(String(100))
    step_execution_status = Column(Boolean, default=False)
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)
