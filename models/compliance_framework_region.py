from sqlalchemy import Column, String

from .base import Base, generate_uuid


class ComplianceFrameworkRegion(Base):
    """This is model for ComplianceFrameworkRegion

    Args:
        Base ([Object]): Base model class

    Returns:
        [ComplianceFrameworkRegion]: ComplianceFrameworkRegion object
    """
    __tablename__ = 'compliance_framework_regions'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    compliance_framework_id = Column(String(64))
    region_name = Column(String(64))
