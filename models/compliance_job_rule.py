from sqlalchemy import Column, String

from .base import Base, generate_uuid


class ComplianceJobRule(Base):
    """This is model for ComplianceJobRule

    Args:
        Base ([Object]): Base model class

    Returns:
        [ComplianceJobRule]: ComplianceJobRule object
    """
    __tablename__ = 'compliance_job_rules'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    compliance_job_id = Column(String(64))
    compliance_rule_id = Column(String(64))
    status = Column(String(64))
    assigned_to_user = Column(String(64))
