import datetime

from sqlalchemy import Column, String, Boolean, DateTime

from .base import Base, generate_uuid


class EventDetails(Base):
    """This is model for Event Details

    Args:
        Base ([Object]): Base model class

    Returns:
        [EventDetails]: EventDetails object
    """
    __tablename__ = 'event_details'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    event_name = Column(String(64))
    event_description = Column(String(64))
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)
