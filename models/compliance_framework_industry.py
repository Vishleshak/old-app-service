from sqlalchemy import Column, String

from .base import Base, generate_uuid


class ComplianceFrameworkIndustry(Base):
    """This is model for ComplianceFrameworkIndustries

    Args:
        Base ([Object]): Base model class

    Returns:
        [ComplianceFrameworkIndustries]: ComplianceFrameworkIndustries object
    """
    __tablename__ = 'compliance_framework_industry'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    compliance_framework_id = Column(String(64))
    industry_id = Column(String(64))
