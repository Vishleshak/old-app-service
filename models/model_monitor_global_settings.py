from sqlalchemy import Column, String, JSON

from .base import Base, generate_uuid


class ModelMonitorGlobalSetting(Base):
    """This is model for Model Monitor Global Setting

    Args:
        Base ([Object]): Base model class

    Returns:
        [ModelMonitorGlobalSetting]: ModelMonitorGlobalSetting object
    """
    __tablename__ = 'model_monitor_global_settings'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    use_case_id = Column(String(64))
    metrics = Column(JSON)
