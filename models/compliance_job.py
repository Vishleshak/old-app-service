import datetime

from sqlalchemy import Column, String, DateTime

from .base import Base, generate_uuid


class ComplianceJob(Base):
    """This is model for ComplianceJob

    Args:
        Base ([Object]): Base model class

    Returns:
        [ComplianceJob]: ComplianceJob object
    """
    __tablename__ = 'compliance_jobs'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    project_id = Column(String(64))
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
