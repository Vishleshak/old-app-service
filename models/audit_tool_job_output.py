import datetime

from sqlalchemy import Column, String, Boolean, DateTime, JSON

from .base import Base


class AuditToolJobOutput(Base):
    """This is model for Audit Tool Job Output

    Args:
        Base ([Object]): Base model class

    Returns:
        [AuditToolJobOutput]: AuditToolJobOutput object
    """
    __tablename__ = 'audit_tool_job_output'
    job_id = Column(String(64), primary_key=True)
    output = Column(JSON)
    execution_status = Column(Boolean, default=False)
    error = Column(String(2000))
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)
