import datetime

from sqlalchemy import Column, String, Boolean, DateTime

from .base import Base, generate_uuid


class AppFunctionality(Base):
    """This is model for UserFunctionalities

    Args:
        Base ([Object]): Base model class

    Returns:
        [Functionality]: Functionality object
    """
    __tablename__ = 'app_functionalities'
    __table_args__ = {'extend_existing': True}
    id = Column(String(54), name="id", primary_key=True, default=generate_uuid)
    name = Column(String(24))
    is_active = Column(Boolean, default=True)
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)

    def __repr__(self):
        return "<Functionality(name='%s')>" % (self.name)
