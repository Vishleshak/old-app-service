import datetime

from sqlalchemy import Column, String, Boolean, DateTime

from .base import Base, generate_uuid


class OrganizationUser(Base):
    """This is model for OrganizationUser

    Args:
        Base ([Object]): Base model class

    Returns:
        [OrganizationUser]: OrganizationUser object
    """
    __tablename__ = 'organization_users'
    id = Column(String(54), name="id", primary_key=True, default=generate_uuid)
    organization_id = Column(String(54))
    user_id = Column(String(54))
    user_role_id = Column(String(54))
    accessible_modules = Column(String(2000))
    user_rights = Column(String(2000))
    is_admin = Column(Boolean, default=False)
    is_active = Column(Boolean, default=True)
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)
