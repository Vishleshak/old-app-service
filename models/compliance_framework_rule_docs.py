from sqlalchemy import Column, String

from .base import Base, generate_uuid


class ComplianceFrameworkRuleDocument(Base):
    """This is model for ComplianceFrameworkRule

    Args:
        Base ([Object]): Base model class

    Returns:
        [ComplianceFrameworkRule]: ComplianceFrameworkRule object
    """
    __tablename__ = 'compliance_framework_rule_documents'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    compliance_framework_rule_id = Column(String(64))
    document_title = Column(String(64))
    document_description = Column(String(2000))
    document_path = Column(String(2000))
