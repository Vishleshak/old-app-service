import datetime

from sqlalchemy import Column, String, Boolean, DateTime

from .base import Base, generate_uuid


class ModelMonitorDetails(Base):
    """This is model for Model Monitor Details

    Args:
        Base ([Object]): Base model class

    Returns:
        [ModelMonitorDetails]: ModelMonitorDetails object
    """
    __tablename__ = 'model_monitor_details'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    user_id = Column(String(64))
    project_id = Column(String(64))
    use_webhook = Column(Boolean, default=False)
    use_dataset = Column(Boolean, default=False)
    sensitive_attributes_selected = Column(Boolean, default=False)
    fields_identified = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)
