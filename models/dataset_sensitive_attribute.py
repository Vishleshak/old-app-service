import datetime

from sqlalchemy import Column, String, DateTime

from .base import Base, generate_uuid


class DatasetSensitiveAttributeDetails(Base):
    __tablename__ = 'dataset_sensitive_attributes'
    id = Column(String(55), name="id", primary_key=True, default=generate_uuid)
    project_id = Column(String(55))
    dataset_path = Column(String(3000))
    sensitive_attribute = Column(String(55))
    sensitive_attribute_value = Column(String(55))
    dataset_type = Column(String(24))
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)
