import datetime

from sqlalchemy import Column, String, DateTime, Boolean

from .base import Base, generate_uuid


class ProjectUsers(Base):
    __tablename__ = 'project_users'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    user_id = Column(String(64))
    project_id = Column(String(64))
    is_active = Column(Boolean, default=True)
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)

    def __repr__(self):
        return "<ProjectUsers(project_id='%s')>" % (self.project_id)
