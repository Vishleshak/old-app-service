import datetime

from sqlalchemy import Column, String, Boolean, DateTime

from .base import Base, generate_uuid


class AfterMitigationJobExecutionStatus(Base):
    """This is model for After Mitigation Job Execution Status

    Args:
        Base ([Object]): Base model class

    Returns:
        [AfterMitigationJobExecutionStatus]: AfterMitigationJobExecutionStatus object
    """
    __tablename__ = 'after_mitigation_job_execution_status'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    job_id = Column(String(64))
    execution_step = Column(String(100))
    step_execution_status = Column(Boolean, default=False)
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)
