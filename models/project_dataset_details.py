import datetime

from sqlalchemy import Column, String, Boolean, DateTime

from .base import Base, generate_uuid


class ProjectDatasetDetails(Base):
    """This is model for Project Dataset Details

    Args:
        Base ([Object]): Base model class

    Returns:
        [ProjectDatasetDetails]: ProjectDatasetDetails object
    """
    __tablename__ = 'project_dataset_details'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    user_id = Column(String(64))
    project_id = Column(String(64))
    dataset_path = Column(String(2000))
    tag = Column(String(64))
    sensitive_attribute_detected = Column(Boolean, default=False)
    target_variable_detected = Column(Boolean, default=False)
    metadata_calculated = Column(Boolean, default=False)
    metadata_calculation_issue = Column(String(2000))
    is_active = Column(Boolean, default=True)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)
