import datetime

from sqlalchemy import Column, String, Boolean, DateTime

from .base import Base, generate_uuid


class ModelMonitorWebhookWhitelistIP(Base):
    """This is model for Model Monitor Webhook Whitelist IP

    Args:
        Base ([Object]): Base model class

    Returns:
        [ModelMonitorWebhookWhitelistIP]: ModelMonitorWebhookWhitelistIP object
    """
    __tablename__ = 'model_monitor_webhook_whitelist_ip'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    user_id = Column(String(64))
    project_id = Column(String(64))
    ip_address = Column(String(64))
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
