import datetime

from sqlalchemy import Column, String, Boolean, DateTime

from .base import Base, generate_uuid


class Organization(Base):
    """This is model for Organization

    Args:
        Base ([Object]): Base model class

    Returns:
        [Organization]: Organization object
    """
    __tablename__ = 'organization'
    __table_args__ = {'extend_existing': True}
    id = Column(String(54), name="id", primary_key=True, default=generate_uuid)
    name = Column(String(54))
    address = Column(String(54))
    is_active = Column(Boolean, default=True)
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)

    def __repr__(self):
        return "<Organization(name='%s')>" % self.name
