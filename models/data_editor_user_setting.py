import datetime

from sqlalchemy import Column, String, Boolean, DateTime

from .base import Base, generate_uuid


import datetime

from sqlalchemy import Column, String, Boolean, DateTime

from .base import Base, generate_uuid


class DataEditorUserSetting(Base):
    """This is model for Data Editor User Setting

    Args:
        Base ([Object]): Base model class

    Returns:
        [DataEditorUserSetting]: DataEditorUserSetting object
    """
    __tablename__ = 'data_editor_user_settings'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    project_id = Column(String(64))
    user_id = Column(String(64))
    statistical_parity_difference = Column(Boolean, default=False)
    disparate_impact_index = Column(Boolean, default=False)
    smoothed_empirical_differential_fairness = Column(Boolean, default=False)
    disparate_impact = Column(Boolean, default=False)
