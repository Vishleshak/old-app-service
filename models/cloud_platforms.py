import datetime

from sqlalchemy import Column, String, DateTime, Boolean

from .base import Base, generate_uuid


class CloudPlatform(Base):
    __tablename__ = 'cloud_platform'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    platform_name = Column(String(15))
    is_active = Column(Boolean, default=True)
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)

    def __repr__(self):
        return "<CloudPlatform(project_name='%s')>" % self.platform_name
