import datetime

from sqlalchemy import Column, String, Boolean, DateTime

from .base import Base, generate_uuid


class User(Base):
    """This is model for User

    Args:
        Base ([Object]): Base model class

    Returns:
        [User]: User object
    """

    __tablename__ = 'users'
    __table_args__ = {'extend_existing': True}
    id = Column(String(54), name="id", primary_key=True, default=generate_uuid)
    first_name = Column(String(54))
    last_name = Column(String(54))
    email = Column(String(54))
    auth_id = Column(String(54))
    terms_and_conditions = Column(Boolean, default=True)
    is_active = Column(Boolean, default=True)
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)

    def __repr__(self):
        return "<User(username='%s')>" % self.username
