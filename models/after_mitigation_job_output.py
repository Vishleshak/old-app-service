import datetime

from sqlalchemy import Column, String, Boolean, DateTime, JSON

from .base import Base


class AfterMitigationJobOutput(Base):
    """This is model for After Mitigation Job Output

    Args:
        Base ([Object]): Base model class

    Returns:
        [AfterMitigationJobOutput]: AfterMitigationJobOutput object
    """
    __tablename__ = 'after_mitigation_job_output'
    job_id = Column(String(64), primary_key=True)
    output = Column(JSON)
    execution_status = Column(Boolean, default=False)
    error = Column(String(2000))
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)
