import datetime

from sqlalchemy import Column, String, Boolean, DateTime

from .base import Base, generate_uuid


class UserRoles(Base):
    """This is model for User Roles

    Args:
        Base ([Object]): Base model class

    Returns:
        [UserRoles]: UserRoles object
    """
    __tablename__ = 'user_roles'
    __table_args__ = {'extend_existing': True}
    id = Column(String(54), name="id", primary_key=True, default=generate_uuid)
    name = Column(String(15))
    description = Column(String)
    is_active = Column(Boolean, default=True)
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)

    def __repr__(self):
        return "<UserRoles(name='%s')>" % self.name
