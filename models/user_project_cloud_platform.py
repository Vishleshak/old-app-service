from sqlalchemy import Column, String

from .base import Base, generate_uuid


class UserProjectCloudPlatform(Base):
    __tablename__ = 'project_cloud_platform'
    id = Column(String, name="id", primary_key=True, default=generate_uuid)
    platform_id = Column(String(64))
    project_id = Column(String(64))

    def __repr__(self):
        return "<UserProjectCloudPlatform(platform_id='%s')>" % self.platform_id
