from sqlalchemy import Column, String, Boolean

from .base import Base, generate_uuid


class ComplianceFrameworkRuleAuditProcedure(Base):
    """This is model for ComplianceFrameworkRuleAuditProcedure

    Args:
        Base ([Object]): Base model class

    Returns:
        [ComplianceFrameworkRuleAuditProcedure]: ComplianceFrameworkRuleAuditProcedure object
    """
    __tablename__ = 'compliance_framework_rule_audit_procedure'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    compliance_framework_rule_id = Column(String(64))
    procedure_details = Column(String(2000))
    is_deleted = Column(Boolean, default=False)
