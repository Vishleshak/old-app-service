import datetime

from sqlalchemy import Column, Integer, String, Boolean, DateTime

from .base import Base, generate_uuid


class KPI(Base):
    """This is model for KPI and fairness metrics

    Args:
        Base ([type]): Base model class

    Returns:
        [MitigationStrategy]: MitigationStrategy object
    """
    __tablename__ = 'kpi_and_fairness_metrics'
    __table_args__ = {'extend_existing': True}
    id = Column(String, name="id", primary_key=True, default=generate_uuid)
    conversion = Column(String(15))
    ARPU = Column(Integer())
    is_active = Column(Boolean, default=True)
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)

    def __repr__(self):
        return "<KPI(conversion='%s')>" % self.conversion
