import datetime

from sqlalchemy import Column, String, Boolean, DateTime

from .base import Base, generate_uuid


class JobTypes(Base):
    """This is model for Job Types

    Args:
        Base ([Object]): Base model class

    Returns:
        [JobTypes]: JobTypes object
    """
    __tablename__ = 'job_types'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    job_name = Column(String(64))
    job_description = Column(String(64))
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)
