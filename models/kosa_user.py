from sqlalchemy import Column, String, Boolean, ForeignKey, DateTime

from base import Base, generate_uuid


class KosaUser(Base):
    """This is model for creating Kosa User

    Args:
        Base ([Object]): Base Model Class

    Returns:
        [KosaUser]: KosaUser Object
    """
    __tablename__ = 'kosa_users'
    __table_args__ = {'extend_existing': True}
    id = Column(String, name="id", primary_key=True, default=generate_uuid)
    name = Column(String(15))
    auth0_user_id = Column(String(12))
    user_role = Column(String, ForeignKey('user_roles.id'))
    is_active = Column(Boolean)
    is_deleted = Column(Boolean)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    def __repr__(self):
        return "<KosaUser(name='%s')>" % self.name


if __name__ == '__main__':
    KosaUser()
