import datetime

from sqlalchemy import Column, String, Boolean, DateTime

from .base import Base, generate_uuid


class DataEditorJobDetails(Base):
    """This is model for Data Editor Job Detail

    Args:
        Base ([Object]): Base model class

    Returns:
        [DataEditorJobDetails]: DataEditorJobDetails object
    """
    __tablename__ = 'data_editor_jobs'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    user_id = Column(String(64))
    project_id = Column(String(64))
    file_path = Column(String(5000))
    target_variable = Column(String(100))
    favorable_classes = Column(String(100))
    protected_attribute_names = Column(String(100))
    privileged_classes = Column(String(100))
    mitigation_strategy = Column(String(100))
    before_mitigation_job_id = Column(String(64))
    job_type = Column(String(20))
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)
