import datetime

from sqlalchemy import Column, Integer, String, Boolean, DateTime

from .base import Base, generate_uuid


class JobDetails(Base):
    """This is model for Job Details

    Args:
        Base ([Object]): Base model class

    Returns:
        [JobDetails]: JobDetails object
    """
    __tablename__ = 'job_details'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    user_id = Column(String(64))
    project_id = Column(String(64))
    job_type = Column(String(64))
    size_of_data = Column(Integer)
    job_params_id = Column(String(64))
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)
