from sqlalchemy import Column, String, Boolean

from .base import Base, generate_uuid


class ComplianceFrameworkRuleQuestion(Base):
    """This is model for ComplianceFrameworkRuleQuestion

    Args:
        Base ([Object]): Base model class

    Returns:
        [ComplianceFrameworkRuleQuestion]: ComplianceFrameworkRuleQuestion object
    """
    __tablename__ = 'compliance_framework_rule_questions'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    compliance_framework_rule_id = Column(String(64))
    question = Column(String(2000))
    is_deleted = Column(Boolean, default=False)
