from sqlalchemy import Column, String

from .base import Base, generate_uuid


class ComplianceFrameworkRule(Base):
    """This is model for ComplianceFrameworkRule

    Args:
        Base ([Object]): Base model class

    Returns:
        [ComplianceFrameworkRule]: ComplianceFrameworkRule object
    """
    __tablename__ = 'compliance_framework_rules'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    title = Column(String(64))
    description = Column(String(2000))
    section_name = Column(String(64))
    compliance_framework_id = Column(String(64))
    audit_procedure_details = Column(String(2000))
