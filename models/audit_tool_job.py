import datetime

from sqlalchemy import Column, String, Boolean, DateTime

from .base import Base, generate_uuid


class AuditToolJobDetail(Base):
    """This is model for Audit Tool Job Detail

    Args:
        Base ([Object]): Base model class

    Returns:
        [AuditToolJobDetail]: AuditToolJobDetail object
    """
    __tablename__ = 'audit_tool_jobs'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    user_id = Column(String(64))
    project_id = Column(String(64))
    file_path = Column(String(5000))
    target_variable = Column(String(100))
    favorable_classes = Column(String(100))
    performance = Column(String(100))
    fairness = Column(String(100))
    charts = Column(String(100))
    protected_attribute_names = Column(String(100))
    model_file_path = Column(String(100))
    privileged_classes = Column(String(100))
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)
