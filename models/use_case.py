from sqlalchemy import Column, String

from .base import Base, generate_uuid


class UseCase(Base):
    """This is model for UseCase

    Args:
        Base ([Object]): Base model class

    Returns:
        [UseCase]: UseCase object
    """
    __tablename__ = 'use_cases'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    industry_id = Column(String(64))
    name = Column(String(64))
    description = Column(String(2000))
