import datetime

from sqlalchemy import Column, String, DateTime

from .base import Base, generate_uuid


class ComplianceFrameworkRuleEvidenceDocument(Base):
    """This is model for ComplianceFrameworkRule

    Args:
        Base ([Object]): Base model class

    Returns:
        [ComplianceFrameworkRule]: ComplianceFrameworkRule object
    """
    __tablename__ = 'compliance_framework_rule_evidence_documents'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    compliance_framework_rule_id = Column(String(64))
    comment_id = Column(String(64))
    document_path = Column(String(2000))
    uploaded_on = Column(DateTime, default=datetime.datetime.utcnow)
