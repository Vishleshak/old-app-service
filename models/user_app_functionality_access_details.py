import datetime

from sqlalchemy import Column, String, Boolean, DateTime

from .base import Base, generate_uuid


class UserAppFunctionalityAccessDetails(Base):
    """This is model for UserRights Details

    Args:
        Base ([Object]): Base model class

    Returns:
        [UserAccessDetails]: UserAccessDetails object
    """
    __tablename__ = 'user_functionality_access_details'
    __table_args__ = {'extend_existing': True}
    id = Column(String(54), name="id", primary_key=True, default=generate_uuid)
    user_id = Column(String(24))
    app_functionality_id = Column(String(24))
    is_active = Column(Boolean, default=True)
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)

    def __repr__(self):
        return "<UserRights(user_id='%s')>" % self.user_id
