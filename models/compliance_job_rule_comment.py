import datetime

from sqlalchemy import Column, String, Boolean, DateTime

from .base import Base, generate_uuid


class ComplianceJobRuleComment(Base):
    """This is model for ComplianceJobRuleComment

    Args:
        Base ([Object]): Base model class

    Returns:
        [ComplianceJobRuleComment]: ComplianceJobRuleComment object
    """
    __tablename__ = 'compliance_job_rule_comments'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    compliance_job_id = Column(String(64))
    compliance_rule_id = Column(String(64))
    comment = Column(String(3000))
    comment_by = Column(String(64))
    commented_on = Column(DateTime, default=datetime.datetime.utcnow)
    updated_on = Column(DateTime, default=datetime.datetime.utcnow)
    is_deleted = Column(Boolean, default=False)
