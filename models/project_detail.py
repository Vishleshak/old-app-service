import datetime

from sqlalchemy import Column, String, DateTime, Boolean

from .base import Base, generate_uuid


class ProjectDetail(Base):
    __tablename__ = 'project_detail'
    id = Column(String, name="id", primary_key=True, default=generate_uuid)
    project_name = Column(String(15))
    description = Column(String(500))
    user_id = Column(String(64))
    use_kosa_storage = Column(Boolean, default=False)
    region = Column(String(64))
    country = Column(String(64))
    use_case = Column(String(64))
    industry = Column(String(64))
    organization_id = Column(String(64), nullable=False)
    is_active = Column(Boolean, default=True)
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)

    def __repr__(self):
        return "<ProjectDetail(project_name='%s')>" % self.project_name
