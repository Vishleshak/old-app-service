import datetime

from sqlalchemy import Column, String, Boolean, DateTime, JSON

from .base import Base, generate_uuid


class AfterMitigationJobDetail(Base):
    """This is model for After Mitigation Job Detail

    Args:
        Base ([Object]): Base model class

    Returns:
        [AfterMitigationJobDetail]: AfterMitigationJobDetail object
    """
    __tablename__ = 'after_mitigation_jobs'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    visible_metrics = Column(JSON)
    project_id = Column(String(64))
    file_path = Column(String(5000))
    target_variable = Column(String(100))
    favorable_classes = Column(String(100))
    protected_attribute_names = Column(String(100))
    privileged_classes = Column(String(100))
    mitigation_strategy = Column(String(100))
    before_mitigation_job_id = Column(String(64))
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)
