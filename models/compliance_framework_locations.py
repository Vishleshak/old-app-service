from sqlalchemy import Column, String

from .base import Base, generate_uuid


class ComplianceFrameworkLocation(Base):
    """This is model for ComplianceFrameworkLocation

    Args:
        Base ([Object]): Base model class

    Returns:
        [ComplianceFrameworkLocation]: ComplianceFrameworkLocation object
    """
    __tablename__ = 'compliance_framework_locations'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    compliance_framework_id = Column(String(64))
    location_id = Column(String(64))
