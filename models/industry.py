from sqlalchemy import Column, String

from .base import Base, generate_uuid


class Industry(Base):
    """This is model for Industry

    Args:
        Base ([Object]): Base model class

    Returns:
        [Industry]: Industry object
    """
    __tablename__ = 'industries'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    name = Column(String(64))
    description = Column(String(2000))
