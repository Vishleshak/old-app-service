from sqlalchemy import Column, String

from .base import Base, generate_uuid


class ComplianceFrameworkRuleState(Base):
    """This is model for ComplianceFrameworkRule

    Args:
        Base ([Object]): Base model class

    Returns:
        [ComplianceFrameworkRule]: ComplianceFrameworkRule object
    """
    __tablename__ = 'compliance_framework_rule_states'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    state = Column(String(64))
    description = Column(String(2000))
