import datetime

from sqlalchemy import Column, String, Boolean, DateTime

from .base import Base, generate_uuid


class OrganizationInvitation(Base):
    """This is model for OrganizationInvitation

    Args:
        Base ([Object]): Base model class

    Returns:
        [OrganizationInvitation]: OrganizationInvitation object
    """
    __tablename__ = 'organization_invitation'
    id = Column(String(54), name="id", primary_key=True, default=generate_uuid)
    organization_id = Column(String(54))
    invitation_hash = Column(String(500))
    email = Column(String(54))
    user_role_id = Column(String(54))
    accessible_modules = Column(String(2000))
    user_rights = Column(String(2000))
    is_admin = Column(Boolean, default=False)
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)

    def __repr__(self):
        return "<OrganizationInvitation(name='%s')>" % self.name
