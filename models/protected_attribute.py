from sqlalchemy import Column, Integer, String, DateTime, Boolean
import datetime

from sqlalchemy import Column, Integer, String, DateTime, Boolean

from .base import Base, generate_uuid


class ProtectedAttribute(Base):
    __tablename__ = 'protected_attribute'
    __table_args__ = {'extend_existing': True}
    id = Column(String, name="id", primary_key=True, default=generate_uuid)
    name = Column(String(15))
    visibilty_priority = Column(Integer())
    is_active = Column(Boolean, default=True)
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)

    def __repr__(self):
        return "<ProtectedAttribute(name='%s')>" % self.name
