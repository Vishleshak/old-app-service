import datetime

from sqlalchemy import Column, String, Boolean, DateTime

from .base import Base, generate_uuid


class ProjectModelDetails(Base):
    """This is model for Project Model Details

    Args:
        Base ([Object]): Base model class

    Returns:
        [ProjectModelDetails]: ProjectModelDetails object
    """
    __tablename__ = 'project_model_details'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    user_id = Column(String(64))
    project_id = Column(String(64))
    model_path = Column(String(2000))
    tag = Column(String(64))
    is_active = Column(Boolean, default=True)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)
