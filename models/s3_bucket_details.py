import datetime

from sqlalchemy import Column, String, Boolean, DateTime

from .base import Base, generate_uuid


class S3BucketDetails(Base):
    """This is model for S3 Bucket Details

    Args:
        Base ([Object]): Base model class

    Returns:
        [S3BucketDetails]: S3BucketDetails object
    """
    __tablename__ = 's3_bucket_details'
    __table_args__ = {'extend_existing': True}
    id = Column(String, name="id", primary_key=True, default=generate_uuid)
    user_id = Column(String(64))
    project_id = Column(String(64))
    bucket_key = Column(String(24))
    s3_access_key = Column(String)
    s3_secret_key = Column(String)
    is_active = Column(Boolean, default=True)
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)

    def __repr__(self):
        return "<S3BucketDetails(bucket_key='%s')>" % self.bucket_key
