import datetime

from sqlalchemy import Column, String, Boolean, DateTime

from .base import Base, generate_uuid


class ProjectModelStorageDetails(Base):
    """This is model for Project Model Storage Details

    Args:
        Base ([Object]): Base model class

    Returns:
        [ProjectDatasetModelDetails]: ProjectModelStorageDetails object
    """
    __tablename__ = 'project_model_storage_details'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    project_id = Column(String(64))
    storage_type = Column(String(20))
    bucket_key = Column(String(200))
    s3_access_key = Column(String(200))
    s3_secret_key = Column(String(200))
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)
