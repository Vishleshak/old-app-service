import datetime

from sqlalchemy import Column, String, Boolean, DateTime

from .base import Base, generate_uuid


class ModelDatasetDetails(Base):
    """This is model for Model Dataset Details

    Args:
        Base ([Object]): Base model class

    Returns:
        [ModelDatasetDetails]: ModelDatasetDetails object
    """
    __tablename__ = 'model_dataset_details'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    user_id = Column(String(64))
    project_id = Column(String(64))
    cloud_id = Column(String(64))
    storage_type = Column(String(64))
    bucket_key = Column(String(24))
    s3_access_key = Column(String(200))
    s3_secret_key = Column(String(200))
    is_active = Column(Boolean, default=True)
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)
