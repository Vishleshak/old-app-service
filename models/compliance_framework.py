import datetime

from sqlalchemy import Column, String, Boolean, DateTime

from .base import Base, generate_uuid


class ComplianceFramework(Base):
    """This is model for ComplianceFramework

    Args:
        Base ([Object]): Base model class

    Returns:
        [ComplianceFramework]: ComplianceFramework object
    """
    __tablename__ = 'compliance_framework'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    name = Column(String(64))
    framework_type = Column(String(64))
    description = Column(String(2000))
    is_active = Column(Boolean, default=True)
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)
