from sqlalchemy import Column, String

from .base import Base, generate_uuid


class ComplianceFrameworkCountry(Base):
    """This is model for ComplianceFrameworkCountry

    Args:
        Base ([Object]): Base model class

    Returns:
        [ComplianceFrameworkCountry]: ComplianceFrameworkCountry object
    """
    __tablename__ = 'compliance_framework_countries'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    compliance_framework_id = Column(String(64))
    country_name = Column(String(64))
