import datetime

from sqlalchemy import Column, String, Boolean, DateTime

from .base import Base, generate_uuid


class BeforeMitigationJobDetail(Base):
    """This is model for Before Mitigation Job Detail

    Args:
        Base ([Object]): Base model class

    Returns:
        [BeforeMitigationJobDetail]: BeforeMitigationJobDetail object
    """
    __tablename__ = 'before_mitigation_jobs'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    user_id = Column(String(64))
    project_id = Column(String(64))
    file_path = Column(String(5000))
    target_variable = Column(String(100))
    favorable_classes = Column(String(100))
    protected_attribute_names = Column(String(100))
    privileged_classes = Column(String(100))
    is_deleted = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)
