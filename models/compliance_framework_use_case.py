from sqlalchemy import Column, String

from .base import Base, generate_uuid


class ComplianceFrameworkUseCase(Base):
    """This is model for ComplianceFrameworkUseCase

    Args:
        Base ([Object]): Base model class

    Returns:
        [ComplianceFrameworkUseCase]: ComplianceFrameworkUseCase object
    """
    __tablename__ = 'compliance_framework_use_case'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    compliance_framework_id = Column(String(64))
    use_case_id = Column(String(64))
