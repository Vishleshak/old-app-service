from sqlalchemy import Column, String, JSON

from .base import Base, generate_uuid


class AuditToolGlobalSetting(Base):
    """This is model for Audit Tool Global Setting

    Args:
        Base ([Object]): Base model class

    Returns:
        [AuditToolGlobalSetting]: AuditToolGlobalSetting object
    """
    __tablename__ = 'audit_tool_global_settings'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    use_case_id = Column(String(64))
    metrics = Column(JSON)
