import datetime

from sqlalchemy import Column, String, DateTime

from .base import Base, generate_uuid


class ModelMonitorWebhookApiKey(Base):
    """This is model for Model Monitor Details

    Args:
        Base ([Object]): Base model class

    Returns:
        [ModelMonitorDetails]: ModelMonitorDetails object
    """
    __tablename__ = 'model_monitor_webhook_details'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    user_id = Column(String(64))
    project_id = Column(String(64))
    webhook_api_key = Column(String(256))
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
