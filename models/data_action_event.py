from sqlalchemy import Column, Integer, String, ForeignKey
from .base import Base, generate_uuid
from sqlalchemy import Column, Integer, String, ForeignKey

from .base import Base, generate_uuid


class DataActionEvent(Base):
    __tablename__ = 'data_action_event'
    __table_args__ = {'extend_existing': True}
    id = Column(String, name="id", primary_key=True, default=generate_uuid)
    event_id = Column(String, ForeignKey('event.id'))
    action_type = Column(Integer())
    failure_reason = Column(String(255))

    def __repr__(self):
        return "<DataActionEvent(event_id='%s')>" % self.event_id
