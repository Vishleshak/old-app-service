import datetime

from sqlalchemy import Column, String, Boolean, DateTime

from .base import Base, generate_uuid


class ModuleAccessDetail(Base):
    """This is model for Module Access Detail

    Args:
        Base ([Object]): Base model class

    Returns:
        [ModuleAccessDetail]: ModuleAccessDetail object
    """
    __tablename__ = 'module_access_details'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    user_id = Column(String(64))
    module_id = Column(String(64))
    accessed_at = Column(DateTime, default=datetime.datetime.utcnow)
