import datetime

from sqlalchemy import Column, String, DateTime

from .base import Base, generate_uuid


class JobExecutionEventDetails(Base):
    """This is model for Event Details

    Args:
        Base ([Object]): Base model class

    Returns:
        [EventDetails]: EventDetails object
    """
    __tablename__ = 'job_execution_event_details'
    id = Column(String(64), name="id", primary_key=True, default=generate_uuid)
    job_id = Column(String(64))
    event_name = Column(String(64))
    started_at = Column(DateTime, default=datetime.datetime.utcnow)
    finished_at = Column(DateTime)
