import io

import boto3
import pandas as pd

from utils.event_util import get_body
from utils.log_util import get_logger

logging = get_logger(__name__)


def dataset_details(event, context):
    try:
        logging.debug(event)
        event_body = get_body(event)
        df = get_s3_data(event_body)
        return {"status_code": 200, "result": df.columns}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred"}


def get_s3_data(event_body):
    result = {}
    sesssion = boto3.Session(event_body.get("aws_access_key"), event_body.get("aws_secret_key"))
    s3 = sesssion.resource("s3")
    obj = s3.get_object(Bucket=event_body.get("bucket_name"), Key=event_body.get("key"))
    df = pd.read_csv(io.BytesIO(obj['Body'].read()))
    return df
