import json

from models.compliance_framework import ComplianceFramework
from models.compliance_framework_country import ComplianceFrameworkCountry
from models.compliance_framework_industry import ComplianceFrameworkIndustry
from models.compliance_framework_region import ComplianceFrameworkRegion
from models.compliance_framework_use_case import ComplianceFrameworkUseCase
from models.json_converter import AlchemyEncoder
from utils.db_util import get_database_session, commit_database_session, close_database_session
from utils.event_util import get_body, get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def add_compliance_framework(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        if event_body.get("name") is None:
            return {"status_code": 500, "error": "Field name is mandatory"}
        if event_body.get("framework_type") is None:
            return {"status_code": 500, "error": "Field framework_type is mandatory"}
        if event_body.get("description") is None:
            return {"status_code": 500, "error": "Field description is mandatory"}
        if event_body.get("industries") is None:
            return {"status_code": 500, "error": "Field industries is mandatory"}
        if event_body.get("use_cases") is None:
            return {"status_code": 500, "error": "Field use_cases is mandatory"}
        if event_body.get("regions") is None:
            return {"status_code": 500, "error": "Field regions is mandatory"}

        session = get_database_session()
        result = session.query(ComplianceFramework).filter(ComplianceFramework.name == event_body.get("name")).first()
        if result:
            return {"status_code": 500,
                    "message": "ComplianceFramework with name {} already exists".format(event_body.get("name"))}

        compliance_framework = ComplianceFramework()
        compliance_framework.name = event_body.get("name")
        compliance_framework.framework_type = event_body.get("framework_type")
        compliance_framework.description = event_body.get("description")
        session.add(compliance_framework)
        commit_database_session(session)
        compliance_framework_id = compliance_framework.id

        regions = event_body.get("regions")
        for region in regions:
            compliance_framework_region = ComplianceFrameworkRegion()
            compliance_framework_region.compliance_framework_id = compliance_framework_id
            compliance_framework_region.region_name = region
            session.add(compliance_framework_region)
            commit_database_session(session)

        use_cases = event_body.get("use_cases")
        for use_case in use_cases:
            compliance_framework_use_case = ComplianceFrameworkUseCase()
            compliance_framework_use_case.compliance_framework_id = compliance_framework_id
            compliance_framework_use_case.use_case_id = use_case
            session.add(compliance_framework_use_case)
            commit_database_session(session)

        industries = event_body.get("industries")
        for industry in industries:
            compliance_framework_industry = ComplianceFrameworkIndustry()
            compliance_framework_industry.compliance_framework_id = compliance_framework_id
            compliance_framework_industry.industry_id = industry
            session.add(compliance_framework_industry)
            commit_database_session(session)

        countries = event_body.get("countries")
        if countries is not None:
            for country in countries:
                compliance_framework_country = ComplianceFrameworkCountry()
                compliance_framework_country.compliance_framework_id = compliance_framework_id
                compliance_framework_country.country_name = country
                session.add(compliance_framework_country)
                commit_database_session(session)

        return {"status_code": 201, "compliance_framework": json.dumps(compliance_framework, cls=AlchemyEncoder)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while adding compliance framework"}
    finally:
        if session is not None:
            close_database_session(session)


def get_compliance_framework(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        id = path_parameter.get("id")
        if id is None:
            return {"status_code": 500, "error": "Missing id in path parameter"}
        session = get_database_session()
        if id is not None:
            result = session.query(ComplianceFramework).filter(ComplianceFramework.id == id,
                                                               ComplianceFramework.is_deleted == False).first()
        if not result:
            return {"status_code": 404, "message": "ComplianceFramework {} does not exist".format(id)}
        return json.dumps(result, cls=AlchemyEncoder)
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting compliance framework"}
    finally:
        if session is not None:
            close_database_session(session)


def get_all_compliance_frameworks(event, context):
    session = None
    try:
        logging.debug(event)
        session = get_database_session()
        result = session.query(ComplianceFramework).all()
        if not result:
            return {"status_code": 404, "message": "ComplianceFramework {} does not exist".format(id)}
        return json.dumps(result, cls=AlchemyEncoder)
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting all compliance frameworks"}
    finally:
        if session is not None:
            close_database_session(session)


def edit_compliance_framework(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        if event_body.get("id") is not None:
            compliance_framework = ComplianceFramework()
            compliance_framework.id = event_body.get("id")
            session = get_database_session()
            data_to_update = {}
            if event_body.get("name") is not None:
                data_to_update["name"] = event_body.get("name")
            if event_body.get("framework_type") is not None:
                data_to_update["framework_type"] = event_body.get("framework_type")
            if event_body.get("description") is not None:
                data_to_update["description"] = event_body.get("description")
            session.query(ComplianceFramework).filter(id == event_body.get("id")).update(data_to_update)
            commit_database_session(session)
        return {"status_code": 500}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while deleting compliance framework"}
    finally:
        if session is not None:
            close_database_session(session)


def delete_compliance_framework(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        id = path_parameter.get("id")
        session = get_database_session()
        result = session.query(ComplianceFramework).filter(ComplianceFramework.id == id).first()
        if not result:
            return {"status_code": 404, "message": "ComplianceFramework {} does not exist".format(id)}
        data_to_update = {}
        data_to_update["is_deleted"] = True
        session.query(ComplianceFramework).filter(ComplianceFramework.id == id).update(data_to_update)
        session.commit()
        return {"message": "ComplianceFramework {} deleted successfully".format(id)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while deleting compliance framework"}
    finally:
        if session is not None:
            close_database_session(session)


def get_compliance_framework_use_cases(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        id = path_parameter.get("id")
        session = get_database_session()
        if id is None:
            return {"status_code": 500, "error": "Please provide ID"}
        result = session.query(ComplianceFramework, ComplianceFrameworkUseCase).filter(
            ComplianceFramework.id == id).filter(ComplianceFrameworkUseCase.compliance_framework_id == id).all()
        if not result:
            return {"status_code": 404, "message": "Compliance Framework Use cases {} does not exist".format(id)}
        return json.dumps(result, cls=AlchemyEncoder)
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting compliance framework use cases"}
    finally:
        if session is not None:
            close_database_session(session)


def get_compliance_framework_industries(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        if event_body.get("framework_id") is None:
            return {"status_code": 500, "error_type": "missing_value", "error": "Field framework_id is missing"}
        if event_body.get("industries") is None:
            return {"status_code": 500, "error_type": "missing_value", "error": "Field industries is missing"}

        framework_id = event_body.get("framework_id")
        industries_id = event_body.get("industries")
        session = get_database_session()

        result = session.query(ComplianceFrameworkIndustry).filter(
            ComplianceFrameworkIndustry.compliance_framework_id == framework_id).all()
        if result is not None:
            for compliance_framework_industry in result:
                if compliance_framework_industry.industry_id in industries_id:
                    industries_id.remove(compliance_framework_industry.industry_id)

        if len(industries_id) == 0:
            return {"status_code": 500, "error_type": "already_added_industries",
                    "error": "The given industries are already added"}

        for industry_id in industries_id:
            compliance_framework_industry = ComplianceFrameworkIndustry()
            compliance_framework_industry.compliance_framework_id = framework_id
            compliance_framework_industry.industry_id = industry_id
            session.add(compliance_framework_industry)
        commit_database_session(session)
        return {"status_code": 200, "message": "Industries added successsfully"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while adding industries to compliance framework"}
    finally:
        if session is not None:
            close_database_session(session)


def add_compliance_framework_use_cases(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        if event_body.get("framework_id") is None:
            return {"status_code": 500, "error_type": "missing_value", "error": "Field framework_id is missing"}
        if event_body.get("use_cases") is None:
            return {"status_code": 500, "error_type": "missing_value", "error": "Field use_cases is missing"}

        framework_id = event_body.get("framework_id")
        use_cases_id = event_body.get("use_cases")
        session = get_database_session()

        result = session.query(ComplianceFrameworkUseCase).filter(
            ComplianceFrameworkUseCase.compliance_framework_id == framework_id).all()
        if result is not None:
            for compliance_framework_use_case in result:
                if compliance_framework_use_case.use_case_id in use_cases_id:
                    use_cases_id.remove(compliance_framework_use_case.use_case_id)

        if len(use_cases_id) == 0:
            return {"status_code": 500, "error_type": "already_added_use_case",
                    "error": "The given use case is already added"}

        for use_case_id in use_cases_id:
            compliance_framework_use_case = ComplianceFrameworkUseCase()
            compliance_framework_use_case.compliance_framework_id = framework_id
            compliance_framework_use_case.use_case_id = use_case_id
            session.add(compliance_framework_use_case)
        commit_database_session(session)
        return {"status_code": 200, "message": "Use cases added successsfully"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while adding use cases to compliance framework"}
    finally:
        if session is not None:
            close_database_session(session)


def add_compliance_framework_locations(event, context):
    session = None
    try:
        logging.debug(event)
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while adding locations compliance framework"}
    finally:
        if session is not None:
            close_database_session(session)
