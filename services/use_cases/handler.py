import json

from models.json_converter import AlchemyEncoder
from models.use_case import UseCase
from utils.db_util import get_database_session, close_database_session
from utils.event_util import get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def get_use_case(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        record_id = path_parameter.get("id")
        if record_id is None:
            return {"status_code": 500, "message": "Parameter ID is required"}
        session = get_database_session()
        if record_id is not None:
            result = session.query(UseCase).filter(UseCase.id == record_id).first()
            if not result:
                return {"status_code": 404, "message": "Use Case {} does not exist".format(record_id)}
        return json.dumps(result, cls=AlchemyEncoder)
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting use cases"}
    finally:
        if session is not None:
            close_database_session(session)


def get_use_cases(event, context):
    session = None
    try:
        logging.debug(event)
        session = get_database_session()
        result = session.query(UseCase).all()
        return {"status_code": 200, "use_cases": json.dumps(result, cls=AlchemyEncoder)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting use cases"}
    finally:
        if session is not None:
            close_database_session(session)


def get_industry_use_cases(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        industry_id = path_parameter.get("id")
        if industry_id is None:
            return {"status_code": 500, "message": "Parameter industry_id is required"}
        session = get_database_session()
        if industry_id is not None:
            result = session.query(UseCase).filter(UseCase.industry_id == industry_id).all()
            if not result:
                return {"status_code": 404, "message": "Use CaseS for industry {} does not exist".format(industry_id)}
        return json.dumps(result, cls=AlchemyEncoder)
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting use cases by industry"}
    finally:
        if session is not None:
            close_database_session(session)
