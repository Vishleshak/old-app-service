import json

from models.compliance_framework_rule import ComplianceFrameworkRule
from models.compliance_framework_rule_question import ComplianceFrameworkRuleQuestion
from models.json_converter import AlchemyEncoder
from utils.db_util import get_database_session, commit_database_session, close_database_session
from utils.event_util import get_body, get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def add_compliance_framework_rule(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)

        title = event_body.get("title")
        if title is None:
            return {"status_code": 500, "message": "Field title is mandatory"}

        description = event_body.get("description")
        if description is None:
            return {"status_code": 500, "message": "Field description is mandatory"}

        section_name = event_body.get("section_name")
        if section_name is None:
            return {"status_code": 500, "message": "Field section_name is mandatory"}

        compliance_framework_id = event_body.get("compliance_framework_id")
        if compliance_framework_id is None:
            return {"status_code": 500, "message": "Field compliance_framework_id is mandatory"}

        audit_procedure_details = event_body.get("audit_procedure_details")
        if audit_procedure_details is None:
            return {"status_code": 500, "message": "Field audit_procedure_details is mandatory"}

        questions = event_body.get("questions")
        compliance_framework_rule = ComplianceFrameworkRule()
        compliance_framework_rule.title = title
        compliance_framework_rule.description = description
        compliance_framework_rule.section_name = section_name
        compliance_framework_rule.compliance_framework_id = compliance_framework_id
        compliance_framework_rule.audit_procedure_details = audit_procedure_details
        session = get_database_session()
        session.add(compliance_framework_rule)
        commit_database_session(session)

        if questions is not None:
            for question in questions:
                compliance_framework_rule_question = ComplianceFrameworkRuleQuestion()
                compliance_framework_rule_question.compliance_framework_rule_id = compliance_framework_rule.id
                compliance_framework_rule_question.question = question
                session.add(compliance_framework_rule)
                commit_database_session(session)

        return {"status_code": 201,
                "compliance_framework_rule": json.dumps(compliance_framework_rule, cls=AlchemyEncoder)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while adding compliance framework rule"}
    finally:
        if session is not None:
            close_database_session(session)


def get_compliance_rule_details(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_path_parameters(event)
        compliance_rule_id = event_body.get("compliance_rule_id")
        if compliance_rule_id is None:
            return {"status_code": 500, "message": "Field compliance_rule_id is mandatory"}

        session = get_database_session()
        compliance_framework_rule = session.query(ComplianceFrameworkRule).filter(
            ComplianceFrameworkRule.id == compliance_rule_id).first()
        if not compliance_framework_rule:
            return {"status_code": 500, "message": "Compliance framework rule is not present"}
        return json.dumps(compliance_framework_rule, cls=AlchemyEncoder)
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting  compliance framework rule"}
    finally:
        if session is not None:
            close_database_session(session)


def get_compliance_rules(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_path_parameters(event)
        compliance_framework_id = event_body.get("id")
        if compliance_framework_id is None:
            return {"status_code": 500, "message": "Field compliance_framework_id is mandatory"}

        session = get_database_session()
        compliance_framework_rules = session.query(ComplianceFrameworkRule).filter(
            ComplianceFrameworkRule.compliance_framework_id == compliance_framework_id).all()
        if not compliance_framework_rules:
            return {"status_code": 500, "message": "Compliance Framework Rules are not present for this project"}
        return {"status_code": 200,
                "compliance_framework_rules": json.dumps(compliance_framework_rules, cls=AlchemyEncoder)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while adding compliance job"}
    finally:
        if session is not None:
            close_database_session(session)


def get_compliance_rules_by_section(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_path_parameters(event)
        id = event_body.get("id")
        if compliance_framework_id is None:
            return {"status_code": 500, "message": "Field compliance_framework_id is mandatory"}

        session = get_database_session()
        compliance_framework_rules = session.query(ComplianceFrameworkRule).filter(
            ComplianceFrameworkRule.compliance_framework_id == compliance_framework_id).all()
        if not compliance_framework_rules:
            return {"status_code": 500, "message": "Compliance Framework Rules are not present for this project"}
        return {"status_code": 200,
                "compliance_framework_rules": json.dumps(compliance_framework_rules, cls=AlchemyEncoder)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while adding compliance job"}
    finally:
        if session is not None:
            close_database_session(session)


def edit_compliance_framework_rule(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        data_to_update = {}

        id = event_body.get("id")
        if id is None:
            return {"status_code": 500, "message": "Field id is mandatory"}

        compliance_framework_id = event_body.get("compliance_framework_id")
        if compliance_framework_id is None:
            return {"status_code": 500, "message": "Field compliance_framework_id is mandatory"}

        title = event_body.get("title")
        if title is None:
            return {"status_code": 500, "message": "Field title is mandatory"}
        else:
            data_to_update["title"] = title

        description = event_body.get("description")
        if description is None:
            return {"status_code": 500, "message": "Field description is mandatory"}
        else:
            data_to_update["description"] = description

        section_name = event_body.get("section_name")
        if section_name is None:
            return {"status_code": 500, "message": "Field section_name is mandatory"}
        else:
            data_to_update["section_name"] = section_name

        audit_procedure_details = event_body.get("audit_procedure_details")
        if audit_procedure_details is None:
            return {"status_code": 500, "message": "Field audit_procedure_details is mandatory"}
        else:
            data_to_update["audit_procedure_details"] = audit_procedure_details

        session = get_database_session()
        if len(data_to_update) > 0:
            session.query(ComplianceFrameworkRule).filter(ComplianceFrameworkRule.id == id).update(data_to_update)
            commit_database_session(session)
        result = session.query(ComplianceFrameworkRule).filter(ComplianceFrameworkRule.id == id).first()
        return json.dumps(result, cls=AlchemyEncoder)
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while editing compliance framework rule"}
    finally:
        if session is not None:
            close_database_session(session)
