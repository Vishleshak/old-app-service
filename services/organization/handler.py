import json

from models.json_converter import AlchemyEncoder
from models.organization import Organization
from utils.db_util import get_database_session, commit_database_session, close_database_session
from utils.event_util import get_body, get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def add_organization(event, context):
    """This is method for adding KPI and fairness metrics

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        organization = Organization()
        organization.name = event_body.get("name")
        organization.address = event_body.get("address")
        session = get_database_session()
        session.add(organization)
        commit_database_session(session)
        return {"status_code": 201, "organization": json.dumps(organization, cls=AlchemyEncoder)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred in creating organization"}
    finally:
        if session is not None:
            close_database_session(session)


def update_organization(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        if event_body.get("id") is not None:
            organization = Organization()
            organization.id = event_body.get("id")
            session = get_database_session()
            data_to_update = {}
            if event_body.get("name") is not None:
                data_to_update["name"] = event_body.get("name")
            if event_body.get("address") is not None:
                data_to_update["address"] = event_body.get("address")
            session.query(Organization).filter(id == event_body.get("id")).update(data_to_update)
            commit_database_session(session)
        return {"status_code": 500}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred in updating organization"}
    finally:
        if session is not None:
            close_database_session(session)


def get_organization(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        record_id = path_parameter.get("id")
        session = get_database_session()
        if record_id is not None:
            result = session.query(Organization).filter(Organization.id == record_id).first()
            if not result:
                return {"status_code": 404, "message": "Organization {} does not exist".format(record_id)}
            return json.dumps(result, cls=AlchemyEncoder)
        return {"status_code": 404, "message": "Please pass organization id"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting organization"}
    finally:
        if session is not None:
            close_database_session(session)


def get_all_organizations(event, context):
    session = None
    try:
        logging.debug(event)
        session = get_database_session()
        if id is not None:
            result = session.query(Organization).all()
            if not result:
                return {"status_code": 404, "message": "Organizations does not exist"}
            return json.dumps(result, cls=AlchemyEncoder)
        return {"status_code": 404, "message": "Please pass organization id"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting all organization"}
    finally:
        if session is not None:
            close_database_session(session)


def delete_organization(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        record_id = path_parameter.get("id")
        session = get_database_session()
        result = session.query(Organization).filter(Organization.id == record_id).first()
        if not result:
            return {"status_code": 404, "message": "Organization {} does not exist".format(record_id)}
        session.delete(result)
        session.commit()
        return {"message": "Organization {} deleted successfully".format(record_id)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while deleting organization"}
    finally:
        if session is not None:
            close_database_session(session)
