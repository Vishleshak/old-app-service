import json

from models.compliance_job import ComplianceJob
from models.compliance_job_rule_comment import ComplianceJobRuleComment
from models.json_converter import AlchemyEncoder
from utils.db_util import get_database_session, commit_database_session, close_database_session
from utils.event_util import get_body, get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def add_compliance_job_rule_comment(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        project_id = event_body.get("project_id")
        if project_id is None:
            return {"status_code": 500, "message": "Field project_id is mandatory"}

        session = get_database_session()
        compliance_job = session.query(ComplianceJob).filter(ComplianceJob.project_id == project_id).first()
        if not compliance_job:
            return {"status_code": 500, "message": "Compliance job is not present for this project"}

        compliance_rule_id = event_body.get("compliance_rule_id")
        if compliance_rule_id is None:
            return {"status_code": 500, "message": "Field compliance_rule_id is mandatory"}

        comment = event_body.get("comment")
        if comment is None:
            return {"status_code": 500, "message": "Field comment is mandatory"}

        comment_by = event_body.get("comment_by")
        if comment_by is None:
            return {"status_code": 500, "message": "Field comment_by is mandatory"}

        compliance_job_rule_comment = ComplianceJobRuleComment()
        compliance_job_rule_comment.compliance_job_id = compliance_job.id
        compliance_job_rule_comment.compliance_rule_id = compliance_rule_id
        compliance_job_rule_comment.comment = comment
        compliance_job_rule_comment.comment_by = comment_by

        session.add(compliance_job_rule_comment)
        commit_database_session(session)
        return {"status_code": 201,
                "compliance_job_rule_comment": json.dumps(compliance_job_rule_comment, cls=AlchemyEncoder)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred in adding comment in compliance jon rule"}
    finally:
        if session is not None:
            close_database_session(session)


def update_compliance_job_rule_comment(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        data_to_update = {}

        comment_id = event_body.get("id")
        if comment_id is None:
            return {"status_code": 500, "message": "Field id is mandatory"}

        comment = event_body.get("comment")
        if comment is None:
            return {"status_code": 500, "message": "Field comment is mandatory"}
        else:
            data_to_update["comment"] = event_body.get("comment")

        session = get_database_session()
        session.query(ComplianceJobRuleComment).filter(ComplianceJobRuleComment.id == comment_id).update(data_to_update)
        commit_database_session(session)
        result = session.query(ComplianceJobRuleComment).filter(ComplianceJobRuleComment.id == comment_id).first()
        return json.dumps(result, cls=AlchemyEncoder)
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while ComplianceJobRuleComment"}
    finally:
        if session is not None:
            close_database_session(session)


def get_compliance_job_rule_comment(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        id = path_parameter.get("id")
        if id is None:
            return {"status_code": 500, "message": "Field 'id' is mandatory"}

        session = get_database_session()
        if id is not None:
            result = session.query(ComplianceJobRuleComment).filter(ComplianceJobRuleComment.id == id).all()
            if not result:
                return {"status_code": 404, "message": "Compliance Job Rule Comments does not exist"}
            return {"status_code": 200, "comments": json.dumps(result, cls=AlchemyEncoder)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting project detail"}
    finally:
        if session is not None:
            close_database_session(session)


def get_compliance_job_rule_comments(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        project_id = path_parameter.get("project_id")
        if project_id is None:
            return {"status_code": 500, "message": "Field 'project_id' is mandatory"}

        session = get_database_session()
        compliance_job = session.query(ComplianceJob).filter(ComplianceJob.project_id == project_id).first()
        if not compliance_job:
            return {"status_code": 500, "message": "Compliance job is not present for this project"}

        compliance_job_id = compliance_job.id

        compliance_rule_id = path_parameter.get("compliance_rule_id")
        if compliance_rule_id is None:
            return {"status_code": 500, "message": "Field 'compliance_rule_id' is mandatory"}

        result = session.query(ComplianceJobRuleComment).filter(
            ComplianceJobRuleComment.compliance_job_id == compliance_job_id,
            ComplianceJobRuleComment.compliance_rule_id == compliance_rule_id).all()
        if not result:
            return {"status_code": 404, "message": "Compliance Job Rule Comment {} does not exist".format(id)}
        return {"status_code": 200, "comments": json.dumps(result, cls=AlchemyEncoder)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting project detail"}
    finally:
        if session is not None:
            close_database_session(session)


def delete_compliance_job_rule_comment(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        id = path_parameter.get("id")
        session = get_database_session()
        compliance_job_rule_comment = session.query(ComplianceJobRuleComment).filter(
            ComplianceJobRuleComment.id == id).first()
        if not compliance_job_rule_comment:
            return {"status_code": 404, "message": "Compliance Job Rule Comment {} does not exist".format(id)}
        session.delete(compliance_job_rule_comment)
        commit_database_session(session)
        return {"message": "Compliance Job Rule Comment - {} deleted successfully".format(id)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while deleting compliance job rule comment"}
    finally:
        if session is not None:
            close_database_session(session)
