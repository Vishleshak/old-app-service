import json

from models.json_converter import AlchemyEncoder
from models.user_roles import UserRoles
from utils.db_util import get_database_session, commit_database_session, close_database_session
from utils.event_util import get_body, get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def add_user_role(event, context):
    """This is method for adding User Role Object

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["name", "description"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        user_role = UserRoles()
        user_role.name = event_body.get("name")
        user_role.description = event_body.get("description")
        session = get_database_session()
        session.add(user_role)
        commit_database_session(session)
        return {"status_code": 201, "user_role": json.dumps(user_role, cls=AlchemyEncoder)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred in creating user"}
    finally:
        if session is not None:
            close_database_session(session)


def update_user_role(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["name", "description"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        if event_body.get("id") is not None:
            session = get_database_session()
            data_to_update = {"name": event_body.get("name"), "description": event_body.get("description")}
            session.query(UserRoles).filter(id == event_body.get("id")).update(data_to_update)
            commit_database_session(session)
            return {"status_code": 200}
        return {"status_code": 500, "error": "Error occurred in updating user"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred in updating user role"}
    finally:
        if session is not None:
            close_database_session(session)


def get_user_role(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        record_id = path_parameter.get("id")
        session = get_database_session()
        if id is not None:
            result = session.query(UserRoles).filter(UserRoles.id == record_id).first()
            if not result:
                return {"status_code": 404, "message": "User Role with ID :  {} does not exist".format(record_id)}
            return json.dumps(result, cls=AlchemyEncoder)
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting User Role"}
    finally:
        if session is not None:
            close_database_session(session)


def get_user_roles(event, context):
    session = None
    try:
        logging.debug(event)
        session = get_database_session()
        result = session.query(UserRoles).filter(UserRoles.is_deleted == 0).all()
        if not result:
            return {"status_code": 404, "message": "User Roles does not exist"}
        return json.dumps(result, cls=AlchemyEncoder)
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting User Roles"}
    finally:
        if session is not None:
            close_database_session(session)


def delete_user_role(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        record_id = path_parameter.get("id")
        session = get_database_session()
        result = session.query(UserRoles).filter(UserRoles.id == record_id).first()
        if not result:
            return {"status_code": 404, "message": "User role with ID {} does not exist".format(record_id)}
        session = get_database_session()
        session.delete(result)
        session.commit()
        return {"status_code": 200, "message": "User role with ID {} deleted successfully".format(record_id)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while deleting User Role"}
    finally:
        if session is not None:
            close_database_session(session)
