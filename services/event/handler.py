from models.event import Event
from utils.db_util import get_database_session, commit_database_session, close_database_session
from utils.event_util import get_body, get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def add_event(event, context):
    """This is method for adding events

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        event = Event()
        event.name = event_body.get("name")
        event.description = event_body.get("description")
        session = get_database_session()
        session.add(event)
        commit_database_session(session)
        return {"status_code": 201}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred in creating Event"}
    finally:
        if session is not None:
            close_database_session(session)


def update_event(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        if event_body.get("id") is not None:
            event = Event()
            event.id = event_body.get("id")
            event.name = event_body.get("name")
            event.description = event_body.get("description")
            session = get_database_session()
            session.query(Event).filter(id == event_body.get("id")).update(
                {'name': event_body.get("name"), 'description': event_body.get("description")})
            commit_database_session(session)
        return {"status_code": 500}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred in creating Event"}
    finally:
        if session is not None:
            close_database_session(session)


def get_event(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        id = path_parameter.get("id")
        page = path_parameter.get("page")
        if page is None:
            page = 0
        else:
            page = int(str(page))
        session = get_database_session()
        if id is not None:
            result = session.query(Event).filter(Event.id == id).first()
            if not result:
                return {"status_code": 404, "message": "Event {} does not exist".format(id)}
            return result
        else:
            result = session.query(Event).offset(page * 10).limit(10)
            if not result:
                return {"status_code": 404, "message": "Event {} does not exist".format(id)}
            return result
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting Event"}
    finally:
        if session is not None:
            close_database_session(session)


def delete_kpi(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        id = path_parameter.get("id")
        session = get_database_session()
        result = session.query(Event).filter(Event.id == id).first()
        if not result:
            return {"status_code": 404, "message": "Event {} does not exist".format(id)}
        session.delete(result)
        session.commit()
        return {"message": "Event {} deleted successfully".format(id)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while deleting Event"}
    finally:
        if session is not None:
            close_database_session(session)
