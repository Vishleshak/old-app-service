import json
import uuid

from models.json_converter import AlchemyEncoder
from models.s3_bucket_details import S3BucketDetails
from utils.db_util import get_database_session, commit_database_session, close_database_session
from utils.event_util import get_body, get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def add_s3_bucket_detail(event, context):
    """This is method for adding S3 Bucket Details

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        s3_bucket_details = get_s3_object(event_body)
        uid = str(uuid.uuid4())
        s3_bucket_details.id = uid
        session = get_database_session()
        session.add(s3_bucket_details)
        commit_database_session(session)
        return {"status_code": 201, "data": {"id": uid}}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "data": {"error": "Error occurred in Updating S3 Bucket Details"}}
    finally:
        if session is not None:
            close_database_session(session)


def update_s3_bucket_detail(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        if event_body.get("id") is not None:
            s3_bucket_details = get_s3_object(event_body)
            session = get_database_session()
            session.query(S3BucketDetails).filter(id == event_body.get("id")).update(
                {'bucket_key': event_body.get("bucket_key"), 's3_access_key': event_body.get("s3_access_key"),
                 's3_secret_key': event_body.get("s3_secret_key")})
            commit_database_session(session)
        return {"status_code": 200, "data": {"message": "Updated"}}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "data": {"error": "Error occurred in Updating S3 Bucket Details"}}
    finally:
        if session is not None:
            close_database_session(session)


def get_s3_bucket_detail(event, context):
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        user_id = path_parameter.get("user_id")
        session = get_database_session()
        if user_id is not None:
            result = session.query(S3BucketDetails).filter(S3BucketDetails.user_id == user_id).first()
            if not result:
                return {"status_code": 404, "data": {"message": "S3 Bucket Details {} does not exist".format(user_id)}}
            return {"status_code": 200, "data": json.loads(json.dumps(result, cls=AlchemyEncoder))}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "data": {"error": "Error occurred in Updating S3 Bucket Details"}}


def get_s3_bucket_details_by_project(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        project_id = path_parameter.get("project_id")
        session = get_database_session()
        if project_id is not None:
            result = session.query(S3BucketDetails).filter(S3BucketDetails.project_id == project_id).first()
            if not result:
                return {"status_code": 404,
                        "data": {"message": "S3 Bucket Details for project {} does not exist".format(project_id)}}
            return {"status_code": 200, "data": json.loads(json.dumps(result, cls=AlchemyEncoder))}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "data": {"error": "Error occurred in Updating S3 Bucket Details"}}
    finally:
        if session is not None:
            close_database_session(session)


def delete_s3_bucket_detail(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        id = path_parameter.get("details_id")
        session = get_database_session()
        result = session.query(S3BucketDetails).filter(S3BucketDetails.id == id).first()
        if not result:
            return {"status_code": 404, "message": "S3 Bucket Details {} does not exist".format(id)}
        session.delete(result)
        session.commit()
        return {"message": "S3 Bucket Details{} deleted successfully".format(id)}

    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while deleting S3 Bucket Detail"}
    finally:
        if session is not None:
            close_database_session(session)


def get_s3_object(event_body):
    s3_bucket_details = S3BucketDetails()
    if event_body.get("id") is not None:
        s3_bucket_details.id = event_body.get("id")
    s3_bucket_details.user_id = event_body.get("user_id")
    s3_bucket_details.project_id = event_body.get("project_id")
    s3_bucket_details.bucket_key = event_body.get("bucket_key")
    s3_bucket_details.s3_access_key = event_body.get("s3_access_key")
    s3_bucket_details.s3_secret_key = event_body.get("s3_secret_key")
    return s3_bucket_details
