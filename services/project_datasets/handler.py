import json
import uuid
from urllib import request

import boto3

from models.dataset_details import DatasetDetails
from models.json_converter import AlchemyEncoder
from models.project_dataset_details import ProjectDatasetDetails
from models.project_dataset_storage_details import ProjectDatasetStorageDetails
from models.project_model_details import ProjectModelDetails
from models.project_model_storage_details import ProjectModelStorageDetails
from models.user import User
from utils.aws_util import validate_aws_credentials, validate_s3_access
from utils.db_util import get_database_session, commit_database_session, close_database_session
from utils.event_util import get_body, get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def add_dataset_details(event, context):
    """This is method for adding S3 Bucket Details

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["user_id", "project_id", "bucket_key", "s3_access_key", "s3_secret_key", "dataset_type"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}
        if event_body.get("storage_type") != "kosa":
            if not validate_aws_credentials(event_body.get("s3_access_key"), event_body.get("s3_secret_key")):
                return {"status_code": 50004,
                        "error": "Invalid access key id or secret key is provided please check and enter valid keys"}
            if not validate_s3_access(event_body.get("s3_access_key"), event_body.get("s3_secret_key"),
                                      event_body.get("bucket_key")):
                return {"status_code": 50005,
                        "error": "The given S3 bucket is either not present "
                                 "or you are not having access to the given bucket"}

        dataset_details = get_dataset_details(event_body)
        uid = str(uuid.uuid4())
        dataset_details.id = uid
        session = get_database_session()
        session.add(dataset_details)
        commit_database_session(session)
        return {"status_code": 201, "data": {"id": uid}}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "data": {"error": "Error occurred in adding dataset details"}}


def update_prediction_dataset_details(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["user_id", "project_id", "bucket_key", "s3_access_key", "s3_secret_key", "dataset_type"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}
        if event_body.get("storage_type") != "kosa":
            if not validate_aws_credentials(event_body.get("s3_access_key"), event_body.get("s3_secret_key")):
                return {"status_code": 50004,
                        "error": "Invalid access key id or secret key is provided please check and enter valid keys"}
            if not validate_s3_access(event_body.get("s3_access_key"), event_body.get("s3_secret_key"),
                                      event_body.get("bucket_key")):
                return {"status_code": 50005,
                        "error": "The given S3 bucket is either not present or you are not having "
                                 "access to the given bucket"}

        project_id = event_body.get("project_id")
        session = get_database_session()
        dataset_details_from_db = session.query(DatasetDetails).filter(
            DatasetDetails.project_id == project_id).first()
        if dataset_details_from_db is None:
            dataset_details = get_dataset_details(event_body)
            uid = str(uuid.uuid4())
            dataset_details.id = uid
            session.add(dataset_details)
            commit_database_session(session)
            return {"status_code": 200, "data": {"id": uid}}

        if event_body.get("id") is not None:
            session.query(DatasetDetails).filter(DatasetDetails.id == event_body.get("id")).update(
                {'bucket_key': event_body.get("bucket_key"), 's3_access_key': event_body.get("s3_access_key"),
                 's3_secret_key': event_body.get("s3_secret_key"), 'storage_type': event_body.get("storage_type"),
                 'dataset_type': event_body.get("dataset_type")})
            commit_database_session(session)
            return {"status_code": 200, "data": {"message": "Dataset details are updated successfully"}}
        else:
            return {"status_code": 50001, "error": "Please provide id, it should not be empty",
                    "error_field_name": "id"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "data": {"error": "Error occurred in updating dataset details"}}
    finally:
        if session is not None:
            close_database_session(session)


def get_dataset_details(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        project_id = path_parameter.get("project_id")
        session = get_database_session()
        if project_id is not None:
            result = session.query(DatasetDetails).filter(DatasetDetails.project_id == project_id).first()
            if not result:
                return {"status_code": 404,
                        "data": {"message": "S3 Bucket Details {} does not exist".format(project_id)}}
            return {"status_code": 200, "data": json.loads(json.dumps(result, cls=AlchemyEncoder))}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "data": {"error": "Error occurred in Updating S3 Bucket Details"}}
    finally:
        if session is not None:
            close_database_session(session)


def get_dataset_details(event_body):
    dataset_details = DatasetDetails()
    if event_body.get("id") is not None:
        dataset_details.id = event_body.get("id")
    dataset_details.user_id = event_body.get("user_id")
    dataset_details.project_id = event_body.get("project_id")
    dataset_details.cloud_id = event_body.get("cloud_id")
    dataset_details.storage_type = event_body.get("storage_type")
    dataset_details.bucket_key = event_body.get("bucket_key")
    dataset_details.s3_access_key = event_body.get("s3_access_key")
    dataset_details.s3_secret_key = event_body.get("s3_secret_key")
    dataset_details.s3_secret_key = event_body.get("dataset_type")
    return dataset_details


def list_datasets(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["bucket_key", "s3_access_key", "s3_secret_key"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

            if not validate_aws_credentials(event_body.get("s3_access_key"), event_body.get("s3_secret_key")):
                return {"status_code": 500, "error": "Please provide correct S3 credentials"}

            if not validate_s3_access(event_body.get("s3_access_key"), event_body.get("s3_secret_key"),
                                      event_body.get("bucket_key")):
                return {"status_code": 500, "error": "S3 bucket provided is not accessible"}

            session = boto3.Session(event_body.get("s3_access_key"), event_body.get("s3_secret_key"))
            s3 = session.resource("s3")
            bucket_key = event_body.get("bucket_key")
            bucket = s3.Bucket(bucket_key)
            result = {}
            dataset_list = []
            if bucket_key is not None:
                for obj in bucket.objects.all():
                    data_details = {"bucket_name": obj.bucket_name, "key": obj.key,
                                    "s3_url": "s3://{}/{}".format(obj.bucket_name, obj.key)}
                    dataset_list.append(data_details)
            result["datasets"] = dataset_list
            return {"status_code": 200, "result": result}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Exception occurred while reading S3 bucket"}


def add_dataset_tag(event, context):
    """This is method to add project dataset details

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["datasets", "project_id", "user_id"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        datasets = event_body.get("datasets")
        for dataset in datasets:
            mandatory_fields = ["s3_url", "tag"]
            for mandatory_field in mandatory_fields:
                if dataset.get(mandatory_field) is None:
                    return {"status_code": 50001,
                            "error": "Please provide {}, it should not be empty".format(mandatory_field),
                            "error_field_name": mandatory_field}

        session = get_database_session()
        result = []
        file_urls = []
        for dataset in datasets:
            project_dataset_details = ProjectDatasetDetails()
            project_dataset_details.project_id = event_body.get("project_id")
            project_dataset_details.dataset_path = dataset.get("s3_url")
            project_dataset_details.tag = dataset.get("tag")
            project_dataset_details.user_id = event_body.get("user_id")
            session.add(project_dataset_details)
            commit_database_session(session)
            result.append(project_dataset_details)
            file_urls.append(dataset.get("s3_url"))
        # Send metadata calculation request
        user_details = session.query(User).filter(User.auth_id == event_body.get("user_id")).first()
        req = request.Request("https://apis.kosa.ai/api/metadata", method="POST")
        req.add_header('Content-Type', 'application/json')
        data = {"email": user_details.email, 'file_urls': file_urls, 'project_id': event_body.get("project_id")}
        data = json.dumps(data)
        data = data.encode()
        r = request.urlopen(req, data=data)
        content = r.read()
        logging.debug(content)
        return {"status_code": 200, "data": json.dumps(result, cls=AlchemyEncoder)}
    except Exception as ex:
        return {"status_code": 500, "error": "Exception occurred in storing datasets and tags"}
    finally:
        if session is not None:
            close_database_session(session)


def add_project_dataset_storage(event, context):
    """This is method to add project dataset storage

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["project_id", "storage_type"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}
        if event_body.get("storage_type") == "aws":
            mandatory_fields = ["bucket_key", "s3_access_key", "s3_secret_key"]
            for mandatory_field in mandatory_fields:
                if event_body.get(mandatory_field) is None:
                    return {"status_code": 50001,
                            "error": "Please provide {}, it should not be empty".format(mandatory_field),
                            "error_field_name": mandatory_field}

        session = get_database_session()
        project_dataset_storage_details = session.query(ProjectDatasetStorageDetails).filter(
            ProjectDatasetStorageDetails.project_id == event_body.get("project_id")).first()
        if project_dataset_storage_details is None:
            project_dataset_storage_details = ProjectDatasetStorageDetails()
            project_dataset_storage_details.project_id = event_body.get("project_id")
            project_dataset_storage_details.storage_type = event_body.get("storage_type")
            project_dataset_storage_details.bucket_key = event_body.get("bucket_key")
            project_dataset_storage_details.s3_access_key = event_body.get("s3_access_key")
            project_dataset_storage_details.s3_secret_key = event_body.get("s3_secret_key")
            session.add(project_dataset_storage_details)
        else:
            data_to_update = {"storage_type": event_body.get("storage_type"),
                              "bucket_key": event_body.get("bucket_key"),
                              "s3_access_key": event_body.get("s3_access_key"),
                              "s3_secret_key": event_body.get("s3_secret_key")}
            session.query(ProjectModelStorageDetails).filter(ProjectModelStorageDetails.project_id ==
                                                             event_body.get("project_id")).update(data_to_update)
        commit_database_session(session)
        return {"status_code": 200, "data": json.dumps(project_dataset_storage_details, cls=AlchemyEncoder)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Exception occurred in storing datasets details"}
    finally:
        if session is not None:
            close_database_session(session)


def get_project_dataset_storage(event, context):
    """This is method to add project dataset storage

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        path_parameter = get_path_parameters(event)
        project_id = path_parameter.get("project_id")
        if project_id is None:
            return {"status_code": 50001,
                    "error": "Please provide project_id, it should not be empty",
                    "error_field_name": "project_id"}
        session = get_database_session()
        project_dataset_storage_details = session.query(ProjectDatasetStorageDetails).filter(
            ProjectDatasetStorageDetails.project_id == project_id).first()
        if project_dataset_storage_details is None:
            project_dataset_storage_details = ProjectDatasetStorageDetails()
            return {"status_code": 200, "data": json.dumps(project_dataset_storage_details, cls=AlchemyEncoder)}
        return {"status_code": 200, "data": json.dumps(project_dataset_storage_details, cls=AlchemyEncoder)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Exception occurred in getting datasets storage details"}
    finally:
        if session is not None:
            close_database_session(session)


def get_project_model_storage(event, context):
    """This is method to add project dataset storage

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        path_parameter = get_path_parameters(event)
        project_id = path_parameter.get("project_id")
        if project_id is None:
            return {"status_code": 50001,
                    "error": "Please provide project_id, it should not be empty",
                    "error_field_name": "project_id"}
        session = get_database_session()
        project_model_storage_details = session.query(ProjectModelStorageDetails).filter(
            ProjectModelStorageDetails.project_id == project_id).first()
        if project_model_storage_details is None:
            project_model_storage_details = ProjectModelStorageDetails()
            return {"status_code": 200, "data": json.dumps(project_model_storage_details, cls=AlchemyEncoder)}
        return {"status_code": 200, "data": json.dumps(project_model_storage_details, cls=AlchemyEncoder)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Exception occurred in getting model storage details"}
    finally:
        if session is not None:
            close_database_session(session)


def add_project_model_storage(event, context):
    """This is method to add project model storage

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["project_id", "storage_type"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        if event_body.get("storage_type") == "aws":
            mandatory_fields = ["bucket_key", "s3_access_key", "s3_secret_key"]
            for mandatory_field in mandatory_fields:
                if event_body.get(mandatory_field) is None:
                    return {"status_code": 50001,
                            "error": "Please provide {}, it should not be empty".format(mandatory_field),
                            "error_field_name": mandatory_field}

        session = get_database_session()
        project_model_storage_details = session.query(ProjectModelStorageDetails).filter(
            ProjectModelStorageDetails.project_id == event_body.get("project_id")).first()
        if project_model_storage_details is None:
            project_model_storage_details = ProjectModelStorageDetails()
            project_model_storage_details.project_id = event_body.get("project_id")
            project_model_storage_details.storage_type = event_body.get("storage_type")
            project_model_storage_details.bucket_key = event_body.get("bucket_key")
            project_model_storage_details.s3_access_key = event_body.get("s3_access_key")
            project_model_storage_details.s3_secret_key = event_body.get("s3_secret_key")
            session.add(project_model_storage_details)
        else:
            data_to_update = {"storage_type": event_body.get("storage_type"),
                              "bucket_key": event_body.get("bucket_key"),
                              "s3_access_key": event_body.get("s3_access_key"),
                              "s3_secret_key": event_body.get("s3_secret_key")}
            session.query(ProjectModelStorageDetails).filter(ProjectModelStorageDetails.project_id ==
                                                             event_body.get("project_id")).update(data_to_update)
        commit_database_session(session)
        return {"status_code": 200, "data": json.dumps(project_model_storage_details, cls=AlchemyEncoder)}
    except Exception as ex:
        return {"status_code": 500, "error": "Exception occurred in storing model details"}
    finally:
        if session is not None:
            close_database_session(session)


def list_models(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["bucket_key", "s3_access_key", "s3_secret_key"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

            if not validate_aws_credentials(event_body.get("s3_access_key"), event_body.get("s3_secret_key")):
                return {"status_code": 500, "error": "Please provide correct S3 credentials"}

            if not validate_s3_access(event_body.get("s3_access_key"), event_body.get("s3_secret_key"),
                                      event_body.get("bucket_key")):
                return {"status_code": 500, "error": "S3 bucket provided is not accessible"}

            session = boto3.Session(event_body.get("s3_access_key"), event_body.get("s3_secret_key"))
            s3 = session.resource("s3")
            bucket_key = event_body.get("bucket_key")
            bucket = s3.Bucket(bucket_key)
            result = {}
            dataset_list = []
            if bucket_key is not None:
                for obj in bucket.objects.all():
                    data_details = {"bucket_name": obj.bucket_name, "key": obj.key,
                                    "s3_url": "s3://{}/{}".format(obj.bucket_name, obj.key)}
                    dataset_list.append(data_details)
            result["datasets"] = dataset_list
            return {"status_code": 200, "result": result}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Exception occurred while reading S3 bucket"}


def add_model_tag(event, context):
    """This is method to add project dataset details

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["datasets", "project_id", "user_id"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        datasets = event_body.get("datasets")
        for dataset in datasets:
            mandatory_fields = ["s3_url", "tag"]
            for mandatory_field in mandatory_fields:
                if dataset.get(mandatory_field) is None:
                    return {"status_code": 50001,
                            "error": "Please provide {}, it should not be empty".format(mandatory_field),
                            "error_field_name": mandatory_field}

        session = get_database_session()
        result = []
        for dataset in datasets:
            project_model_details = ProjectModelDetails()
            project_model_details.project_id = event_body.get("project_id")
            project_model_details.model_path = dataset.get("s3_url")
            project_model_details.tag = dataset.get("tag")
            project_model_details.user_id = event_body.get("user_id")
            session.add(project_model_details)
            commit_database_session(session)
            result.append(project_model_details)
        return {"status_code": 200, "data": json.dumps(result, cls=AlchemyEncoder)}
    except Exception as ex:
        return {"status_code": 500, "error": "Exception occurred in storing models and tags"}
    finally:
        if session is not None:
            close_database_session(session)


def list_project_datasets(event, context):
    db_session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        project_id = path_parameter.get("project_id")
        dataset_type = path_parameter.get("dataset_type")
        if project_id is None:
            return {"status_code": 50001,
                    "error": "Please provide project_id, it should not be empty", "error_field_name": "project_id"}
        if dataset_type is None:
            return {"status_code": 50001,
                    "error": "Please provide dataset_type, it should not be empty", "error_field_name": "dataset_type"}
        s3_access_key = None
        s3_secret_key = None
        bucket_key = None
        aws_storage = False
        datasets = None
        result = {}
        db_session = get_database_session()
        if dataset_type == "model":
            project_model_storage_details = db_session.query(ProjectModelStorageDetails).filter(
                ProjectModelStorageDetails.project_id == project_id).first()
            if project_model_storage_details is None:
                return {"status_code": 500,
                        "error": "Not able to find model details for the given project"}
            if project_model_storage_details is not None and project_model_storage_details.storage_type == "aws":
                s3_access_key = project_model_storage_details.s3_access_key
                s3_secret_key = project_model_storage_details.s3_secret_key
                bucket_key = project_model_storage_details.bucket_key
                aws_storage = True
            datasets = db_session.query(ProjectModelDetails).filter(
                ProjectModelDetails.project_id == project_id).all()
        else:
            project_dataset_storage_details = db_session.query(ProjectDatasetStorageDetails).filter(
                ProjectDatasetStorageDetails.project_id == project_id).first()
            if project_dataset_storage_details is None:
                return {"status_code": 500,
                        "error": "Not able to find dataset details for the given project"}
            if project_dataset_storage_details is not None and project_dataset_storage_details.storage_type == "aws":
                s3_access_key = project_dataset_storage_details.s3_access_key
                s3_secret_key = project_dataset_storage_details.s3_secret_key
                bucket_key = project_dataset_storage_details.bucket_key
                aws_storage = True
            datasets = db_session.query(ProjectDatasetDetails).filter(
                ProjectDatasetDetails.project_id == project_id).all()
        if aws_storage:
            dataset_map = {}
            if datasets is not None and dataset_type == "model":
                for dataset in datasets:
                    dataset_map[dataset.model_path] = dataset
            if datasets is not None and dataset_type == "dataset":
                for dataset in datasets:
                    dataset_map[dataset.dataset_path] = dataset

            session = boto3.Session(s3_access_key, s3_secret_key)
            s3 = session.resource("s3")
            bucket = s3.Bucket(bucket_key)

            dataset_list = []
            if bucket_key is not None:
                for obj in bucket.objects.all():
                    data_details = {"bucket_name": obj.bucket_name, "key": obj.key,
                                    "s3_url": "s3://{}/{}".format(obj.bucket_name, obj.key)}
                    if dataset_map.get(data_details.get("s3_url")) is not None:
                        data_details["tag"] = dataset.tag
                        data_details["id"] = dataset.id
                    dataset_list.append(data_details)
            result["datasets"] = dataset_list
            return {"status_code": 200, "result": result}
        else:
            dataset_list = []
            if datasets is not None and dataset_type == "model":
                for dataset in datasets:
                    data_details = {"s3_url": dataset.model_path, "key": dataset.model_path.split("/")[-1],
                                    "id": dataset.id, "tag": dataset.tag}
                    dataset_list.append(data_details)
            if datasets is not None and dataset_type == "dataset":
                for dataset in datasets:
                    data_details = {"s3_url": dataset.dataset_path, "key": dataset.dataset_path.split("/")[-1],
                                    "id": dataset.id, "tag": dataset.tag}
                    dataset_list.append(data_details)
            result["datasets"] = dataset_list
            return {"status_code": 200, "result": result}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Exception occurred while reading S3 bucket"}
    finally:
        if db_session is not None:
            close_database_session(db_session)


def update_dataset_tag(event, context):
    """This is method to add project dataset details

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["id", "project_id", "tag"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        session = get_database_session()
        result = session.query(ProjectDatasetDetails).filter(ProjectDatasetDetails.id == event_body.get("id")).first()
        if result is None:
            return {"status_code": 50003, "error": "The project dataset details are not present"}
        data_to_update = {"tag": event_body.get("tag")}
        session.query(ProjectDatasetDetails).filter(ProjectDatasetDetails.id == event_body.get("id")).update(
            data_to_update)
        commit_database_session(session)
        return {"status_code": 200, "message": "Tag updated successfully"}
    except Exception as ex:
        return {"status_code": 500, "error": "Exception occurred in storing datasets and tags"}
    finally:
        if session is not None:
            close_database_session(session)


def update_model_tag(event, context):
    """This is method to add project dataset details

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["id", "project_id", "tag"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        session = get_database_session()
        result = session.query(ProjectModelDetails).filter(ProjectModelDetails.id == event_body.get("id")).first()
        if result is None:
            return {"status_code": 50003, "error": "The project dataset details are not present"}
        data_to_update = {"tag": event_body.get("tag")}
        session.query(ProjectModelDetails).filter(ProjectModelDetails.id == event_body.get("id")).update(data_to_update)
        commit_database_session(session)
        return {"status_code": 200, "message": "Tag updated successfully"}
    except Exception as ex:
        return {"status_code": 500, "error": "Exception occurred in storing models and tags"}
    finally:
        if session is not None:
            close_database_session(session)


def delete_dataset(event, context):
    """This is method to add project dataset details

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        dataset_id = path_parameter.get("dataset_id")
        if dataset_id is None:
            return {"status_code": 50001,
                    "error": "Please provide dataset_id, it should not be empty",
                    "error_field_name": "dataset_id"}

        session = get_database_session()
        result = session.query(ProjectDatasetDetails).filter(ProjectDatasetDetails.id == dataset_id).first()
        if not result:
            return {"status_code": 404, "message": "Project dataset details are not found"}
        session.delete(result)
        session.commit()
        return {"status_code": 200, "data": json.dumps(result, cls=AlchemyEncoder)}
    except Exception as ex:
        return {"status_code": 500, "error": "Exception occurred in storing datasets and tags"}
    finally:
        if session is not None:
            close_database_session(session)


def delete_model(event, context):
    """This is method to add project dataset details

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        model_id = path_parameter.get("model_id")
        if model_id is None:
            return {"status_code": 50001,
                    "error": "Please provide model_id, it should not be empty",
                    "error_field_name": "model_id"}

        session = get_database_session()
        result = session.query(ProjectModelDetails).filter(ProjectModelDetails.id == model_id).first()
        if not result:
            return {"status_code": 404, "message": "Project model details are not found"}
        session.delete(result)
        session.commit()
        return {"status_code": 200, "data": json.dumps(result, cls=AlchemyEncoder)}
    except Exception as ex:
        return {"status_code": 500, "error": "Exception occurred in storing datasets and tags"}
    finally:
        if session is not None:
            close_database_session(session)


def list_tagged_project_datasets(event, context):
    db_session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        project_id = path_parameter.get("project_id")
        dataset_type = path_parameter.get("dataset_type")
        if project_id is None:
            return {"status_code": 50001,
                    "error": "Please provide project_id, it should not be empty", "error_field_name": "project_id"}
        if dataset_type is None:
            return {"status_code": 50001,
                    "error": "Please provide dataset_type, it should not be empty", "error_field_name": "dataset_type"}
        datasets = None
        result = []
        db_session = get_database_session()
        if dataset_type == "model":
            project_model_storage_details = db_session.query(ProjectModelStorageDetails).filter(
                ProjectModelStorageDetails.project_id == project_id).first()
            if project_model_storage_details is None:
                return {"status_code": 500,
                        "error": "Not able to find model details for the given project"}
            datasets = db_session.query(ProjectModelDetails.id, ProjectModelDetails.project_id,
                                        ProjectModelDetails.user_id, ProjectModelDetails.model_path,
                                        ProjectModelDetails.tag, ProjectModelDetails.created_at, User.first_name,
                                        User.last_name).filter(
                ProjectModelDetails.project_id == project_id).join(
                User, ProjectModelDetails.user_id == User.auth_id).all()
            if datasets is not None:
                for dataset in datasets:
                    data_details = {"id": dataset[0], "s3_url": dataset[3],
                                    "key": dataset[3].split("/")[-1], "tag": dataset[4],
                                    "created_at": dataset[5].strftime("%m/%d/%Y %H:%M:%S"),
                                    "user_first_name": dataset[6], "user_last_name": dataset[7]}
                    result.append(data_details)
        else:
            project_dataset_storage_details = db_session.query(ProjectDatasetStorageDetails).filter(
                ProjectDatasetStorageDetails.project_id == project_id).first()
            if project_dataset_storage_details is None:
                return {"status_code": 500,
                        "error": "Not able to find dataset details for the given project"}
            datasets = db_session.query(ProjectDatasetDetails.id, ProjectDatasetDetails.project_id,
                                        ProjectDatasetDetails.user_id, ProjectDatasetDetails.dataset_path,
                                        ProjectDatasetDetails.tag, ProjectDatasetDetails.created_at, User.first_name,
                                        User.last_name, ProjectDatasetDetails.target_variable_detected,
                                        ProjectDatasetDetails.sensitive_attribute_detected,
                                        ProjectDatasetDetails.metadata_calculated,
                                        ProjectDatasetDetails.metadata_calculation_issue).filter(
                ProjectDatasetDetails.project_id == project_id).join(
                User, ProjectDatasetDetails.user_id == User.auth_id).all()

            if datasets is not None:
                for dataset in datasets:
                    data_details = {"id": dataset[0], "s3_url": dataset[3],
                                    "key": dataset[3].split("/")[-1], "tag": dataset[4],
                                    "created_at": dataset[5].strftime("%m/%d/%Y %H:%M:%S"),
                                    "user_first_name": dataset[6], "user_last_name": dataset[7],
                                    "target_variable_detected": dataset[8], "sensitive_attribute_detected": dataset[9],
                                    "metadata_calculated": dataset[10], "metadata_calculation_issue": dataset[11],
                                    }
                    result.append(data_details)
        return {"status_code": 200, "datasets": result}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Exception occurred while reading S3 bucket"}
    finally:
        if db_session is not None:
            close_database_session(db_session)
