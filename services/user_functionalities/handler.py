import json

from models.app_functionality import AppFunctionality
from models.json_converter import AlchemyEncoder
from utils.db_util import get_database_session, commit_database_session, close_database_session
from utils.event_util import get_body, get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def add_app_functionality(event, context):
    """This is method for adding KPI and fairness metrics

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        functionality = AppFunctionality()
        functionality.name = event_body.get("name")
        session = get_database_session()
        session.add(functionality)
        commit_database_session(session)
        return {"status_code": 201}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred in creating functionality"}
    finally:
        if session is not None:
            close_database_session(session)


def update_app_functionality(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        if event_body.get("id") is not None:
            functionality = AppFunctionality()
            functionality.id = event_body.get("id")
            functionality.name = event_body.get("name")
            session = get_database_session()
            session.query(functionality).filter(id == event_body.get("id")).update({'name': event_body.get("name")})
            commit_database_session(session)
        return {"status_code": 500}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred in updating functionality"}
    finally:
        if session is not None:
            close_database_session(session)


def get_app_functionality(event, context):
    session = None
    try:
        logging.debug(event)
        session = get_database_session()
        result = session.query(AppFunctionality).all()
        if not result:
            return {"status_code": 404, "message": "App functionality {} does not exist".format(id)}
        return json.dumps(result, cls=AlchemyEncoder)
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting App functionality"}
    finally:
        if session is not None:
            close_database_session(session)


def delete_app_functionality(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        id = path_parameter.get("id")
        session = get_database_session()
        result = session.query(AppFunctionality).filter(AppFunctionality.id == id).first()
        if not result:
            return {"status_code": 404, "message": "KPI and fairness metrics {} does not exist".format(id)}
        session.delete(result)
        session.commit()
        return {"message": "KPI and fairness metrics {} deleted successfully".format(id)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while deleting KPI and fairness metrics"}
    finally:
        if session is not None:
            close_database_session(session)
