import json

from models.data_editor_global_setting import DataEditorGlobalSetting
from models.data_editor_user_setting import DataEditorUserSetting
from models.json_converter import AlchemyEncoder
from models.project_detail import ProjectDetail
from utils.db_util import get_database_session, commit_database_session, close_database_session
from utils.event_util import get_path_parameters, get_body
from utils.log_util import get_logger

logging = get_logger(__name__)


def get_user_settings(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        user_id = path_parameter.get("user_id")
        project_id = path_parameter.get("project_id")

        mandatory_fields = ["user_id", "project_id"]
        for mandatory_field in mandatory_fields:
            if path_parameter.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        session = get_database_session()

        data_editor_user_settings = session.query(DataEditorUserSetting).filter(
            DataEditorUserSetting.user_id == user_id, DataEditorUserSetting.project_id == project_id).first()
        if data_editor_user_settings is not None:
            return {"user_settings": json.dumps(data_editor_user_settings, cls=AlchemyEncoder), "global_settings": None}
        else:
            project_details = session.query(ProjectDetail).filter(ProjectDetail.id == project_id).first()
            if not project_details:
                return {"status_code": 404, "message": "Project with ID {} does not exist".format(project_id)}

            data_editor_global_settings = session.query(DataEditorGlobalSetting).filter(
                DataEditorGlobalSetting.use_case_id == project_details.use_case).first()
            if not data_editor_global_settings:
                return {"status_code": 404, "message": "Data editor user and global settings are missing"}
            return {"global_settings": json.dumps(data_editor_global_settings, cls=AlchemyEncoder),
                    "user_settings": None}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting data editor user settings"}
    finally:
        if session is not None:
            close_database_session(session)


def add_user_settings(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["statistical_parity_difference", "disparate_impact",
                            "smoothed_empirical_differential_fairness", "disparate_impact_index",
                            "user_id", "project_id"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        session = get_database_session()
        data_editor_user_setting = DataEditorUserSetting()
        data_editor_user_setting.statistical_parity_difference = event_body.get("statistical_parity_difference")
        data_editor_user_setting.disparate_impact = event_body.get("disparate_impact")
        data_editor_user_setting.smoothed_empirical_differential_fairness = event_body.get(
            "smoothed_empirical_differential_fairness")
        data_editor_user_setting.disparate_impact_index = event_body.get("disparate_impact_index")
        data_editor_user_setting.user_id = event_body.get("user_id")
        data_editor_user_setting.project_id = event_body.get("project_id")
        session = get_database_session()
        session.add(data_editor_user_setting)
        commit_database_session(session)
        return {"status_code": 201}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred in creating data editor user setting"}
    finally:
        if session is not None:
            close_database_session(session)


def update_user_settings(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["statistical_parity_difference", "disparate_impact",
                            "smoothed_empirical_differential_fairness", "disparate_impact_index",
                            "user_id", "project_id"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        session = get_database_session()
        data_to_update = {"statistical_parity_difference": event_body.get("statistical_parity_difference"),
                          "disparate_impact": event_body.get("disparate_impact"),
                          "smoothed_empirical_differential_fairness": event_body.get(
                              "smoothed_empirical_differential_fairness"),
                          "disparate_impact_index": event_body.get("disparate_impact_index")}
        data_editor_user_settings = session.query(DataEditorUserSetting).filter(
            DataEditorUserSetting.user_id == event_body.get("user_id"),
            DataEditorUserSetting.project_id == event_body.get("project_id")).first()

        if not data_editor_user_settings:
            return {"status_code": 404, "error": "Data editor user settings did not found"}
        session.query(DataEditorUserSetting).filter(
            DataEditorUserSetting.user_id == event_body.get("user_id"),
            DataEditorUserSetting.project_id == event_body.get("project_id")).update(data_to_update)
        commit_database_session(session)
        return {"status_code": 201, "message": "Data editor user settings updated successfully"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while updating data editor user settings"}
    finally:
        if session is not None:
            close_database_session(session)
