from models.module_access_details import ModuleAccessDetail
from utils.db_util import get_database_session, commit_database_session, close_database_session
from utils.event_util import get_body, get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def add_module_access_details(event, context):
    """This is method for adding module access details

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["user_id", "module_id"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        module_id = event_body.get("module_id")
        if module_id == "data_editor":
            module_id = 1
        if module_id == "audit_tool":
            module_id = 2
        session = get_database_session()
        module_access_details = ModuleAccessDetail()
        module_access_details.user_id = event_body.get("user_id")
        module_access_details.module_id = module_id
        session.add(module_access_details)
        commit_database_session(session)
        return {"status_code": 201}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error in adding module access details"}
    finally:
        if session is not None:
            close_database_session(session)


def get_module_access_details(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        user_id = path_parameter.get("user_id")
        module_name = path_parameter.get("module_name")
        if user_id is None:
            return {"status_code": 50001,
                    "error": "Please provide user_id, it should not be empty",
                    "error_field_name": "user_id"}
        if module_name is None:
            return {"status_code": 50001,
                    "error": "Please provide module_name, it should not be empty",
                    "error_field_name": "module_name"}
        if module_name not in ["data_editor", "audit_tool"]:
            return {"status_code": 50001,
                    "error": "module_name must be data_editor or audit_tool",
                    "error_field_name": "module_name"}

        module_id = None
        if module_name == "data_editor":
            module_id = 1
        if module_name == "audit_tool":
            module_id = 2

        session = get_database_session()
        module_access_details = session.query(ModuleAccessDetail).filter(
            ModuleAccessDetail.user_id == user_id, ModuleAccessDetail.module_id == module_id).first()
        if not module_access_details:
            return {"status_code": 200, "module_accessed": False}
        return {"status_code": 200, "module_accessed": True}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting module access details"}
    finally:
        if session is not None:
            close_database_session(session)
