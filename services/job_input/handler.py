import os
import uuid
from datetime import datetime

import boto3
from boto3.dynamodb.conditions import Key

from utils.event_util import get_body, get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def get_jobs(event, context):
    logging.debug(event)
    path_parameter = get_path_parameters(event)
    project_id = path_parameter.get("id")
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table('kosa_jobs_' + os.environ['STAGE'])
    response = table.query(KeyConditionExpression=Key('project_id').eq(project_id), Limit=10, ScanIndexForward=False)
    return response['Items']


def get_audit_tool_jobs(event, context):
    logging.debug(event)
    path_parameter = get_path_parameters(event)
    project_id = path_parameter.get("id")
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table('audit_tool_jobs_' + os.environ['STAGE'])
    response = table.query(KeyConditionExpression=Key('project_id').eq(project_id),
                           FilterExpression=Key('type').eq('audit_tool'), Limit=10, ScanIndexForward=False)
    return response['Items']


def get_data_editor_jobs(event, context):
    logging.debug(event)
    path_parameter = get_path_parameters(event)
    project_id = path_parameter.get("id")
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table('data_editor_jobs_' + os.environ['STAGE'])
    response = table.query(KeyConditionExpression=Key('project_id').eq(project_id),
                           FilterExpression=Key('type').eq('data_editor_after_mitigation') | Key('type').eq(
                               'data_editor_before_mitigation'), Limit=10, ScanIndexForward=False)
    return response['Items']


def get_metadata_jobs(event, context):
    logging.debug(event)
    path_parameter = get_path_parameters(event)
    project_id = path_parameter.get("id")
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table('metadata_jobs_' + os.environ['STAGE'])
    response = table.query(KeyConditionExpression=Key('project_id').eq(project_id), Limit=10, ScanIndexForward=False)
    return response['Items']


def get_job(event, context):
    logging.debug(event)
    path_parameter = get_path_parameters(event)
    job_id = path_parameter.get("id")
    job_type = path_parameter.get("job_type")
    dynamodb = boto3.resource('dynamodb')
    table = None
    if job_type == "data_editor":
        table = dynamodb.Table('data_editor_jobs_' + os.environ['STAGE'])
    if job_type == "audit_tool":
        table = dynamodb.Table('audit_tool_jobs_' + os.environ['STAGE'])
    if job_type == "metadata_job":
        table = dynamodb.Table('metadata_job_output_' + os.environ['STAGE'])
    response = table.scan(FilterExpression=Key('id').eq(job_id))
    return response['Items']


def get_job_status(event, context):
    logging.debug(event)
    path_parameter = get_path_parameters(event)
    job_id = path_parameter.get("id")
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table('kosa_jobs_execution_status_' + os.environ['STAGE'])
    response = table.query(KeyConditionExpression=Key('job_id').eq(job_id))
    return response['Items']


def get_data_editor_job_output(event, context):
    logging.debug(event)
    path_parameter = get_path_parameters(event)
    job_id = path_parameter.get("id")
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table('data_editor_job_output_' + os.environ['STAGE'])
    output = table.query(KeyConditionExpression=Key('job_id').eq(job_id))
    result = {"output": output['Items']}
    table = dynamodb.Table('kosa_jobs_execution_status_' + os.environ['STAGE'])
    response = table.scan(FilterExpression=Key('job_id').eq(job_id))
    result["execution_status"] = response['Items']
    return result


def get_audit_tool_job_output(event, context):
    logging.debug(event)
    path_parameter = get_path_parameters(event)
    job_id = path_parameter.get("id")
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table('audit_tool_job_output_' + os.environ['STAGE'])
    # response = table.query(KeyConditionExpression=Key('job_id').eq(job_id))
    output = table.query(KeyConditionExpression=Key('job_id').eq(job_id))
    result = {"output": output['Items']}
    table = dynamodb.Table('kosa_jobs_execution_status_' + os.environ['STAGE'])
    response = table.scan(FilterExpression=Key('job_id').eq(job_id))
    result["execution_status"] = response['Items']
    return result
    # return response['Items']


def create_job(event, context):
    try:
        logging.debug(event)
        event_body = get_body(event)
        logging.debug(event_body)
        response = None
        job_type = event_body.get("job_type")
        dynamodb = boto3.resource('dynamodb')
        table = None
        if job_type == "data_editor_before_mitigation" or job_type == "data_editor_after_mitigation":
            table = dynamodb.Table('data_editor_jobs_' + os.environ['STAGE'])
        if job_type == "audit_tool":
            table = dynamodb.Table('audit_tool_jobs_' + os.environ['STAGE'])
        if job_type == "metadata_job":
            table = dynamodb.Table('metadata_jobs_' + os.environ['STAGE'])
        job_data = create_job_object(event_body)
        response = table.put_item(Item=job_data)
        send_sqs_message(job_data)
        return response
    except Exception as ex:
        logging.error(ex)
        return ex


def create_job_object(event_body):
    try:
        job = {'id': str(uuid.uuid4()), 'project_id': event_body.get('project_id'), 'type': event_body.get('job_type'),
               'created_at': datetime.now().isoformat(), 'is_successful': False}

        if event_body.get("job_type") == "data_editor_before_mitigation" or event_body.get(
                "job_type") == "data_editor_after_mitigation":
            data_editor_job = {'dataframe': event_body.get('dataframe'), 'project_id': event_body.get('project_id'),
                               'label_name': event_body.get('label_name'),
                               'favorable_classes': event_body.get('favorable_classes'),
                               'protected_attribute_names': event_body.get('protected_attribute_names'),
                               'privileged_classes': event_body.get('privileged_classes'),
                               'Mitigation_strategy': event_body.get('Mitigation_strategy'),
                               'email': event_body.get('email'), 'user_name': event_body.get('user_name'),
                               'before_mitigation_execution_id': event_body.get('before_mitigation_execution_id')}
            job['input_params'] = data_editor_job

        if event_body.get("job_type") == "audit_tool":
            audit_tool_job = {}
            audit_tool_job['dataframe'] = event_body.get('dataframe')
            audit_tool_job['project_id'] = event_body.get('project_id')
            audit_tool_job['label_name'] = event_body.get('label_name')
            audit_tool_job['favorable_classes'] = event_body.get('favorable_classes')
            audit_tool_job['performance'] = event_body.get('performance')
            audit_tool_job['fairness'] = event_body.get('fairness')
            audit_tool_job['charts'] = event_body.get('charts')
            audit_tool_job['protected_attribute_names'] = event_body.get('protected_attribute_names')
            audit_tool_job['file'] = event_body.get('file')
            audit_tool_job['privileged_classes'] = event_body.get('privileged_classes')
            audit_tool_job['email'] = event_body.get('email')
            audit_tool_job['user_name'] = event_body.get('user_name')
            job['input_params'] = audit_tool_job

        if event_body.get("job_type") == "metadata_job":
            metadata_job = {'file_url': event_body.get('file_url'), 'bucket_name': event_body.get('bucket_name'),
                            'bucket_path': event_body.get('bucket_path')}
            job['input_params'] = metadata_job

        return job
    except Exception as ex:
        logging.error(ex)
    return None


def send_sqs_message(job_data):
    try:
        # Create SQS client
        sqs = boto3.client('sqs')
        queue_url = 'https://sqs.eu-west-1.amazonaws.com/085964451742/KOSA-Jobs-' + os.environ['STAGE']
        # Send message to SQS queue
        response = sqs.send_message(
            QueueUrl=queue_url,
            MessageBody=str(job_data)
        )
    except Exception as ex:
        logging.error(ex)
