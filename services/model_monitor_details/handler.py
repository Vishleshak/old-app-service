import json
import os
import secrets

import boto3
from jinja2 import Environment, FileSystemLoader

from models.json_converter import AlchemyEncoder
from models.model_monitor_details import ModelMonitorDetails
from models.model_monitor_notification_details import ModelMonitorNotificationDetails
from models.model_monitor_webhook_api_key import ModelMonitorWebhookApiKey
from models.model_monitor_webhook_ip import ModelMonitorWebhookWhitelistIP
from models.user import User
from models.project_detail import ProjectDetail
from utils.db_util import get_database_session, commit_database_session, close_database_session
from utils.event_util import get_body, get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def add_model_monitor_details(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["user_id", "project_id", "use_webhook", "use_dataset", "by_email"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        by_slack = event_body.get("by_slack")
        if by_slack is not None and by_slack:
            mandatory_fields = ["slack_api_token", "slack_channel_name"]
            for mandatory_field in mandatory_fields:
                if event_body.get(mandatory_field) is None:
                    return {"status_code": 50001,
                            "error": "Please provide {}, it should not be empty".format(mandatory_field),
                            "error_field_name": mandatory_field}

        session = get_database_session()
        # Add model monitor details
        model_monitor_details = ModelMonitorDetails()
        model_monitor_details.user_id = event_body.get("user_id")
        model_monitor_details.project_id = event_body.get("project_id")
        model_monitor_details.use_webhook = event_body.get("use_webhook")
        model_monitor_details.use_dataset = event_body.get("use_dataset")
        session.add(model_monitor_details)
        api_key = None
        # Add model monitor webhook details when use_webhook flag is true
        if event_body.get("use_webhook") is not None and event_body.get("use_webhook"):
            api_key = secrets.token_hex(64)
            model_monitor_webhook_api_key = ModelMonitorWebhookApiKey()
            model_monitor_webhook_api_key.user_id = event_body.get("user_id")
            model_monitor_webhook_api_key.project_id = event_body.get("project_id")
            model_monitor_webhook_api_key.webhook_api_key = api_key
            session.add(model_monitor_webhook_api_key)

            model_monitor_webhook_whitelist_ip = ModelMonitorWebhookWhitelistIP()
            model_monitor_webhook_whitelist_ip.user_id = event_body.get("user_id")
            model_monitor_webhook_whitelist_ip.project_id = event_body.get("project_id")
            model_monitor_webhook_whitelist_ip.ip_address = event_body.get("ip_address")
            session.add(model_monitor_webhook_whitelist_ip)

        model_monitor_notification_details = ModelMonitorNotificationDetails()
        model_monitor_notification_details.user_id = event_body.get("user_id")
        model_monitor_notification_details.project_id = event_body.get("project_id")
        model_monitor_notification_details.by_email = event_body.get("by_email")
        model_monitor_notification_details.by_slack = by_slack
        if event_body.get("emails") is not None:
            emails = event_body.get("emails")
            emails = emails.replace(" ", "")
            model_monitor_notification_details.emails = emails
        model_monitor_notification_details.slack_api_token = event_body.get("slack_api_token")
        model_monitor_notification_details.slack_channel_name = event_body.get("slack_channel_name")
        session.add(model_monitor_notification_details)

        commit_database_session(session)
        if api_key is not None:
            send_api_key_email(event_body.get("user_id"), event_body.get("project_id"), api_key)
        return {"status_code": 201, "data": json.dumps(model_monitor_details, cls=AlchemyEncoder)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "data": {"error": "Error occurred in adding model monitor details"}}
    finally:
        if session is not None:
            close_database_session(session)


def get_model_monitor_details(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        project_id = path_parameter.get("project_id")
        session = get_database_session()
        if project_id is not None:
            result = session.query(ModelMonitorDetails.use_webhook, ModelMonitorDetails.use_dataset).filter(
                ModelMonitorDetails.project_id == project_id).first()
            if not result:
                return {"status_code": 200, "data": None}
            model_monitor_details = {"use_webhook": result[0], "use_dataset": result[1]}

            notification_details = {}
            model_monitor_notification_details = session.query(ModelMonitorNotificationDetails.by_email,
                                                               ModelMonitorNotificationDetails.emails,
                                                               ModelMonitorNotificationDetails.by_slack,
                                                               ModelMonitorNotificationDetails.slack_channel_name).filter(
                ModelMonitorNotificationDetails.project_id == project_id).first()
            if model_monitor_notification_details is not None:
                notification_details["by_email"] = model_monitor_notification_details[0]
                emails_from_db = model_monitor_notification_details[1]
                if emails_from_db is not None:
                    notification_details["emails"] = emails_from_db.split(",")
                notification_details["by_slack"] = model_monitor_notification_details[2]
                notification_details["slack_channel_name"] = model_monitor_notification_details[3]

            whitelisted_ips = []
            model_monitor_whitelisted_ips = session.query(ModelMonitorWebhookWhitelistIP.ip_address).filter(
                ModelMonitorWebhookWhitelistIP.project_id == project_id,
                ModelMonitorWebhookWhitelistIP.is_deleted == 0).all()
            if model_monitor_whitelisted_ips is not None:
                for model_monitor_whitelisted_ip in model_monitor_whitelisted_ips:
                    whitelisted_ips.append(model_monitor_whitelisted_ip[0])
            return {"status_code": 200, "model_monitor_details": model_monitor_details,
                    "model_monitor_notification_details": model_monitor_notification_details,
                    "whitelisted_ips": whitelisted_ips}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "data": {"error": "Error occurred in getting model monitor details"}}
    finally:
        if session is not None:
            close_database_session(session)


def add_model_monitor_notification_details(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)

        mandatory_fields = ["user_id", "project_id", "by_email"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}
        by_slack = False
        if event_body.get("by_slack") is not None:
            by_slack = event_body.get("by_slack")
            mandatory_fields = ["slack_api_token", "slack_channel_name"]
            for mandatory_field in mandatory_fields:
                if event_body.get(mandatory_field) is None:
                    return {"status_code": 50001,
                            "error": "Please provide {}, it should not be empty".format(mandatory_field),
                            "error_field_name": mandatory_field}

        session = get_database_session()
        model_monitor_notification_details = ModelMonitorNotificationDetails()
        model_monitor_notification_details.user_id = event_body.get("user_id")
        model_monitor_notification_details.project_id = event_body.get("project_id")
        model_monitor_notification_details.by_email = event_body.get("by_email")
        model_monitor_notification_details.by_slack = by_slack
        model_monitor_notification_details.slack_api_token = event_body.get("slack_api_token")
        model_monitor_notification_details.slack_channel_name = event_body.get("slack_channel_name")
        session.add(model_monitor_notification_details)
        commit_database_session(session)
        return {"status_code": 201, "data": json.dumps(model_monitor_notification_details, cls=AlchemyEncoder)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "data": {"error": "Error occurred in adding model monitor notification details"}}
    finally:
        if session is not None:
            close_database_session(session)


def update_model_monitor_notification_details(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        data_to_update = {}
        mandatory_fields = ["user_id", "project_id"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        if event_body.get("by_slack") is not None:
            mandatory_fields = ["by_slack", "slack_api_token", "slack_channel_name"]
            for mandatory_field in mandatory_fields:
                if event_body.get(mandatory_field) is None:
                    return {"status_code": 50001,
                            "error": "Please provide {}, it should not be empty".format(mandatory_field),
                            "error_field_name": mandatory_field}
                data_to_update[mandatory_field] = event_body.get(mandatory_field)

        if event_body.get("by_email") is not None:
            mandatory_fields = ["by_email", "emails"]
            for mandatory_field in mandatory_fields:
                if event_body.get(mandatory_field) is None:
                    return {"status_code": 50001,
                            "error": "Please provide {}, it should not be empty".format(mandatory_field),
                            "error_field_name": mandatory_field}
                data_to_update[mandatory_field] = event_body.get(mandatory_field)

        session = get_database_session()
        session.query(ModelMonitorNotificationDetails).filter(
            ModelMonitorNotificationDetails.project_id == event_body.get("project_id")).update(data_to_update)

        commit_database_session(session)
        return {"status_code": 200, "data": "Model monitor notification details updated successfully"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500,
                "data": {"error": "Error occurred in updating model monitor notification details updated successfully"}}
    finally:
        if session is not None:
            close_database_session(session)


def add_model_monitor_webhook_api_key(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)

        mandatory_fields = ["user_id", "project_id"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        session = get_database_session()
        model_monitor_webhook_api_key = ModelMonitorWebhookApiKey()
        model_monitor_webhook_api_key.user_id = event_body.get("user_id")
        model_monitor_webhook_api_key.project_id = event_body.get("project_id")
        api_key = secrets.token_hex(64)
        model_monitor_webhook_api_key.webhook_api_key = secrets.token_hex(64)
        session.add(model_monitor_webhook_api_key)
        commit_database_session(session)
        send_api_key_email(event_body.get("user_id"), event_body.get("project_id"), api_key)
        return {"status_code": 201, "data": json.dumps(model_monitor_webhook_api_key, cls=AlchemyEncoder)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "data": {"error": "Error occurred in adding model monitor webhook API key"}}
    finally:
        if session is not None:
            close_database_session(session)


def add_model_monitor_whitelist_ip(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)

        mandatory_fields = ["ip", "project_id", "user_id"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        session = get_database_session()
        model_monitor_webhook_whitelist_ip = ModelMonitorWebhookWhitelistIP()
        model_monitor_webhook_whitelist_ip.user_id = event_body.get("user_id")
        model_monitor_webhook_whitelist_ip.project_id = event_body.get("project_id")
        model_monitor_webhook_whitelist_ip.ip_address = event_body.get("ip_address")
        session.add(model_monitor_webhook_whitelist_ip)
        commit_database_session(session)
        return {"status_code": 201, "data": json.dumps(model_monitor_webhook_whitelist_ip, cls=AlchemyEncoder)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "data": {"error": "Error occurred in adding model monitor whitelisted IP"}}
    finally:
        if session is not None:
            close_database_session(session)


def delete_model_monitor_whitelist_ip(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)

        mandatory_fields = ["ip", "project_id", "user_id"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        session = get_database_session()
        session.query(ModelMonitorWebhookWhitelistIP).filter(
            ModelMonitorWebhookWhitelistIP.ip_address == event_body.get("ip_address"),
            ModelMonitorWebhookWhitelistIP.project_id == event_body.get("project_id")).update({"is_deleted": True})
        commit_database_session(session)
        return {"status_code": 200, "data": "Whitelisted IP removed successfully"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500,
                "data": {"error": "Error occurred in deleting model monitor whitelisted IP address"}}
    finally:
        if session is not None:
            close_database_session(session)


def delete_email_from_notification_details(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        data_to_update = {}
        mandatory_fields = ["email", "project_id"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        existing_emails = session.query(ModelMonitorNotificationDetails.emails).filter(
            ModelMonitorNotificationDetails.project_id == event_body.get("project_id")).first()
        if existing_emails is not None:
            emails = existing_emails[0].split(",")
            if event_body.get("email") in emails:
                emails.remove(event_body.get("email"))
            data_to_update["emails"] = ",".join(emails)
        session = get_database_session()
        session.query(ModelMonitorNotificationDetails).filter(
            ModelMonitorNotificationDetails.project_id == event_body.get("project_id")).update(data_to_update)
        commit_database_session(session)
        return {"status_code": 200, "data": "Email deleted successfully from notification settings"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500,
                "data": {"error": "Error occurred in deleting email from notification settings"}}
    finally:
        if session is not None:
            close_database_session(session)


def add_email_in_notification_details(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        data_to_update = {}
        mandatory_fields = ["email", "project_id"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        existing_emails = session.query(ModelMonitorNotificationDetails.emails).filter(
            ModelMonitorNotificationDetails.project_id == event_body.get("project_id")).first()
        emails = []
        if existing_emails is not None:
            emails = existing_emails[0].split(",")
            if event_body.get("email") not in emails:
                emails.append(event_body.get("email"))
        if len(emails) > 0:
            data_to_update["emails"] = ",".join(emails)
            data_to_update["by_email"] = 1
        session = get_database_session()
        session.query(ModelMonitorNotificationDetails).filter(
            ModelMonitorNotificationDetails.project_id == event_body.get("project_id")).update(data_to_update)
        commit_database_session(session)
        return {"status_code": 200, "data": "Email added successfully from notification settings"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500,
                "data": {"error": "Error occurred in adding email from notification settings"}}
    finally:
        if session is not None:
            close_database_session(session)


def update_slack_notification_details(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        data_to_update = {}
        mandatory_fields = ["slack_api_token", "slack_channel_name"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}
            data_to_update[mandatory_field] = event_body.get(mandatory_field)
        data_to_update["by_slack"] = 1
        session = get_database_session()
        session.query(ModelMonitorNotificationDetails).filter(
            ModelMonitorNotificationDetails.project_id == event_body.get("project_id")).update(data_to_update)
        commit_database_session(session)
        return {"status_code": 200, "data": "Slack details are added successfully in notification settings"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500,
                "data": {"error": "Error occurred in adding slack details in notification settings"}}
    finally:
        if session is not None:
            close_database_session(session)


def update_dataset_details(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        data_to_update = {}
        mandatory_fields = ["use_webhook", "use_dataset"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}
            data_to_update[mandatory_field] = event_body.get(mandatory_field)

        session = get_database_session()
        session.query(ModelMonitorDetails).filter(
            ModelMonitorDetails.project_id == event_body.get("project_id")).update(data_to_update)
        commit_database_session(session)
        return {"status_code": 200, "data": "Model monitor dataset details updated successfully"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500,
                "data": {"error": "Error occurred in updating model monitor dataset details "}}
    finally:
        if session is not None:
            close_database_session(session)


def update_model_monitor_details(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        data_to_update = {}
        notification_data_to_update = {}
        mandatory_fields = ["use_webhook", "use_dataset", "project_id"]
        # Update dataset details
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}
            data_to_update[mandatory_field] = event_body.get(mandatory_field)

        # Update slack details
        if event.get("by_slack") is not None:
            mandatory_fields = ["slack_api_token", "slack_channel_name"]
            for mandatory_field in mandatory_fields:
                if event_body.get(mandatory_field) is None:
                    return {"status_code": 50001,
                            "error": "Please provide {}, it should not be empty".format(mandatory_field),
                            "error_field_name": mandatory_field}
                notification_data_to_update[mandatory_field] = event_body.get(mandatory_field)
                notification_data_to_update["by_slack"] = 1

        session = get_database_session()
        # Add new emails or remove emails
        if event_body.get("emails_to_add") is not None or event_body.get("emails_to_remove") is not None:
            existing_emails = session.query(ModelMonitorNotificationDetails.emails).filter(
                ModelMonitorNotificationDetails.project_id == event_body.get("project_id")).first()
            emails = []
            if existing_emails is not None:
                emails = existing_emails[0].split(",")
                if event_body.get("emails_to_remove") is not None:
                    for email in event_body.get("emails_to_remove"):
                        if email in emails:
                            emails.remove(email)
                if event_body.get("emails_to_add") is not None:
                    for email in event_body.get("emails_to_add"):
                        if email not in emails:
                            emails.append(email)
            else:
                for email in event_body.get("emails_to_add"):
                    emails.append(email)
            if len(emails) > 0:
                notification_data_to_update["emails"] = ",".join(emails)
                notification_data_to_update["by_email"] = 1

        # Add IP address
        if event_body.get("ip_to_add") is not None:
            for ip in event_body.get("ip_to_add"):
                model_monitor_webhook_whitelist_ip = ModelMonitorWebhookWhitelistIP()
                model_monitor_webhook_whitelist_ip.user_id = event_body.get("user_id")
                model_monitor_webhook_whitelist_ip.project_id = event_body.get("project_id")
                model_monitor_webhook_whitelist_ip.ip_address = ip
                session.add(model_monitor_webhook_whitelist_ip)

        # Remove IP address
        if event_body.get("ip_to_remove") is not None:
            for ip in event_body.get("ip_to_remove"):
                session.query(ModelMonitorWebhookWhitelistIP).filter(
                    ModelMonitorWebhookWhitelistIP.ip_address == ip,
                    ModelMonitorWebhookWhitelistIP.project_id == event_body.get("project_id")).update(
                    {"is_deleted": True})

        # Update notification data
        if len(notification_data_to_update) > 0:
            session.query(ModelMonitorNotificationDetails).filter(
                ModelMonitorNotificationDetails.project_id == event_body.get("project_id")).update(
                notification_data_to_update)

        # Update model monitor data
        if len(data_to_update) > 0:
            session.query(ModelMonitorDetails).filter(
                ModelMonitorDetails.project_id == event_body.get("project_id")).update(data_to_update)
        commit_database_session(session)
        return {"status_code": 200, "data": "Model monitor settings updated successfully"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500,
                "data": {"error": "Error occurred in updating model monitor settings"}}
    finally:
        if session is not None:
            close_database_session(session)


def send_api_key_email(user_id, project_id, api_key):
    try:
        session = get_database_session()
        user = session.query(User).filter(User.auth_id == user_id).first()
        user_name = user.first_name if user.first_name else "" + " " + user.last_name if user.last_name else ""
        project = session.query(ProjectDetail).filter(ProjectDetail.id == project_id).first()

        # If necessary, replace us-west-2 with the AWS Region you're using for Amazon SES.
        aws_region = "us-east-2"
        subject = "KOSA - Webhook API Key"
        env = Environment(
            loader=FileSystemLoader(os.path.join(os.path.dirname(__file__), "templates"), encoding="utf8"))
        filename = "webhook_api_key.j2"
        body_html = env.get_template(filename).render(
            USER_NAME=user_name,
            PROJECT_NAME=project.project_name,
            PROJECT_URL="{}.kosa.ai/#/featuredrift".format(os.getenv("STAGE")),
            API_KEY=api_key)
        # The character encoding for the email.
        charset = "UTF-8"

        # Create a new SES resource and specify a region.
        client = boto3.client('ses', region_name=aws_region)

        # Try to send the email.
        # Provide the contents of the email.
        response = client.send_email(
            Destination={
                'ToAddresses': [
                    user.email,
                ],
            },
            Message={
                'Body': {
                    'Html': {
                        'Charset': charset,
                        'Data': body_html,
                    }
                },
                'Subject': {
                    'Charset': charset,
                    'Data': subject,
                },
            },
            Source="team@kosa.ai"
        )
    # Display an error if something goes wrong.
    except Exception as e:
        logging.error(e)
        logging.error(e.response['Error']['Message'])
    finally:
        if session is not None:
            close_database_session(session)
