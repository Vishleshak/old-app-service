service: ONBOARDING-API

provider:
  name: aws
  runtime: python3.8
  stage: ${opt:stage, 'prod'}
  region: ${self:custom.region}
  environment: 
    AUTH0_CLIENT_ID: ${file(../../config/config.${self:provider.stage}.json):AUTH0_CLIENT_ID}
    AUTH0_CLIENT_SECRET: ${file(../../config/config.${self:provider.stage}.json):AUTH0_CLIENT_SECRET}
    AUTH0_AUDIENCE: ${file(../../config/config.${self:provider.stage}.json):AUTH0_AUDIENCE}
    AUTH0_DOMAIN: ${file(../../config/config.${self:provider.stage}.json):AUTH0_DOMAIN}
    AUTH0_CONNECTION: ${file(../../config/config.${self:provider.stage}.json):AUTH0_CONNECTION}
    AUTH0_CLIENT_PUBLIC_KEY: ${file(../../config/public_key.${self:provider.stage})}
    DATABASE_USERNAME: ${file(../../config/config.${self:provider.stage}.json):DATABASE_USERNAME}
    DATABASE_PASSWORD: ${file(../../config/config.${self:provider.stage}.json):DATABASE_PASSWORD}
    DATABASE_NAME: ${file(../../config/config.${self:provider.stage}.json):DATABASE_NAME}
    DATABASE_PORT: ${file(../../config/config.${self:provider.stage}.json):DATABASE_PORT}
    DATABASE_HOST: ${file(../../config/config.${self:provider.stage}.json):DATABASE_HOST}
  iamRoleStatements:
    - ${self:custom.iamRoleStatements}
  apiGateway:
    restApiId:
      'Fn::ImportValue': kosa-${self:provider.stage}-ApiGateway-restApiId
    restApiRootResourceId:
      'Fn::ImportValue': kosa-${self:provider.stage}-ApiGateway-rootResourceId

custom: ${file(../../config/config.yaml)}

package:
  include:
  - ../../utils/**
  - ../../models/**  

functions:
  signup:
    handler: handler.signup
    name: ${self:provider.stage}-${self:service}-signup
    description: User Registration
    timeout: 30
    events:
      - http:
          path: signup
          method: post
          cors: ${self:custom.corsStatements}
          integration: lambda
          response:
            headers:
              Content-Type: "'application/json'"

  login:
    handler: handler.login
    name: ${self:provider.stage}-${self:service}-login
    description: User login
    timeout: 30
    events:
      - http:
          path: login
          method: post
          cors: ${self:custom.corsStatements}
          integration: lambda
          response:
            headers:
              Content-Type: "'application/json'"

  add_new_user:
    handler: handler.add_new_user
    name: ${self:provider.stage}-${self:service}-add-new-user
    description: Create organization user
    timeout: 30
    events:
      - http:
          path: add_new_user
          method: post
          cors: ${self:custom.corsStatements}
          integration: lambda
          response:
            headers:
              Content-Type: "'application/json'"

  forgot_password:
    handler: handler.forgot_password
    name: ${self:provider.stage}-${self:service}-forgot-password
    description: Forgot password
    timeout: 30
    events:
      - http:
          path: forgot_password
          method: post
          cors: ${self:custom.corsStatements}
          integration: lambda
          response:
            headers:
              Content-Type: "'application/json'"

  reset_password:
    handler: handler.reset_password
    name: ${self:provider.stage}-${self:service}-reset-password
    description: Reset password
    timeout: 30
    events:
      - http:
          path: reset_password
          method: post
          cors: ${self:custom.corsStatements}
          integration: lambda
          response:
            headers:
              Content-Type: "'application/json'"
          authorizer:
            type: CUSTOM
            authorizerId: ${cf:AUTH-SERVICE-${self:provider.stage}.apiGatewayAuthorizer}

  check_email_exists:
    handler: handler.check_email_exists
    name: ${self:provider.stage}-${self:service}-check-email-exists
    description: Check email exists or not
    timeout: 30
    events:
      - http:
          path: validate-email
          method: post
          cors: ${self:custom.corsStatements}
          integration: lambda
          response:
            headers:
              Content-Type: "'application/json'"

plugins:
  - serverless-python-requirements
  - serverless-pseudo-parameters
  - serverless-prune-plugin