import json
import os

import requests

from models.json_converter import AlchemyEncoder
from models.organization import Organization
from models.organization_invitation import OrganizationInvitation
from models.organization_users import OrganizationUser
from models.user import User
from utils.db_util import get_database_session, commit_database_session, close_database_session
from utils.event_util import get_body
from utils.jwt_util import jwt_decode
from utils.log_util import get_logger

logging = get_logger(__name__)


def signup(event, context):
    """This is method for signup

    Args:
        event ([dict]): pass the event
        context ([object]): pass the context

    Returns:
        [dict]: returns information about user created or not
    """
    try:
        logging.debug("Event : {}".format(event))
        auth_token_request = requests.post(os.getenv('AUTH0_DOMAIN') + "/oauth/token",
                                           json={
                                               'grant_type': 'client_credentials',
                                               'client_id': os.getenv("AUTH0_CLIENT_ID"),
                                               'client_secret': os.getenv("AUTH0_CLIENT_SECRET"),
                                               'audience': os.getenv("AUTH0_AUDIENCE"),
                                               'scope': 'create:users'},
                                           headers={'content-type': "application/json"})

        auth_token_body = auth_token_request.json()

        if all(key in auth_token_body for key in ['access_token', 'scope', 'expires_in', 'token_type']):
            logging.info("Authenticated with Auth0")
        elif 'error_description' in auth_token_body:
            logging.error(auth_token_body['error_description'])
            return {
                'statusCode': 400,
                'body': json.dumps({'error': auth_token_body['error_description']})
            }
        else:
            logging.error("No error message returned from Auth0")
            return {
                'statusCode': 500,
                'body': json.dumps({'error': 'Unknown error occurred on Auth0'})}

        access_token = auth_token_body['access_token']

        # TODO: Populate this body object from event object
        event_body = get_body(event)
        name = event_body.get("name")
        email = event_body.get("email")

        # TODO: Need to think on password field, whether to generate a random password
        # and ask user to update password on first login or verification
        password = event_body.get("password")

        # TODO: Email verified will be false by default and
        # let user verify email and then we can update the verified flag
        payload = {
            'name': name,
            'email': email,
            'password': password,
            'email_verified': False,
            'blocked': False,
            'connection': os.environ["AUTH0_CONNECTION"],
        }

        resp = requests.post(
            os.environ["AUTH0_AUDIENCE"] + "users",
            json=payload,
            headers={
                'Authorization': 'Bearer {TOKEN}'.format(TOKEN=access_token),
                'content-type': "application/json"})

        body = resp.json()

        if all(k in body for k in ['user_id', 'email']):
            user_id = body['user_id'][6:]
            email = body['email']
            return {"user_created": True, "payload": body}
        else:
            return body
    except Exception as ex:
        logging.error(ex)
        return {"user_created": False}


def login(event, context):
    """This is method for Login 

    Args:
        event ([dict]): Pass the event
        context ([object]): Pass the context

    Returns:
        [dict]: Returns information about Login successful or not
    """
    try:
        logging.debug("Event : {}".format(event))
        auth_token_request = requests.post(os.getenv('AUTH0_DOMAIN') + "/oauth/token",
                                           json={
                                               'grant_type': 'client_credentials',
                                               'client_id': os.getenv("AUTH0_CLIENT_ID"),
                                               'client_secret': os.getenv("AUTH0_CLIENT_SECRET"),
                                               'audience': os.getenv("AUTH0_AUDIENCE"),
                                               'scope': 'create:users'},
                                           headers={'content-type': "application/json"})

        auth_token_body = auth_token_request.json()

        if all(key in auth_token_body for key in [
            'access_token',
            'scope',
            'expires_in',
            'token_type']):
            logging.info("Authenticated with Auth0")
        elif 'error_description' in auth_token_body:
            logging.error(auth_token_body['error_description'])
            return {
                'statusCode': 400,
                'body': json.dumps({'error': auth_token_body['error_description']})
            }
        else:
            logging.error("No error message returned from Auth0")
            return {
                'statusCode': 500,
                'body': json.dumps({'error': 'Unknown error occurred on Auth0'})}

        access_token = auth_token_body['access_token']

        event_body = get_body(event)
        email = event_body.get("email")
        password = event_body.get("password")

        # Authenticate and get tokens
        req = requests.post(os.environ['AUTH0_DOMAIN'] + '/oauth/token',
                            json={'grant_type': 'password',
                                  'username': email,
                                  'password': password,
                                  'audience': os.environ['AUTH0_AUDIENCE'],
                                  'client_id': os.environ['AUTH0_CLIENT_ID'],
                                  'client_secret': os.environ['AUTH0_CLIENT_SECRET'],
                                  'scope': 'openid'})

        body = req.json()

        if 'id_token' in body:
            _id_token = body['id_token']
            payload = jwt_decode(_id_token, os.environ['AUTH0_CLIENT_PUBLIC_KEY'])

            if payload['email_verified']:
                email = payload['email']
                # The payload return by Auth0 has user id with the provider name, in our case provider is Auth0
                # So the value sub in the payload starts with a string auth0|<USER ID>
                # In order to extract user id from it, we are using [6:] which is a substring operation
                user_id = payload['sub'][6:]
                return {
                    'statusCode': 200,
                    'body': {
                        "id_token": _id_token,
                        "payload": payload,
                        "email": email,
                        "user_id": user_id
                    }
                }
            else:
                headers = {'Authorization': "Bearer {}".format(access_token)}
                user_details_request = requests.get('{}users?q=email:"{}"&search_engine=v3'.format(os.getenv("AUTH0_AUDIENCE"), email), headers=headers)
                logging.debug(user_details_request)
                logging.debug(user_details_request.status_code)
                if user_details_request is not None and user_details_request.status_code == 200:
                    user_details_data = json.loads(user_details_request.content.decode())
                    # user_details_data is an array, where the 0th element contains user information,
                    # The user_id field is part of identities details in the response which is a dictionary
                    identity_user_id = user_details_data[0].get("identities")[0].get('user_id')
                    # user_details_data is an array, where the 0th element contains user information,
                    # The provider field is part of identities details in the response which is a dictionary
                    identity_provider = user_details_data[0].get("identities")[0].get('provider')
                    headers = {'content-type': "application/json", 'Authorization': "Bearer {}".format(access_token)}
                    verification_email_response = requests.post("{}jobs/verification-email".format(os.environ['AUTH0_AUDIENCE']),
                                                                json={"user_id": payload['sub'],
                                                                      "client_id": os.environ['AUTH0_CLIENT_ID'],
                                                                      "identity": {"user_id": identity_user_id,
                                                                                   "provider": identity_provider}},
                                                                headers=headers)
                    # If verification_email_response.status_code is 201 means verification email is sent else
                    # Auth0 management API is not able to send verification email
                    if verification_email_response.status_code == 201:
                        return {
                            'statusCode': 400,
                            'body': json.dumps({
                                'error': 'Your account has not been verified. Please check your email for the '
                                         'verification link and accept the invitation.'
                            })
                        }
                    else:
                        logging.debug("Unable to send verification link")
                        return {
                            'statusCode': 400,
                            'body': json.dumps({
                                'error': 'Your account has not been verified. And we are unable to send '
                                         'verification link at the moment. Please contact us on team@kosa.ai'
                            })
                        }
                else:
                    # This code will get called if user details Auth0 API is not returning response
                    return {
                        'statusCode': 400,
                        'body': json.dumps({
                            'error': 'Not able to fetch user details, please try after some time, '
                                     'if this error persists, please contact us on team@kosa.ai'
                        })
                    }
        else:
            logging.error("oAuth token is not returned by Auth0")
            if 'error_description' in body:
                logging.error(body['error_description'])
                return {
                    'statusCode': 400,
                    'body': json.dumps({
                        'error': body['error_description']
                    })
                }
            else:
                logging.error("No error message returned from Auth0")
                return {
                    'statusCode': 500,
                    'body': json.dumps({
                        'error': 'Unknown Error happened'
                    })
                }
    except Exception as ex:
        logging.error(ex)
        return {
            'statusCode': 500,
            'body': json.dumps({
                'error': 'Exception occurred while login : {}'.format(str(ex))
            })
        }


def add_new_user(event, context):
    """This is method adding new user

    Args:
        event ([dict]): pass the event
        context ([object]): pass the context

    Returns:
        [dict]: returns information about user created or not
    """
    session = None
    try:
        logging.debug("Event : {}".format(event))
        event_body = get_body(event)
        first_name = event_body.get("first_name")
        last_name = event_body.get("last_name")
        email = event_body.get("email")
        password = event_body.get("password")
        confirm_password = event_body.get("confirm_password")
        organization_name = event_body.get("organization_name")
        organization_id = event_body.get("organization_id")
        user_role_id = event_body.get("user_role_id")
        is_organization_admin = event_body.get("is_organization_admin")
        invite_id = event_body.get("invite_id")
        terms_and_conditions = event_body.get("terms_and_conditions")

        mandatory_fields = ["first_name", "last_name", "email", "password", "confirm_password", "organization_name",
                            "terms_and_conditions"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        if password != confirm_password:
            return {"status_code": 50001, "error": "Password and confirm password must match",
                    "error_field_name": "password"}

        # Validate organization
        session = get_database_session()
        if invite_id is None:
            organization_details = session.query(Organization).filter(Organization.name == organization_name).first()
            if organization_details is not None and organization_id is None:
                return {"status_code": 50003,
                        "error": "The organization with name {} is already exists".format(organization_name)}

        auth_token_request = requests.post(os.getenv('AUTH0_DOMAIN') + "/oauth/token",
                                           json={
                                               'grant_type': 'client_credentials',
                                               'client_id': os.getenv("AUTH0_CLIENT_ID"),
                                               'client_secret': os.getenv("AUTH0_CLIENT_SECRET"),
                                               'audience': os.getenv("AUTH0_AUDIENCE"),
                                               'scope': 'create:users'},
                                           headers={'content-type': "application/json"})

        auth_token_body = auth_token_request.json()

        if all(key in auth_token_body for key in [
            'access_token',
            'scope',
            'expires_in',
            'token_type']):
            logging.info("Authenticated with Auth0")
        elif 'error_description' in auth_token_body:
            logging.error(auth_token_body['error_description'])
            return {
                'statusCode': 400,
                'body': json.dumps({'error': auth_token_body['error_description']})
            }
        else:
            logging.error("No error message returned from Auth0")
            return {
                'statusCode': 500,
                'body': json.dumps({'error': 'Unknown error occurred on Auth0'})}

        access_token = auth_token_body['access_token']

        # Email verified will be false by default and
        # let user verify email and then we can update the verified flag
        payload = {
            'name': "{} {}".format(first_name, last_name),
            'email': email,
            'password': password,
            'email_verified': False,
            'blocked': False,
            'connection': os.environ["AUTH0_CONNECTION"],
        }

        resp = requests.post(
            os.environ["AUTH0_AUDIENCE"] + "users",
            json=payload,
            headers={
                'Authorization': 'Bearer {TOKEN}'.format(TOKEN=access_token),
                'content-type': "application/json"})

        body = resp.json()
        if all(k in body for k in ['user_id', 'email']):
            user_id = body['user_id'][6:]
            email = body['email']
            # Create new organization if not exists
            if organization_id is None:
                organization_details = Organization()
                organization_details.name = organization_name
                organization_details.address = ""
                session.add(organization_details)
                commit_database_session(session)
            else:
                organization_details = session.query(Organization).filter(Organization.id == organization_id).first()

            # Add user in the organization
            organization_user = OrganizationUser()
            organization_user.user_id = user_id
            organization_user.organization_id = organization_details.id
            organization_user.is_admin = is_organization_admin
            organization_user.user_role_id = user_role_id
            if invite_id is not None:
                organization_invitation = session.query(OrganizationInvitation).filter(
                    OrganizationInvitation.invitation_hash == invite_id,
                    OrganizationInvitation.is_deleted == 0).first()
                if organization_invitation is not None:
                    organization_user.accessible_modules = organization_invitation.accessible_modules
                    organization_user.user_rights = organization_invitation.user_rights
            session.add(organization_user)

            # Add user in the kosa database
            user = User()
            user.first_name = first_name
            user.last_name = last_name
            user.email = email
            user.auth_id = user_id
            user.terms_and_conditions = terms_and_conditions
            session.add(user)
            commit_database_session(session)
            return {"user_created": True, "payload": body, "user": json.dumps(user, cls=AlchemyEncoder),
                    "organization_details": json.dumps(organization_details, cls=AlchemyEncoder),
                    "organization_user": json.dumps(organization_user, cls=AlchemyEncoder)}
        else:
            return body
    except Exception as ex:
        logging.error(ex)
        return {"user_created": False}
    finally:
        if session is not None:
            close_database_session(session)


def forgot_password(event, context):
    """This is method for forgot or reset password 

    Args:
        event ([dict]): Pass the event
        context ([object]): Pass the context

    Returns:
        [dict]: Returns information whether reset password link is sent or not 
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        session = get_database_session()
        result = session.query(User).filter(User.email == event_body.get("email")).first()
        if not result:
            return {"reset_link_sent": False, "message": "User does not exists"}
        payload = '{"client_id": "' + os.environ["AUTH0_CLIENT_ID"] + '","email": "' + event_body.get(
            "email") + '","connection": "' + os.environ["AUTH0_CONNECTION"] + '"}'
        resp = requests.post(os.environ["AUTH0_DOMAIN"] + "/dbconnections/change_password", data=str(payload),
                             headers={'content-type': "application/json"})
        return {"reset_link_sent": True, "message": resp.content}
    except Exception as ex:
        logging.error(ex)
        return {"reset_link_sent": False,
                "message": "It seems like some exception occurred, please contact team@kosa.ai"}
    finally:
        if session is not None:
            close_database_session(session)


def reset_password(event, context):
    """This is method for forgot or reset password 

    Args:
        event ([dict]): Pass the event
        context ([object]): Pass the context

    Returns:
        [dict]: Returns information whether reset password link is sent or not 
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        user_id = event_body.get("auth_id")
        session = get_database_session()
        result = session.query(User).filter(User.auth_id == user_id).first()
        if not result:
            return {"status_code": 400, "message": "User does not exists"}

        auth_token_request = requests.post(os.getenv('AUTH0_DOMAIN') + "/oauth/token",
                                           json={
                                               'grant_type': 'client_credentials',
                                               'client_id': os.getenv("AUTH0_CLIENT_ID"),
                                               'client_secret': os.getenv("AUTH0_CLIENT_SECRET"),
                                               'audience': os.getenv("AUTH0_AUDIENCE"),
                                               'scope': 'create:users'},
                                           headers={'content-type': "application/json"})

        auth_token_body = auth_token_request.json()

        if all(key in auth_token_body for key in [
            'access_token',
            'scope',
            'expires_in',
            'token_type']):
            logging.info("Authenticated with Auth0")
        elif 'error_description' in auth_token_body:
            logging.error(auth_token_body['error_description'])
            return {
                'statusCode': 400,
                'body': json.dumps({'error': auth_token_body['error_description']})
            }
        else:
            logging.error("No error message returned from Auth0")
            return {
                'statusCode': 500,
                'body': json.dumps({'error': 'Unknown error occurred on Auth0'})}

        access_token = auth_token_body['access_token']

        payload = '{"password":"' + event_body.get("new_password") + '", "connection": "' + os.environ[
            "AUTH0_CONNECTION"] + '"}'
        resp = requests.patch(os.environ["AUTH0_DOMAIN"] + "/api/v2/users/auth0|{}".format(user_id), data=payload,
                              headers={'Authorization': 'Bearer {TOKEN}'.format(TOKEN=access_token),
                                       'content-type': "application/json"})
        if resp.status_code == 200:
            return {"reset_password": True, "status_code": resp.status_code}
        else:
            return {"reset_password": False, "message": resp.content, "status_code": resp.status_code}
    except Exception as ex:
        logging.error(ex)
        return {"reset_link_sent": False}
    finally:
        if session is not None:
            close_database_session(session)


def check_email_exists(event, context):
    """This is method to check if email is already used

    Args:
        event ([dict]): Pass the event
        context ([object]): Pass the context

    Returns:
        [dict]: Returns information about email is already used or not
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        email = event_body.get("email")
        if email is None:
            return {"status_code": 50001,
                    "error": "Please provide email, it should not be empty",
                    "error_field_name": "email"}

        session = get_database_session()
        result = session.query(User).filter(User.email == email).first()
        if not result:
            return {"status_code": 200, "email_exists": False}
        return {"status_code": 200, "email_exists": True}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500}
    finally:
        if session is not None:
            close_database_session(session)
