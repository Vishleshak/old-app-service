import hashlib
import json

import boto3

from models.dataset_target_variable import DatasetTargetVariableDetails
from models.project_dataset_details import ProjectDatasetDetails
from utils.db_util import get_database_session, commit_database_session, close_database_session
from utils.event_util import get_body
from utils.log_util import get_logger

logging = get_logger(__name__)


def add_target_variable(event, context):
    """This is method for adding User

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["dataset_path", "project_id", "target_variables", "dataset_type"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}
        target_variables = event_body.get("target_variables")
        for target_variable in target_variables:
            dataset_target_variable_details = DatasetTargetVariableDetails()
            dataset_target_variable_details.dataset_path = event_body.get("dataset_path")
            dataset_target_variable_details.project_id = event_body.get("project_id")
            dataset_target_variable_details.target_variable = target_variable
            dataset_target_variable_details.dataset_type = event_body.get("dataset_type")
            session = get_database_session()
            session.add(dataset_target_variable_details)
            commit_database_session(session)

        if target_variables is not None and len(target_variables) >= 1:
            project_dataset_details = session.query(ProjectDatasetDetails).filter(
                ProjectDatasetDetails.project_id == event_body.get("project_id"),
                ProjectDatasetDetails.dataset_path == event_body.get("dataset_path")).first()
            if project_dataset_details is not None:
                data_to_update = {"target_variable_detected": True}
                session.query(ProjectDatasetDetails).filter(
                    ProjectDatasetDetails.id == project_dataset_details.id).update(data_to_update)
                commit_database_session(session)
        return {"status_code": 201}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred storing target variable"}
    finally:
        if session is not None:
            close_database_session(session)


def get_target_variable(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        session = get_database_session()
        mandatory_fields = ["project_id", "dataset_path"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        target_variables = []
        dataset_target_variable_details = session.query(DatasetTargetVariableDetails).filter(
            DatasetTargetVariableDetails.project_id == event_body.get(
                "project_id"), DatasetTargetVariableDetails.dataset_path == event_body.get(
                "dataset_path"), DatasetTargetVariableDetails.dataset_type == event_body.get(
                "dataset_type")).all()

        if dataset_target_variable_details is not None:
            for dataset_target_variable_detail in dataset_target_variable_details:
                obj = {"id": dataset_target_variable_detail.id,
                       "dataset_path": dataset_target_variable_detail.dataset_path,
                       "target_variable": dataset_target_variable_detail.target_variable,
                       "target_variable_values": read_column_values(event_body.get("dataset_path"),
                                                                    event_body.get("project_id"),
                                                                    dataset_target_variable_detail.target_variable)}
                target_variables.append(obj)
            return {"status_code": 200, "target_variables": target_variables}
        return {"status_code": 404, "target_variable": None,
                "message": "Dataset target variables for project:  {} does not exist".format(
                    event_body.get("project_id"))}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting dataset target variable"}
    finally:
        if session is not None:
            close_database_session(session)


def get_target_variables_data(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        session = get_database_session()
        mandatory_fields = ["project_id", "dataset_path"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        if event_body.get("project_id") is not None and event_body.get("dataset_path") is not None:
            dataset_target_variable_details = session.query(DatasetTargetVariableDetails).filter(
                DatasetTargetVariableDetails.project_id == event_body.get(
                    "project_id"), DatasetTargetVariableDetails.dataset_path == event_body.get(
                    "dataset_path")).all()
            small_dataset = read_small_dataset(event_body.get("dataset_path"), event_body.get("project_id"))
            dataset_target_variables = []
            if dataset_target_variable_details is not None:
                for dataset_target_variable_detail in dataset_target_variable_details:
                    dataset_target_variables.append(dataset_target_variable_detail.target_variable)

            indexes = []
            if len(dataset_target_variables) > 0:
                for target_variable in dataset_target_variables:
                    indexes.append(small_dataset['columns'].index(target_variable))

            for index in indexes:
                del small_dataset['columns'][index]
                del small_dataset['rows'][index]
            return {"status_code": 200, "data": small_dataset}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting target variable data"}
    finally:
        if session is not None:
            close_database_session(session)


def read_small_dataset(file_name, project_id):
    try:
        s3 = boto3.resource('s3')
        hash_data = hashlib.new('sha256')
        hash_data.update(file_name.encode())
        hashed_file_name = hash_data.hexdigest()
        object_key = '{}/{}/dataset_small.json'.format(project_id, hashed_file_name)
        obj = s3.Object("kosa-metadata", object_key)
        return json.load(obj.get()['Body'])
    except Exception as ex:
        logging.error(ex)
    return None


def get_target_variable_values(event, context):
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["project_id", "dataset_path", "target_variable"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        column_values = read_column_values(event_body.get("dataset_path"), event_body.get("project_id"),
                                           event_body.get("target_variable"))
        return {"status_code": 200, "data": column_values}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting target variable values"}


def delete_target_variable(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)

        mandatory_fields = ["project_id", "dataset_path", "target_variable"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}
        session = get_database_session()
        dataset_target_variable_details = session.query(DatasetTargetVariableDetails).filter(
            DatasetTargetVariableDetails.project_id == event_body.get(
                "project_id"), DatasetTargetVariableDetails.dataset_path == event_body.get(
                "dataset_path"), DatasetTargetVariableDetails.target_variable == event_body.get(
                "target_variable")).all()
        if dataset_target_variable_details is not None:
            for dataset_target_variable_detail in dataset_target_variable_details:
                session.delete(dataset_target_variable_detail)
                session.commit()
        return {"status_code": 200, "deleted": True}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while deleting target variables"}
    finally:
        if session is not None:
            close_database_session(session)


def read_column_values(file_name, project_id, variable_name):
    try:
        s3 = boto3.resource('s3')
        hash_data = hashlib.new('sha256')
        hash_data.update(file_name.encode())
        hashed_file_name = hash_data.hexdigest()
        if " " in variable_name:
            variable_name = variable_name.replace(" ", "_")
        object_key = '{}/{}/column_values/{}.json'.format(project_id, hashed_file_name, variable_name)
        obj = s3.Object("kosa-metadata", object_key)
        return json.load(obj.get()['Body'])
    except Exception as ex:
        logging.error(ex)
    return None
