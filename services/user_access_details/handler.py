import json

from models.json_converter import AlchemyEncoder
from models.user_app_functionality_access_details import UserAppFunctionalityAccessDetails
from utils.db_util import get_database_session, commit_database_session, close_database_session
from utils.event_util import get_body, get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def add_user_app_functionality_access(event, context):
    """This is method for user module access

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        session = get_database_session()
        if event_body.get("result") is not None and event_body.get("result").get("add_access") is not None:
            app_functionality_access_list = event_body.get("result").get("add_access")
            for app_functionality_access in app_functionality_access_list:
                user_app_functionality_access_details = UserAppFunctionalityAccessDetails()
                user_app_functionality_access_details.app_functionality_id = app_functionality_access.get(
                    "app_functionality_id")
                user_app_functionality_access_details.user_id = app_functionality_access.get("user_id")
                session.add(user_app_functionality_access_details)
            commit_database_session(session)

        if event_body.get("result") is not None and event_body.get("result").get("remove_access") is not None:
            remove_app_functionality_access_list = event_body.get("result").get("remove_access")
            for app_functionality_access in remove_app_functionality_access_list:
                user_app_functionality_access_details = session.query(UserAppFunctionalityAccessDetails).filter(
                    UserAppFunctionalityAccessDetails.app_functionality_id == app_functionality_access.get(
                        "app_functionality_id"),
                    UserAppFunctionalityAccessDetails.user_id == app_functionality_access.get("user_id")).first()
                if user_app_functionality_access_details is not None:
                    session.delete(user_app_functionality_access_details)
            commit_database_session(session)
        return {"status_code": 201, "accesses": "Module accesses added/removed successfully"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred in adding/removing module level access"}
    finally:
        if session is not None:
            close_database_session(session)


def get_user_functionality_access(event, context):
    """This is method for user module access

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        user_id = path_parameter.get("user_id")
        if user_id is not None:
            session = get_database_session()
            user_module_access = session.query(UserAppFunctionalityAccessDetails).filter(
                UserAppFunctionalityAccessDetails.user_id == user_id).all()
            data = json.dumps(user_module_access, cls=AlchemyEncoder)
            return {"status_code": 200, "accesses": data}
        return {"status_code": 200, "accesses": []}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred in getting module level access"}
    finally:
        if session is not None:
            close_database_session(session)
