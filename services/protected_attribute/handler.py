from models.protected_attribute import ProtectedAttribute
from utils.db_util import get_database_session, commit_database_session, close_database_session
from utils.event_util import get_body, get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def add_protected_attribute(event, context):
    """This is method for adding protected attribute

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        protected_attributes = ProtectedAttribute()
        protected_attributes.name = event_body.get("name")
        protected_attributes.gender = event_body.get("gender")
        protected_attributes.race = event_body.get("race")
        protected_attributes.age = event_body.get("age")
        session = get_database_session()
        session.add(protected_attributes)
        commit_database_session(session)
        return {"status_code": 201}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred in creating protected attribute"}
    finally:
        if session is not None:
            close_database_session(session)


def update_protected_attribute(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        if event_body.get("id") is not None:
            protected_attribute = ProtectedAttribute()
            protected_attribute.id = event_body.get("id")
            protected_attribute.name = event_body.get("name")
            protected_attribute.visibilty_priority = event_body.get("visibilty_priority")
            session = get_database_session()
            session.query(ProtectedAttribute).filter(id == event_body.get("id")).update(
                {'visibilty_priority': event_body.get("visibilty_priority"), 'name': event_body.get("name")})
            commit_database_session(session)
            return {"status_code": 200, "protected_attribute": protected_attribute}
        return {"status_code": 500, "error": "Error occurred in updating Protected Attribute"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred in creating Protected Attribute"}
    finally:
        if session is not None:
            close_database_session(session)


def get_protected_attribute(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        id = path_parameter.get("id")
        page = path_parameter.get("page")
        if page is None:
            page = 0
        else:
            page = int(str(page))
        session = get_database_session()
        if id is not None:
            result = session.query(ProtectedAttribute).filter(ProtectedAttribute.id == id).first()
            if not result:
                return {"status_code": 404, "message": "Protected Attribute {} does not exist".format(id)}
            return result
        else:
            result = session.query(ProtectedAttribute).offset(page * 10).limit(10)
            if not result:
                return {"status_code": 404, "message": "Protected Attribute {} does not exist".format(id)}
            return result

    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting Protected Attribute"}
    finally:
        if session is not None:
            close_database_session(session)


def delete_protected_attribute(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        id = path_parameter.get("id")
        session = get_database_session()
        result = session.query(ProtectedAttribute).filter(ProtectedAttribute.id == id).first()
        if not result:
            return {"status_code": 404, "message": "Protected Attribute {} does not exist".format(id)}
        session.delete(result)
        session.commit()
        return {"message": "Protected Attribute{} deleted successfully".format(id)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while deleting Protected Attribute"}
    finally:
        if session is not None:
            close_database_session(session)
