import json

from models.json_converter import AlchemyEncoder
from models.user_project_cloud_platform import UserProjectCloudPlatform
from utils.db_util import get_database_session, commit_database_session, close_database_session
from utils.event_util import get_body, get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def add_project_cloud_details(event, context):
    """This is method for adding Project Detail

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        project_cloud_platform = UserProjectCloudPlatform()
        project_cloud_platform.project_id = event_body.get("project_id")
        project_cloud_platform.platform_id = event_body.get("platform_id")
        session = get_database_session()
        session.add(project_cloud_platform)
        commit_database_session(session)
        return {"status_code": 201}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred in creating project cloud platform details"}
    finally:
        if session is not None:
            close_database_session(session)


def update_project_cloud_details(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        if event_body.get("id") is not None:
            session = get_database_session()
            data_to_update = {}
            if event_body.get("platform_id") is not None:
                data_to_update["platform_id"] = event_body.get("platform_id")
            session.query(UserProjectCloudPlatform).filter(UserProjectCloudPlatform.id == event_body.get("id")).update(
                data_to_update)
            commit_database_session(session)
        return {"status_code": 500, "error": "Error occurred while updating Project detail"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while updating Project detail"}
    finally:
        if session is not None:
            close_database_session(session)


def get_project_cloud_details(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        project_id = path_parameter.get("id")
        session = get_database_session()
        if id is not None:
            result = session.query(UserProjectCloudPlatform).filter(
                UserProjectCloudPlatform.project_id == project_id).first()
            if not result:
                return {"status_code": 404, "message": "Project Detail {} does not exist".format(project_id)}
            return json.dumps(result, cls=AlchemyEncoder)
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting project detail"}
    finally:
        if session is not None:
            close_database_session(session)


def delete_project_cloud_details(event, context):
    try:
        session = None
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        id = path_parameter.get("id")
        session = get_database_session()
        result = session.query(UserProjectCloudPlatform).filter(UserProjectCloudPlatform.id == id).first()
        if not result:
            return {"status_code": 404, "message": "Project Cloud Details {} does not exist".format(id)}
        session.delete(result)
        session.commit()
        return {"message": "Project Cloud Detail {} deleted successfully".format(id)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while deleting project cloud detail"}
    finally:
        if session is not None:
            close_database_session(session)
