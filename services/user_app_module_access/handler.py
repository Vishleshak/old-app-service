import json

from models.app_modules import AppModules
from models.json_converter import AlchemyEncoder
from models.user_module_access_details import UserModuleAccessDetails
from utils.db_util import get_database_session, commit_database_session, close_database_session
from utils.event_util import get_body, get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def add_user_module_access(event, context):
    """This is method for user module access

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        session = get_database_session()
        if event_body.get("resultUserRight") is not None and event_body.get("resultUserRight").get(
                "add_access") is not None:
            module_access_list = event_body.get("resultUserRight").get("add_access")
            for module_access in module_access_list:
                user_module_access = UserModuleAccessDetails()
                user_module_access.module_id = module_access.get("module_id")
                user_module_access.user_id = module_access.get("user_id")
                session.add(user_module_access)
            commit_database_session(session)

        if event_body.get("resultUserRight") is not None and event_body.get("resultUserRight").get(
                "remove_access") is not None:
            remove_module_access_list = event_body.get("resultUserRight").get("remove_access")
            for module_access in remove_module_access_list:
                user_module_access = session.query(UserModuleAccessDetails).filter(
                    UserModuleAccessDetails.module_id == module_access.get("module_id"),
                    UserModuleAccessDetails.user_id == module_access.get("user_id")).first()
                if user_module_access is not None:
                    session.delete(user_module_access)
            commit_database_session(session)
        return {"status_code": 201, "accesses": "Module accesses added/removed successfully"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred in adding/removing module level access"}
    finally:
        if session is not None:
            close_database_session(session)


def get_user_module_access(event, context):
    """This is method for user module access

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        user_id = path_parameter.get("user_id")

        if user_id is not None:
            session = get_database_session()
            user_module_access = session.query(UserModuleAccessDetails).filter(
                UserModuleAccessDetails.user_id == user_id).all()
            data = json.dumps(user_module_access, cls=AlchemyEncoder)
            return {"status_code": 200, "accesses": data}
        return {"status_code": 200, "accesses": []}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred in getting module level access"}
    finally:
        if session is not None:
            close_database_session(session)


def get_app_modules(event, context):
    """This is method for user module access

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        session = get_database_session()
        app_modules = session.query(AppModules).all()
        data = json.dumps(app_modules, cls=AlchemyEncoder)
        return {"status_code": 200, "app_modules": data}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred in getting module level access"}
    finally:
        if session is not None:
            close_database_session(session)
