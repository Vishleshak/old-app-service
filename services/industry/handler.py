import json

from models.industry import Industry
from models.json_converter import AlchemyEncoder
from utils.db_util import get_database_session, close_database_session
from utils.event_util import get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def get_industries(event, context):
    session = None
    try:
        logging.debug(event)
        session = get_database_session()
        result = session.query(Industry).all()
        # return {"status_code": 200, "industries": json.dumps(result, cls=AlchemyEncoder)}
        return json.dumps(result, cls=AlchemyEncoder)
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting industries"}
    finally:
        if session is not None:
            close_database_session(session)


def get_industry(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        record_id = path_parameter.get("id")
        session = get_database_session()
        if record_id is not None:
            result = session.query(Industry).filter(Industry.id == record_id).first()
        else:
            result = session.query(Industry).all()
        if not result:
            return {"status_code": 404, "message": "Industry {} does not exist".format(record_id)}
        return json.dumps(result, cls=AlchemyEncoder)
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting industries"}
    finally:
        if session is not None:
            close_database_session(session)
