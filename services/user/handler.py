import json

from models.json_converter import AlchemyEncoder
from models.organization_invitation import OrganizationInvitation
from models.organization_users import OrganizationUser
from models.user import User
from utils.db_util import get_database_session, commit_database_session, close_database_session
from utils.event_util import get_body, get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def add_user(event, context):
    """This is method for adding User

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        user = User()
        user.username = event_body.get("username")
        user.email = event_body.get("email")
        user.auth_id = event_body.get("auth_id")
        user.organization_id = event_body.get("organization_id")
        session = get_database_session()
        session.add(user)
        commit_database_session(session)
        return {"status_code": 201}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred in creating user"}
    finally:
        if session is not None:
            close_database_session(session)


def update_user(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["user_id", "user_role_id", "organization_id", "is_admin", "accessible_modules",
                            "user_rights", "invite_pending", "email"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        if event_body.get("invite_pending"):
            data_to_update = {}
            if event_body.get("accessible_modules") is not None:
                accessible_modules = event_body.get("accessible_modules")
                if len(accessible_modules) == 0:
                    return {"status_code": 50001, "error": "Please provide accessible_modules, it should not be empty",
                            "error_field_name": "accessible_modules"}
                accessible_modules = '^'.join(accessible_modules)
                data_to_update["accessible_modules"] = accessible_modules
            if event_body.get("user_rights") is not None:
                user_rights = event_body.get("user_rights")
                if len(user_rights) == 0:
                    return {"status_code": 50001, "error": "Please provide user_rights, it should not be empty",
                            "error_field_name": "user_rights"}
                user_rights = '^'.join(user_rights)
                data_to_update["user_rights"] = user_rights
            if event_body.get("user_role_id") is not None:
                data_to_update["user_role_id"] = event_body.get("user_role_id")
            if event_body.get("is_admin") is not None:
                data_to_update["is_admin"] = event_body.get("is_admin")

            session = get_database_session()
            if len(data_to_update) > 0:
                session.query(OrganizationInvitation).filter(OrganizationInvitation.email == event_body.get("email"),
                                                             OrganizationInvitation.organization_id == event_body.get(
                                                                 "organization_id")).update(data_to_update)
                commit_database_session(session)
            return {"status_code": 200, "updated": True}
        if event_body.get("user_id") is not None and not event_body.get("invite_pending"):
            session = get_database_session()
            user_data_to_update = {}
            if event_body.get("first_name") is not None:
                user_data_to_update["first_name"] = event_body.get("first_name")
            if event_body.get("last_name") is not None:
                user_data_to_update["last_name"] = event_body.get("last_name")
            if len(user_data_to_update) > 0:
                session.query(User).filter(User.auth_id == event_body.get("user_id")).update(user_data_to_update)
                commit_database_session(session)

            org_user_data_to_update = {"is_admin": event_body.get("is_admin"),
                                       "user_role_id": event_body.get("user_role_id")}
            if event_body.get("user_rights") is not None:
                user_rights = '^'.join(event_body.get("user_rights"))
                org_user_data_to_update["user_rights"] = user_rights
            if event_body.get("accessible_modules") is not None:
                accessible_modules = '^'.join(event_body.get("accessible_modules"))
                org_user_data_to_update["accessible_modules"] = accessible_modules
            session.query(OrganizationUser).filter(OrganizationUser.user_id == event_body.get("user_id"),
                                                   OrganizationUser.organization_id == event_body.get(
                                                       "organization_id")).update(org_user_data_to_update)
            commit_database_session(session)
            return {"status_code": 200, "updated": True}
        return {"status_code": 500, "error": "Error occurred in updating user"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred in updating user"}
    finally:
        if session is not None:
            close_database_session(session)


def get_user(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        record_id = path_parameter.get("id")
        session = get_database_session()
        if record_id is not None:
            result = session.query(User).filter(User.auth_id == record_id).first()
            if not result:
                return {"status_code": 404, "message": "User for organization :  {} does not exist".format(record_id)}
            user_details = json.dumps(result, cls=AlchemyEncoder)
            organization_user = session.query(OrganizationUser).filter(OrganizationUser.user_id == record_id).first()
            if organization_user is not None:
                if organization_user.accessible_modules is not None:
                    organization_user.accessible_modules = organization_user.accessible_modules.split("^")
                if organization_user.user_rights is not None:
                    organization_user.user_rights = organization_user.user_rights.split("^")
                organization_user = json.dumps(organization_user, cls=AlchemyEncoder)
            else:
                organization_invitation = session.query(OrganizationInvitation).filter(
                    OrganizationInvitation.email == result.email).first()
                if organization_invitation is not None:
                    if organization_invitation.accessible_modules is not None:
                        organization_invitation.accessible_modules = organization_invitation.accessible_modules.split(
                            "^")
                    if organization_invitation.user_rights is not None:
                        organization_invitation.user_rights = organization_invitation.user_rights.split("^")
                    organization_invitation = json.dumps(organization_invitation, cls=AlchemyEncoder)
                return {"user_details": user_details, "organization_invitation": organization_invitation}
            return {"user_details": user_details, "organization_user": organization_user}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting User Role"}
    finally:
        if session is not None:
            close_database_session(session)


def get_users_by_organization(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        organization_id = path_parameter.get("organization_id")
        page = path_parameter.get("page")
        if page is None:
            page = 0
        else:
            page = int(str(page))
        session = get_database_session()
        if organization_id is not None:
            result = session.query(User).filter(User.organization_id == organization_id).all()
            if not result:
                return {"status_code": 404,
                        "message": "User for organization :  {} does not exist".format(organization_id)}
            return json.dumps(result, cls=AlchemyEncoder)
        return {"status_code": 404, "message": "Please provide organization_id"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting Users by organization_id"}
    finally:
        if session is not None:
            close_database_session(session)


def delete_user(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        id = path_parameter.get("id")
        session = get_database_session()
        result = session.query(User).filter(User.id == id).first()
        if not result:
            return {"status_code": 404, "message": "user {} does not exist".format(id)}
        session = get_database_session()
        session.delete(result)
        session.commit()
        return {"status_code": 200, "message": "user {} deleted successfully".format(id)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while deleting User Role"}
    finally:
        if session is not None:
            close_database_session(session)
