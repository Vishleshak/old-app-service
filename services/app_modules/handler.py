import json

from models.app_modules import AppModules
from models.json_converter import AlchemyEncoder
from utils.db_util import get_database_session, close_database_session
from utils.event_util import get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def get_app_modules(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        id = path_parameter.get("id")
        session = get_database_session()
        if id is not None:
            result = session.query(AppModules).filter(AppModules.id == id).first()
            print(result)
            print(json.dumps(result, cls=AlchemyEncoder))
        else:
            result = session.query(AppModules).all()
        if not result:
            return {"status_code": 404, "message": "App Modules {} does not exist".format(id)}
        return json.dumps(result, cls=AlchemyEncoder)
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting AppModules"}
    finally:
        if session is not None:
            close_database_session(session)
