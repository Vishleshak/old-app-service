import json

from models.json_converter import AlchemyEncoder
from models.location import Location
from utils.db_util import get_database_session, close_database_session
from utils.event_util import get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def get_location(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        id = path_parameter.get("id")
        if id is None:
            return {"status_code": 500, "message": "Parameter ID is required"}
        session = get_database_session()
        result = session.query(Location).filter(Location.id == id).first()
        if not result:
            return {"status_code": 404, "message": "Location {} does not exist".format(id)}
        return json.dumps(result, cls=AlchemyEncoder)
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting location"}
    finally:
        if session is not None:
            close_database_session(session)


def get_regions(event, context):
    session = None
    try:
        logging.debug(event)
        session = get_database_session()
        result = session.query(Location.region).distinct()
        if not result:
            return {"status_code": 404, "message": "Regions {} does not exist".format(id)}
        return json.dumps(result, cls=AlchemyEncoder)
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting regions"}
    finally:
        if session is not None:
            close_database_session(session)


def get_countries(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        region = path_parameter.get("region")
        if region is None:
            return {"status_code": 500, "message": "Parameter region is required"}
        session = get_database_session()
        result = session.query(Location).filter(Location.region == region).all()
        if not result:
            return {"status_code": 404, "message": "Countries for region {} does not exist".format(region)}
        return json.dumps(result, cls=AlchemyEncoder)
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting countries by region"}
    finally:
        if session is not None:
            close_database_session(session)
