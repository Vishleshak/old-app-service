service: ORGANIZATION-USER-API

provider:
  name: aws
  runtime: python3.8
  stage: ${opt:stage, 'prod'}
  region: ${self:custom.region}
  environment: 
    DATABASE_USERNAME: ${file(../../config/config.${self:provider.stage}.json):DATABASE_USERNAME}
    DATABASE_PASSWORD: ${file(../../config/config.${self:provider.stage}.json):DATABASE_PASSWORD}
    DATABASE_NAME: ${file(../../config/config.${self:provider.stage}.json):DATABASE_NAME}
    DATABASE_PORT: ${file(../../config/config.${self:provider.stage}.json):DATABASE_PORT}
    DATABASE_HOST: ${file(../../config/config.${self:provider.stage}.json):DATABASE_HOST}
  iamRoleStatements:
    - ${self:custom.iamRoleStatements}
  apiGateway:
    restApiId:
      'Fn::ImportValue': kosa-${self:provider.stage}-ApiGateway-restApiId
    restApiRootResourceId:
      'Fn::ImportValue': kosa-${self:provider.stage}-ApiGateway-rootResourceId

custom: ${file(../../config/config.yaml)}

package:
  include:
  - ../../utils/**
  - ../../models/**


functions:
  add_organization_user:
    handler: handler.add_organization_user
    name: ${self:provider.stage}-${self:service}-add-organization-user
    description: 
    timeout: 30
    events:
      - http:
          path: organization-user
          method: post
          cors: ${self:custom.corsStatements}
          integration: lambda
          response:
            headers:
              Content-Type: "'application/json'"
          authorizer:
            type: CUSTOM
            authorizerId: ${cf:AUTH-SERVICE-${self:provider.stage}.apiGatewayAuthorizer}

  get_user_organizations:
    handler: handler.get_user_organizations
    name: ${self:provider.stage}-${self:service}-get-user-organizations
    description:
    timeout: 30
    events:
      - http:
          path: user-organizations/{user_id}
          method: get
          cors: ${self:custom.corsStatements}
          integration: lambda
          parameters:
            paths:
              user_id: true
          response:
            headers:
              Content-Type: "'application/json'"
          authorizer:
            type: CUSTOM
            authorizerId: ${cf:AUTH-SERVICE-${self:provider.stage}.apiGatewayAuthorizer}

  delete_organization_user:
    handler: handler.delete_organization_user
    name: ${self:provider.stage}-${self:service}-delete-organization-user
    description:
    timeout: 30
    events:
      - http:
          path: delete-organization-user/{user_id}/{organization_id}
          method: delete
          cors: ${self:custom.corsStatements}
          integration: lambda
          parameters:
            paths:
              user_id: true
              organization_id: true
          response:
            headers:
              Content-Type: "'application/json'"
          authorizer:
            type: CUSTOM
            authorizerId: ${cf:AUTH-SERVICE-${self:provider.stage}.apiGatewayAuthorizer}

  get_organization_users:
    handler: handler.get_organization_users
    name: ${self:provider.stage}-${self:service}-get-organization-users
    description:
    timeout: 30
    events:
      - http:
          path: organization-user/{id}
          method: get
          cors: ${self:custom.corsStatements}
          integration: lambda
          parameters:
            paths:
              id: true
          response:
            headers:
              Content-Type: "'application/json'"
          authorizer:
            type: CUSTOM
            authorizerId: ${cf:AUTH-SERVICE-${self:provider.stage}.apiGatewayAuthorizer}

  get_org_user_rights:
    handler: handler.get_user_rights
    name: ${self:provider.stage}-${self:service}-get-org-user-rights
    description:
    timeout: 30
    events:
      - http:
          path: organization-user-rights/{organization_id}/{user_id}
          method: get
          cors: ${self:custom.corsStatements}
          integration: lambda
          parameters:
            paths:
              user_id: true
              organization_id: true
          response:
            headers:
              Content-Type: "'application/json'"
          authorizer:
            type: CUSTOM
            authorizerId: ${cf:AUTH-SERVICE-${self:provider.stage}.apiGatewayAuthorizer}

plugins:
  - serverless-python-requirements
  - serverless-pseudo-parameters
  - serverless-prune-plugin