import json

from models.json_converter import AlchemyEncoder
from models.organization import Organization
from models.organization_invitation import OrganizationInvitation
from models.organization_users import OrganizationUser
from models.user import User
from utils.db_util import get_database_session, commit_database_session, close_database_session
from utils.event_util import get_body, get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def add_organization_user(event, context):
    """This is method for adding organization users

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["user_id", "user_role_id", "organization_id", "is_admin"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        organization_user = OrganizationUser()
        organization_user.user_id = event_body.get("user_id")
        organization_user.user_role_id = event_body.get("user_role_id")
        organization_user.organization_id = event_body.get("organization_id")
        organization_user.is_admin = event_body.get("is_admin")
        session = get_database_session()
        session.add(organization_user)
        commit_database_session(session)
        return {"status_code": 201, "organization": json.dumps(organization_user, cls=AlchemyEncoder)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred in creating organization"}
    finally:
        if session is not None:
            close_database_session(session)


def get_organization_users(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        organization_id = path_parameter.get("id")
        session = get_database_session()
        organization_users = []
        if organization_id is not None:
            result = session.query(OrganizationUser.organization_id, OrganizationUser.user_id,
                                   User.first_name, User.last_name, User.email).filter(
                OrganizationUser.organization_id == organization_id,
                OrganizationUser.is_deleted == 0).join(User,
                                                       OrganizationUser.user_id == User.auth_id).all()
            emails = []
            if result is not None:
                for obj in result:
                    data = {"organization_id": obj[0], "user_id": obj[1], "first_name": obj[2],
                            "last_name": obj[3], "email": obj[4], "invitation_accepted": 1}
                    emails.append(obj[4])
                    organization_users.append(data)

            pending_invitations = []
            organization_invitations = session.query(OrganizationInvitation).filter(
                OrganizationInvitation.organization_id == organization_id,
                OrganizationInvitation.is_deleted == 0).all()
            if organization_invitations is not None:
                for obj in organization_invitations:
                    if obj.email not in emails:
                        data = {"organization_id": obj.organization_id, "invitation_id": obj.id, "email": obj.email,
                                "user_id": "", "first_name": "", "last_name": "", "invitation_accepted": 2}
                        pending_invitations.append(data)

            return {"status_code": 200, "organization_users": organization_users,
                    "pending_invitations": pending_invitations}
        return {"status_code": 404, "message": "Please pass organization id"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting organization users"}
    finally:
        if session is not None:
            close_database_session(session)


def delete_organization_user(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        user_id = path_parameter.get("user_id")
        organization_id = path_parameter.get("organization_id")
        session = get_database_session()
        result = session.query(OrganizationUser).filter(OrganizationUser.user_id == user_id,
                                                        OrganizationUser.organization_id == organization_id).first()
        if not result:
            return {"status_code": 404, "message": "Organization user with id {} for organization id"
                                                   " {} does not exist".format(organization_id, user_id)}
        session.delete(result)
        session.commit()
        return {"message": "Organization user with id {} for organization id"
                           " {} deleted successfully".format(organization_id, user_id)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while deleting organization"}
    finally:
        if session is not None:
            close_database_session(session)


def get_user_organizations(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        user_id = path_parameter.get("user_id")
        session = get_database_session()

        result = session.query(OrganizationUser.organization_id, OrganizationUser.user_id,
                               Organization.name).filter(OrganizationUser.user_id == user_id,
                                                         OrganizationUser.is_deleted == 0).join(Organization,
                                                                                                OrganizationUser.
                                                                                                organization_id ==
                                                                                                Organization.id).all()

        if not result:
            return {"status_code": 404, "message": "Organization user with id "
                                                   " {} does not exist".format(user_id)}
        user_organizations = []
        for obj in result:
            data = {"organization_id": obj[0], "user_id": obj[1], "name": obj[2]}
            user_organizations.append(data)
        return {"status_code": 200, "organizations": user_organizations}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred getting users organization"}
    finally:
        if session is not None:
            close_database_session(session)


def get_user_rights(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        user_id = path_parameter.get("user_id")
        organization_id = path_parameter.get("organization_id")
        if user_id is None:
            return {"status_code": 50001,
                    "error": "Please provide user_id, it should not be empty",
                    "error_field_name": "user_id"}

        if organization_id is None:
            return {"status_code": 50001,
                    "error": "Please provide organization_id, it should not be empty",
                    "error_field_name": "user_id"}

        session = get_database_session()
        result = session.query(OrganizationUser).filter(OrganizationUser.user_id == user_id,
                                                        OrganizationUser.organization_id == organization_id,
                                                        OrganizationUser.is_deleted == 0).first()

        if not result:
            return {"status_code": 200, "organization_rights": []}
        organization_rights = {"user_role_id": result.user_role_id, "is_admin": result.is_admin}
        accessible_modules = result.accessible_modules
        if accessible_modules is not None and "^" in accessible_modules:
            accessible_modules = accessible_modules.split("^")
        organization_rights["accessible_modules"] = accessible_modules
        user_rights = result.user_rights
        if user_rights is not None and "^" in user_rights:
            user_rights = user_rights.split("^")
        organization_rights["user_rights"] = user_rights
        return {"status_code": 200, "organization_rights": organization_rights}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred getting users organization"}
    finally:
        if session is not None:
            close_database_session(session)
