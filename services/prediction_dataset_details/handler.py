import json
import uuid

from utils.aws_util import validate_aws_credentials, validate_s3_access

from models.json_converter import AlchemyEncoder
from models.prediction_dataset_details import PredictionDatasetDetails
from utils.db_util import get_database_session, commit_database_session, close_database_session
from utils.event_util import get_body, get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def add_prediction_dataset_details(event, context):
    """This is method for adding S3 Bucket Details

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["user_id", "project_id", "bucket_key", "s3_access_key", "s3_secret_key"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}
        if event_body.get("storage_type") != "kosa":
            if not validate_aws_credentials(event_body.get("s3_access_key"), event_body.get("s3_secret_key")):
                return {"status_code": 50004,
                        "error": "Invalid access key id or secret key is provided please check and enter valid keys"}
            if not validate_s3_access(event_body.get("s3_access_key"), event_body.get("s3_secret_key"),
                                      event_body.get("bucket_key")):
                return {"status_code": 50005,
                        "error": "The given S3 bucket is either not present or you are not having access to the given bucket"}

        prediction_dataset_details = get_prediction_dataset_details(event_body)
        uid = str(uuid.uuid4())
        prediction_dataset_details.id = uid
        session = get_database_session()
        session.add(prediction_dataset_details)
        commit_database_session(session)
        return {"status_code": 201, "data": {"id": uid}}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "data": {"error": "Error occurred in updating prediction dataset details"}}
    finally:
        if session is not None:
            close_database_session(session)


def update_prediction_dataset_details(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["user_id", "project_id", "bucket_key", "s3_access_key", "s3_secret_key"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}
        if event_body.get("storage_type") != "kosa":
            if not validate_aws_credentials(event_body.get("s3_access_key"), event_body.get("s3_secret_key")):
                return {"status_code": 50004,
                        "error": "Invalid access key id or secret key is provided please check and enter valid keys"}
            if not validate_s3_access(event_body.get("s3_access_key"), event_body.get("s3_secret_key"),
                                      event_body.get("bucket_key")):
                return {"status_code": 50005,
                        "error": "The given S3 bucket is either not present or you are not having access to the given bucket"}

        user_id = event_body.get("user_id")
        session = get_database_session()
        prediction_dataset_details_from_db = session.query(PredictionDatasetDetails).filter(
            PredictionDatasetDetails.user_id == user_id).first()
        if prediction_dataset_details_from_db is None:
            prediction_dataset_details = get_prediction_dataset_details(event_body)
            uid = str(uuid.uuid4())
            prediction_dataset_details.id = uid
            session.add(prediction_dataset_details)
            commit_database_session(session)
            return {"status_code": 200, "data": {"id": uid}}

        if event_body.get("id") is not None:
            session.query(PredictionDatasetDetails).filter(PredictionDatasetDetails.id == event_body.get("id")).update(
                {'bucket_key': event_body.get("bucket_key"), 's3_access_key': event_body.get("s3_access_key"),
                 's3_secret_key': event_body.get("s3_secret_key"), 'storage_type': event_body.get("storage_type")})
            commit_database_session(session)
            return {"status_code": 200, "data": {"message": "Prediction dataset details are updated successfully"}}
        else:
            return {"status_code": 50001, "error": "Please provide id, it should not be empty",
                    "error_field_name": "id"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "data": {"error": "Error occurred in updating prediction dataset details"}}
    finally:
        if session is not None:
            close_database_session(session)


def get_prediction_dataset_details(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        user_id = path_parameter.get("user_id")
        session = get_database_session()
        if user_id is not None:
            result = session.query(PredictionDatasetDetails).filter(PredictionDatasetDetails.user_id == user_id).first()
            if not result:
                return {"status_code": 404, "data": {"message": "S3 Bucket Details {} does not exist".format(user_id)}}
            return {"status_code": 200, "data": json.loads(json.dumps(result, cls=AlchemyEncoder))}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "data": {"error": "Error occurred in Updating S3 Bucket Details"}}
    finally:
        if session is not None:
            close_database_session(session)


def get_prediction_dataset_details_by_project(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        project_id = path_parameter.get("project_id")
        session = get_database_session()
        if project_id is not None:
            result = session.query(PredictionDatasetDetails).filter(
                PredictionDatasetDetails.project_id == project_id).first()
            if not result:
                return {"status_code": 404,
                        "data": {"message": "S3 Bucket Details for project {} does not exist".format(project_id)}}
            return {"status_code": 200, "data": json.loads(json.dumps(result, cls=AlchemyEncoder))}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "data": {"error": "Error occurred in Updating S3 Bucket Details"}}
    finally:
        if session is not None:
            close_database_session(session)


def delete_prediction_dataset_details(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        id = path_parameter.get("details_id")
        session = get_database_session()
        result = session.query(PredictionDatasetDetails).filter(PredictionDatasetDetails.id == id).first()
        if not result:
            return {"status_code": 404, "message": "S3 Bucket Details {} does not exist".format(id)}
        session.delete(result)
        session.commit()
        return {"message": "S3 Bucket Details{} deleted successfully".format(id)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while deleting S3 Bucket Detail"}
    finally:
        if session is not None:
            close_database_session(session)


def get_prediction_dataset_details(event_body):
    prediction_dataset_details = PredictionDatasetDetails()
    if event_body.get("id") is not None:
        prediction_dataset_details.id = event_body.get("id")
    prediction_dataset_details.user_id = event_body.get("user_id")
    prediction_dataset_details.project_id = event_body.get("project_id")
    prediction_dataset_details.cloud_id = event_body.get("cloud_id")
    prediction_dataset_details.storage_type = event_body.get("storage_type")
    prediction_dataset_details.bucket_key = event_body.get("bucket_key")
    prediction_dataset_details.s3_access_key = event_body.get("s3_access_key")
    prediction_dataset_details.s3_secret_key = event_body.get("s3_secret_key")
    return prediction_dataset_details
