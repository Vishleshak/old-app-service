import logging
import os

from utils.jwt_util import jwt_verify

AUTH0_CLIENT_ID = os.getenv('AUTH0_CLIENT_ID')
AUTH0_CLIENT_PUBLIC_KEY = os.getenv('AUTH0_CLIENT_PUBLIC_KEY')


def auth(event, context):
    """This is method of Authorization 

    Args:
        event ([dict]): pass the event 
        context ([object]): pass the context

    Raises:
        Exception: Raises the exception 'Unauthorized!! Auth token is missing'
        Exception: Raises the exception 'Unauthorized!! Invalid token method'
        Exception: Raises the exception 'Unauthorized!! Missing auth token'
        ex: Raises the exception

    Returns:
        [string]: returns the policy
    """
    logging.debug(event)
    complete_auth_token = event.get('authorizationToken')
    if not complete_auth_token:
        raise Exception('Unauthorized!! Auth token is missing')

    tokens = complete_auth_token.split(' ')
    auth_token = tokens[1]
    token_method = tokens[0]

    if not token_method.lower() == 'bearer':
        logging.error("Invalid token method {}".format(token_method))
        raise Exception('Unauthorized!! Invalid token method')
    if not auth_token:
        logging.error("Auth token is missing")
        raise Exception('Unauthorized!! Missing auth token')

    try:
        principal_id = jwt_verify(auth_token, AUTH0_CLIENT_PUBLIC_KEY)
        arn = event['methodArn']
        for request_type in ['GET', 'POST', 'PUT', 'DELETE']:
            slug = '/' + request_type + '/'
            if slug in arn:
                arn = arn.split(slug)[0]

        arn += '/*'
        policy = generate_policy(principal_id, 'Allow', arn)
        return policy
    except Exception as ex:
        logging.error(f'Exception encountered: {ex}')
        raise ex


def generate_policy(principal_id, effect, resource):
    """This is method for generate the policy

    Args:
        principal_id ([string]): pass the principal_id
        effect ([string]): pass the effect
        resource ([string]): pass the resource

    Returns:
        [json]: returns policy details
    """

    return {
        'principalId': principal_id,
        'policyDocument': {
            'Version': '2012-10-17',
            'Statement': [
                {
                    'Action': 'execute-api:Invoke',
                    'Effect': effect,
                    'Resource': resource

                }
            ]
        }
    }


def format_public_key(public_key):
    """This is method for formating public key

    Args:
        public_key ([String]): Pass the public key

    Returns:
        [string]: Returns public key
    """
    public_key = public_key.replace('\n', ' ').replace('\r', '')
    public_key = public_key.replace('-----BEGIN CERTIFICATE-----',
                                    '-----BEGIN CERTIFICATE-----\n')
    public_key = public_key.replace('-----END CERTIFICATE-----',
                                    '\n-----END CERTIFICATE-----')
    return public_key
