service: AUTH-SERVICE

provider:
  name: aws
  runtime: python3.8
  stage: ${opt:stage, 'prod'}
  region: ${file(../../config/config.${self:provider.stage}.json):REGION}
  environment:
    AUTH0_CLIENT_ID: ${file(../../config/config.${self:provider.stage}.json):AUTH0_CLIENT_ID}
    AUTH0_CLIENT_PUBLIC_KEY: ${file(../../config/public_key.${self:provider.stage})}
    AUTHORIZER_ARN: arn:aws:lambda:${self:provider.region}:#{AWS::AccountId}:function:${self:provider.stage}-AUTH-SERVICE-auth
  apiGateway:
    restApiId:
      Ref: webapiApiGateway
    restApiResources:
      Fn::GetAtt:
        - webapiApiGateway
        - RootResourceId

package:
  include:
  - ../../utils/**
  - ../../models/**
        
custom:
  region: ${file(../../config/config.${self:provider.stage}.json):REGION}
  pythonRequirements:
    dockerizePip: true
    fileName: ../../requirements/requirements.txt
    slim: true
    useDownloadCache: false
    useStaticCache: false
  prune:
    automatic: true
    number: 3

functions:
  auth:
    handler: handler.auth
    name: ${self:provider.stage}-${self:service}-auth
    cors: true

resources:
  Resources:
    webapiApiGateway:
      Type: AWS::ApiGateway::RestApi
      Properties:
        Name: KOSA-API-GATEWAY-${self:provider.stage}

    ApiGatewayAuthorizer:
      Type: AWS::ApiGateway::Authorizer
      Properties:
        AuthorizerResultTtlInSeconds: 300
        IdentitySource: method.request.header.Authorization
        Name: kosaAuthorizer
        RestApiId:
          Ref: webapiApiGateway
        Type: TOKEN
        IdentityValidationExpression: '^Bearer [-0-9a-zA-z\.]*$'
        AuthorizerUri: arn:aws:apigateway:${self:provider.region}:lambda:path/2015-03-31/functions/${self:provider.environment.AUTHORIZER_ARN}/invocations
        AuthorizerCredentials:
          Fn::GetAtt:
            - ApiGatewayAuthorizerRole
            - Arn

    ApiGatewayAuthorizerRole:
      Type: AWS::IAM::Role
      Properties:
        AssumeRolePolicyDocument:
          Version: '2012-10-17'
          Statement:
            - Effect: 'Allow'
              Principal:
                Service:
                  - 'apigateway.amazonaws.com'
              Action:
                - sts:AssumeRole
        Policies:
          - PolicyName: 'InvokeAuthorizerFunction-${self:provider.stage}'
            PolicyDocument:
              Version: '2012-10-17'
              Statement:
                - Effect: 'Allow'
                  Action:
                    - lambda:InvokeAsync
                    - lambda:InvokeFunction
                  Resource: ${self:provider.environment.AUTHORIZER_ARN}

  # Api Gateway resources to make the API Base URL unchanging
  Outputs:
    apiGatewayRestApiId:
      Value:
        Ref: webapiApiGateway
      Export:
        Name: kosa-${self:provider.stage}-ApiGateway-restApiId

    apiGatewayRestApiRootResourceId:
      Value:
          Fn::GetAtt:
          - webapiApiGateway
          - RootResourceId
      Export:
        Name: kosa-${self:provider.stage}-ApiGateway-rootResourceId

    apiGatewayAuthorizer:
      Value:
        Ref: ApiGatewayAuthorizer
      Export:
        Name: kosa-${self:provider.stage}-ApiGateway-Authorizer

plugins:
  - serverless-python-requirements
  - serverless-pseudo-parameters
  - serverless-prune-plugin
