import os

import boto3

from models.project_detail import ProjectDetail
from models.training_dataset_details import TrainingDatasetDetails
from utils.db_util import get_database_session, close_database_session
from utils.event_util import get_body, get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def training_dataset_list(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        project_id = path_parameter.get("id")
        if project_id is not None:
            session = get_database_session()
            project_details = session.query(ProjectDetail).filter(ProjectDetail.id == project_id).first()
            sesssion = None
            bucket_key = None
            bucket_name = None
            storage_type = None
            dataset_id = None
            if project_details is not None:
                s3_bucket_details = session.query(TrainingDatasetDetails).filter(
                    TrainingDatasetDetails.project_id == project_id).first()
                if s3_bucket_details is not None:
                    dataset_id = s3_bucket_details.id
                    if s3_bucket_details.storage_type == "kosa":
                        sesssion = boto3.Session(os.environ['S3_ACCESS_KEY'], os.environ['S3_SECRET_KEY'])
                        bucket_name = os.environ['DEFAULT_STORAGE_BUCKET']
                        bucket_key = "{USER_ID}/{PROJECT_ID}/{DATASET_TYPE}".format(USER_ID=project_details.user_id,
                                                                                    PROJECT_ID=project_id,
                                                                                    DATASET_TYPE="training_dataset")
                        storage_type = "kosa"
                    else:
                        sesssion = boto3.Session(s3_bucket_details.s3_access_key, s3_bucket_details.s3_secret_key)
                        bucket_name = s3_bucket_details.bucket_key
                        storage_type = "AWS"
                    s3 = sesssion.resource("s3")
                    bucket = s3.Bucket(bucket_name)
                    result = {}
                    dataset_list = []
                    if bucket_key is None:
                        for obj in bucket.objects.all():
                            data_details = {}
                            data_details["bucket_name"] = obj.bucket_name
                            data_details["key"] = obj.key
                            data_details["s3_url"] = "s3://{}/{}".format(obj.bucket_name, obj.key)
                            dataset_list.append(data_details)
                    else:
                        for obj in bucket.objects.filter(Prefix=bucket_key):
                            data_details = {}
                            data_details["bucket_name"] = obj.bucket_name
                            data_details["key"] = obj.key.split("/")[-1]
                            data_details["s3_url"] = "s3://{}/{}".format(obj.bucket_name, obj.key)
                            dataset_list.append(data_details)
                    result["datasets"] = dataset_list
                    return {"status_code": 200, "result": result, "bucket_key": s3_bucket_details.bucket_key,
                            "storage_type": storage_type, "id": dataset_id}
                return {"status_code": 200, "bucket_key": bucket_name, "storage_type": storage_type, "id": dataset_id}
            return {"status_code": 500, "error": "No S3 bucket found"}
        return {"status_code": 500, "error": "Please enter project id"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Exception occurred!!"}
    finally:
        if session is not None:
            close_database_session(session)


def bucket_data_list_no_cred_store(event, context):
    try:
        logging.debug(event)
        event_body = get_body(event)
        result = get_s3_data(event_body)
        return {"status_code": 200, "result": result}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred"}


def get_s3_data(event_body):
    logging.debug(event_body)
    result = {}
    sesssion = boto3.Session(event_body.get("aws_access_key"), event_body.get("aws_secret_key"))
    s3 = sesssion.resource("s3")
    bucket = s3.Bucket(event_body.get("bucket_name"))
    dataset_list = []
    for obj in bucket.objects.all():
        data_details = {"bucket_name": obj.bucket_name, "key": obj.key,
                        "s3_url": "s3://{}/{}".format(obj.bucket_name, obj.key)}
        dataset_list.append(data_details)
    result["datasets"] = dataset_list
    return result
