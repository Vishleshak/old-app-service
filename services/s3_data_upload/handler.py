import os

import boto3
from botocore.client import Config

from models.model_dataset_details import ModelDatasetDetails
from models.prediction_dataset_details import PredictionDatasetDetails
from models.project_dataset_storage_details import ProjectDatasetStorageDetails
from models.project_detail import ProjectDetail
from models.project_model_storage_details import ProjectModelStorageDetails
from models.training_dataset_details import TrainingDatasetDetails
from utils.db_util import get_database_session, close_database_session
from utils.event_util import get_body
from utils.log_util import get_logger

logging = get_logger(__name__)


def get_presigned_url(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        use_kosa_storage = event_body.get("use_kosa_storage")
        user_id = event_body.get("user_id")
        project_id = event_body.get("project_id")
        file_name = event_body.get("file_name")
        dataset_type = event_body.get("dataset_type")
        content_type = event_body.get("content_type")
        s3 = None
        dataset_details = None
        if project_id is None:
            return {"status_code": 503, "data": {"message": "Please provide project_id"}}
        if user_id is None:
            return {"status_code": 503, "data": {"message": "Please provide user_id"}}
        if file_name is None:
            return {"status_code": 503, "data": {"message": "Please provide file_name"}}
        if use_kosa_storage is None:
            return {"status_code": 503, "data": {"message": "Please provide storage details"}}
        if dataset_type is None:
            return {"status_code": 503, "data": {"message": "Please provide dataset_type"}}
        if use_kosa_storage:
            logging.info("Creating presined url against kosa default storage")
            bucket_name = os.environ['DEFAULT_STORAGE_BUCKET']
            s3 = boto3.client('s3', region_name='us-east-2', aws_access_key_id=os.environ['S3_ACCESS_KEY'],
                              aws_secret_access_key=os.environ['S3_SECRET_KEY'],
                              config=Config(signature_version='s3v4'))
            # TODO : Fetch data from settings
        else:
            session = get_database_session()
            if dataset_type == "prediction_dataset":
                dataset_details = session.query(PredictionDatasetDetails).filter(
                    PredictionDatasetDetails.project_id == project_id).first()
                if not dataset_details:
                    return {"status_code": 404,
                            "data": {"message": "S3 Bucket Details for project {} does not exist".format(project_id)}}
            if dataset_type == "model_dataset":
                dataset_details = session.query(ModelDatasetDetails).filter(
                    ModelDatasetDetails.project_id == project_id).first()
                if not dataset_details:
                    return {"status_code": 404,
                            "data": {"message": "S3 Bucket Details for project {} does not exist".format(project_id)}}
            if dataset_type == "training_dataset":
                dataset_details = session.query(TrainingDatasetDetails).filter(
                    TrainingDatasetDetails.project_id == project_id).first()
                if not dataset_details:
                    return {"status_code": 404,
                            "data": {"message": "S3 Bucket Details for project {} does not exist".format(project_id)}}
            s3 = boto3.client('s3', region_name='us-east-2', aws_access_key_id=dataset_details.s3_access_key,
                              aws_secret_access_key=dataset_details.s3_secret_key,
                              config=Config(signature_version='s3v4'))
            logging.info("Creating presined url in user storage")
            bucket_name = dataset_details.bucket_key
        object_key = "{USER_ID}/{PROJECT_ID}/{DATASET_TYPE}/{FILE_NAME}".format(USER_ID=user_id, PROJECT_ID=project_id,
                                                                                DATASET_TYPE=dataset_type,
                                                                                FILE_NAME=file_name)
        url = s3.generate_presigned_url(ClientMethod='put_object', Params={'Bucket': bucket_name, 'Key': object_key},
                                        ExpiresIn=3600)
        return {"url": url}
    except Exception as ex:
        logging.error(ex)
    finally:
        if session is not None:
            close_database_session(session)


def get_pre_signed_url(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        use_kosa_storage = event_body.get("use_kosa_storage")
        user_id = event_body.get("user_id")
        project_id = event_body.get("project_id")
        file_name = event_body.get("file_name")
        dataset_type = event_body.get("dataset_type")

        mandatory_fields = ["use_kosa_storage", "project_id", "user_id", "file_name", "dataset_type"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        s3 = None
        dataset_details = None
        if use_kosa_storage:
            logging.info("Creating pre-signed url against kosa default storage")
            bucket_name = os.environ['DEFAULT_STORAGE_BUCKET']
            s3 = boto3.client('s3', region_name='us-east-2', aws_access_key_id=os.environ['S3_ACCESS_KEY'],
                              aws_secret_access_key=os.environ['S3_SECRET_KEY'],
                              config=Config(signature_version='s3v4'))
        else:
            session = get_database_session()
            if dataset_type == "dataset":
                dataset_details = session.query(ProjectDatasetStorageDetails).filter(
                    ProjectDatasetStorageDetails.project_id == project_id).first()
                if not dataset_details:
                    return {"status_code": 404,
                            "data": {"message": "S3 Bucket Details for project {} does not exist".format(project_id)}}
            if dataset_type == "model":
                dataset_details = session.query(ProjectModelStorageDetails).filter(
                    ProjectModelStorageDetails.project_id == project_id).first()
                if not dataset_details:
                    return {"status_code": 404,
                            "data": {"message": "S3 Bucket Details for project {} does not exist".format(project_id)}}
            s3 = boto3.client('s3', region_name='us-east-2', aws_access_key_id=dataset_details.s3_access_key,
                              aws_secret_access_key=dataset_details.s3_secret_key,
                              config=Config(signature_version='s3v4'))
            logging.info("Creating pre-signed url in user storage")
            bucket_name = dataset_details.bucket_key
        object_key = "{USER_ID}/{PROJECT_ID}/{DATASET_TYPE}/{FILE_NAME}".format(USER_ID=user_id, PROJECT_ID=project_id,
                                                                                DATASET_TYPE=dataset_type,
                                                                                FILE_NAME=file_name)
        url = s3.generate_presigned_url(ClientMethod='put_object', Params={'Bucket': bucket_name, 'Key': object_key},
                                        ExpiresIn=3600)
        s3_url = "s3://{BUCKET_NAME}/{OBJECT_KEY}".format(BUCKET_NAME=bucket_name, OBJECT_KEY=object_key)
        return {"pre_signed_url": url, "s3_url": s3_url}
    except Exception as ex:
        logging.error(ex)
    finally:
        if session is not None:
            close_database_session(session)


def delete_data(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        project_id = event_body.get("project_id")
        dataset_type = event_body.get("dataset_type")
        s3_url = event_body.get("s3_url")
        session = get_database_session()
        project_details = session.query(ProjectDetail).filter(ProjectDetail.id == project_id).first()
        if project_details is not None:
            s3_bucket_details = None
            if dataset_type == "prediction_dataset":
                s3_bucket_details = session.query(PredictionDatasetDetails).filter(
                    PredictionDatasetDetails.project_id == project_id).first()
                if not s3_bucket_details:
                    return {"status_code": 404,
                            "data": {"message": "S3 Bucket Details for project {} does not exist".format(project_id)}}
            if dataset_type == "model_dataset":
                s3_bucket_details = session.query(ModelDatasetDetails).filter(
                    ModelDatasetDetails.project_id == project_id).first()
                if not s3_bucket_details:
                    return {"status_code": 404,
                            "data": {"message": "S3 Bucket Details for project {} does not exist".format(project_id)}}
            if dataset_type == "training_dataset":
                s3_bucket_details = session.query(TrainingDatasetDetails).filter(
                    TrainingDatasetDetails.project_id == project_id).first()
                if not s3_bucket_details:
                    return {"status_code": 404,
                            "data": {"message": "S3 Bucket Details for project {} does not exist".format(project_id)}}
            if s3_bucket_details is not None:
                s3 = None
                bucket_key = None
                bucket_name = None
                if s3_bucket_details.storage_type == "kosa":
                    s3 = boto3.client("s3", aws_access_key_id=os.environ['S3_ACCESS_KEY'],
                                      aws_secret_access_key=os.environ['S3_SECRET_KEY'])
                else:
                    s3 = boto3.client("s3", aws_access_key_id=s3_bucket_details.s3_access_key,
                                      aws_secret_access_key=s3_bucket_details.s3_secret_key)
                s3_url = s3_url.replace("s3://", "")
                bucket_name = s3_url.split("/")[0]
                bucket_key = s3_url.replace(s3_url.split("/")[0] + "/", "")
                s3.delete_object(Bucket=bucket_name, Key=bucket_key)
                return {"status_code": 200, "file_deleted": True}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "file_deleted": False}
    finally:
        if session is not None:
            close_database_session(session)
