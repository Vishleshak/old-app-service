import boto3

from models.s3_bucket_details import S3BucketDetails
from utils.db_util import get_database_session
from utils.event_util import get_body, get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def bucket_data_list(event, context):
    logging.debug(event)
    path_parameter = get_path_parameters(event)
    project_id = path_parameter.get("id")
    session = None
    try:
        if project_id is not None:
            session = get_database_session()
            s3_bucket_details = session.query(S3BucketDetails).filter(S3BucketDetails.project_id == project_id).first()
            if s3_bucket_details is not None:
                sesssion = boto3.Session(s3_bucket_details.aws_access_key, s3_bucket_details.aws_secret_key)
                s3 = sesssion.resource("s3")
                bucket = s3.Bucket(s3_bucket_details.bucket_key)
                result = {}
                dataset_list = []
                for obj in bucket.objects.all():
                    data_details = {}
                    data_details["bucket_name"] = obj.bucket_name
                    data_details["key"] = obj.key
                    dataset_list.append(data_details)
                result["datasets"] = dataset_list
                return {"status_code": 200, "result": result}
            return {"status_code": 500, "error": "No S3 bucket found"}
        return {"status_code": 500, "error": "Please enter project id"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred quering"}
    finally:
        if session is not None:
            close_database_session(session)


def bucket_data_list_no_cred_store(event, context):
    try:
        logging.debug(event)
        event_body = get_body(event)
        result = get_s3_data(event_body)
        return {"status_code": 200, "result": result}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred"}


def get_s3_data(event_body):
    logging.debug(event_body)
    result = {}
    session = boto3.Session(event_body.get("aws_access_key"), event_body.get("aws_secret_key"))
    s3 = session.resource("s3")
    bucket = s3.Bucket(event_body.get("bucket_name"))
    dataset_list = []
    for obj in bucket.objects.all():
        data_details = {"bucket_name": obj.bucket_name, "key": obj.key,
                        "s3_url": "s3://{}/{}".format(obj.bucket_name, obj.key)}
        dataset_list.append(data_details)
    result["datasets"] = dataset_list
    return result
