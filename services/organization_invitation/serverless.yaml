service: ORGANIZATION-INVITE-API

provider:
  name: aws
  runtime: python3.8
  stage: ${opt:stage, 'prod'}
  region: ${self:custom.region}
  environment:
    DATABASE_USERNAME: ${file(../../config/config.${self:provider.stage}.json):DATABASE_USERNAME}
    DATABASE_PASSWORD: ${file(../../config/config.${self:provider.stage}.json):DATABASE_PASSWORD}
    DATABASE_NAME: ${file(../../config/config.${self:provider.stage}.json):DATABASE_NAME}
    DATABASE_PORT: ${file(../../config/config.${self:provider.stage}.json):DATABASE_PORT}
    DATABASE_HOST: ${file(../../config/config.${self:provider.stage}.json):DATABASE_HOST}
    STAGE: ${file(../../config/config.${self:provider.stage}.json):STAGE}
  iamRoleStatements:
    - ${self:custom.iamRoleStatements}
    - ${self:provider.sesRoleStatements}
  apiGateway:
    restApiId:
      'Fn::ImportValue': kosa-${self:provider.stage}-ApiGateway-restApiId
    restApiRootResourceId:
      'Fn::ImportValue': kosa-${self:provider.stage}-ApiGateway-rootResourceId
  sesRoleStatements:
    Effect: Allow
    Action:
      - ses:SendEmail
      - ses:SendRawEmail
    Resource:
      - arn:aws:ses:us-east-2:085964451742:identity/kosa.ai

custom: ${file(../../config/config.yaml)}

package:
  include:
  - ../../utils/**
  - ../../models/**


functions:
  add_organization_invite:
    handler: handler.add_organization_invite
    name: ${self:provider.stage}-${self:service}-add-org-invite
    description:
    timeout: 30
    events:
      - http:
          path: organization-invite
          method: post
          cors: ${self:custom.corsStatements}
          integration: lambda
          response:
            headers:
              Content-Type: "'application/json'"
          authorizer:
            type: CUSTOM
            authorizerId: ${cf:AUTH-SERVICE-${self:provider.stage}.apiGatewayAuthorizer}

  get_org_invite_details:
    handler: handler.get_org_invite_details
    name: ${self:provider.stage}-${self:service}-get-org-invite-details
    description:
    timeout: 30
    events:
      - http:
          path: organization-invite/{invitation_hash}
          method: get
          cors: ${self:custom.corsStatements}
          integration: lambda
          parameters:
            paths:
              invitation_hash: true
          response:
            headers:
              Content-Type: "'application/json'"
          authorizer:
            type: CUSTOM
            authorizerId: ${cf:AUTH-SERVICE-${self:provider.stage}.apiGatewayAuthorizer}

  get_org_invite_details_by_id:
    handler: handler.get_org_invite_details_by_id
    name: ${self:provider.stage}-${self:service}-get-org-invite-details-by-id
    description:
    timeout: 30
    events:
      - http:
          path: organization-invite-by-id/{id}
          method: get
          cors: ${self:custom.corsStatements}
          integration: lambda
          parameters:
            paths:
              id: true
          response:
            headers:
              Content-Type: "'application/json'"
          authorizer:
            type: CUSTOM
            authorizerId: ${cf:AUTH-SERVICE-${self:provider.stage}.apiGatewayAuthorizer}

  delete_invitation:
    handler: handler.delete_invitation
    name: ${self:provider.stage}-${self:service}-delete-org-invitation
    description:
    timeout: 30
    events:
      - http:
          path: delete-organization-invite/{id}
          method: delete
          cors: ${self:custom.corsStatements}
          integration: lambda
          parameters:
            paths:
              id: true
          response:
            headers:
              Content-Type: "'application/json'"
          authorizer:
            type: CUSTOM
            authorizerId: ${cf:AUTH-SERVICE-${self:provider.stage}.apiGatewayAuthorizer}

  get_org_invites:
    handler: handler.get_org_invites
    name: ${self:provider.stage}-${self:service}-get_org_invites
    description:
    timeout: 30
    events:
      - http:
          path: organization-invite
          method: get
          cors: ${self:custom.corsStatements}
          integration: lambda
          parameters:
            paths:
              id: true
          response:
            headers:
              Content-Type: "'application/json'"
          authorizer:
            type: CUSTOM
            authorizerId: ${cf:AUTH-SERVICE-${self:provider.stage}.apiGatewayAuthorizer}

  validate_org_invite:
    handler: handler.validate_org_invite
    name: ${self:provider.stage}-${self:service}-validate-org-invite
    description:
    timeout: 30
    events:
      - http:
          path: validate-org-invite/{invitation_hash}
          method: get
          cors: ${self:custom.corsStatements}
          integration: lambda
          parameters:
            paths:
              invitation_hash: true
          response:
            headers:
              Content-Type: "'application/json'"

  get_user_org_invites:
    handler: handler.get_user_org_invites
    name: ${self:provider.stage}-${self:service}-get-user-org-invites
    description:
    timeout: 30
    events:
      - http:
          path: user-organization-invites
          method: post
          cors: ${self:custom.corsStatements}
          integration: lambda
          response:
            headers:
              Content-Type: "'application/json'"
          authorizer:
            type: CUSTOM
            authorizerId: ${cf:AUTH-SERVICE-${self:provider.stage}.apiGatewayAuthorizer}

  accept_org_invite:
    handler: handler.accept_org_invite
    name: ${self:provider.stage}-${self:service}-accept-org-invite
    description:
    timeout: 30
    events:
      - http:
          path: accept-organization-invite
          method: post
          cors: ${self:custom.corsStatements}
          integration: lambda
          response:
            headers:
              Content-Type: "'application/json'"
          authorizer:
            type: CUSTOM
            authorizerId: ${cf:AUTH-SERVICE-${self:provider.stage}.apiGatewayAuthorizer}

  update_organization_invite:
    handler: handler.update_organization_invite
    name: ${self:provider.stage}-${self:service}-update-org-invite
    description:
    timeout: 30
    events:
      - http:
          path: organization-invite
          method: put
          cors: ${self:custom.corsStatements}
          integration: lambda
          response:
            headers:
              Content-Type: "'application/json'"
          authorizer:
            type: CUSTOM
            authorizerId: ${cf:AUTH-SERVICE-${self:provider.stage}.apiGatewayAuthorizer}

plugins:
  - serverless-python-requirements
  - serverless-pseudo-parameters
  - serverless-prune-plugin