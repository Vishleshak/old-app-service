import datetime
import hashlib
import json
import os

import boto3
from botocore.exceptions import ClientError

from models.json_converter import AlchemyEncoder
from models.organization import Organization
from models.organization_invitation import OrganizationInvitation
from models.organization_users import OrganizationUser
from utils.db_util import get_database_session, commit_database_session, close_database_session
from utils.event_util import get_body, get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def add_organization_invite(event, context):
    """This is method for sending invitation to the users for organization

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["organization_id", "email", "user_role_id", "accessible_modules",
                            "user_rights", "is_admin", "organization_name", "user_name"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        accessible_modules = None
        user_rights = None

        if event_body.get("accessible_modules") is not None:
            accessible_modules = event_body.get("accessible_modules")
            if len(accessible_modules) == 0:
                return {"status_code": 50001, "error": "Please provide accessible_modules, it should not be empty",
                        "error_field_name": "accessible_modules"}
            accessible_modules = '^'.join(accessible_modules)

        if event_body.get("user_rights") is not None:
            user_rights = event_body.get("user_rights")
            if len(user_rights) == 0:
                return {"status_code": 50001, "error": "Please provide user_rights, it should not be empty",
                        "error_field_name": "user_rights"}
            user_rights = '^'.join(user_rights)

        organization_invitation = OrganizationInvitation()
        organization_invitation.organization_id = event_body.get("organization_id")
        hash_data = hashlib.new('sha256')
        hash_data.update("{}{}".format(event_body.get("organization_id"), event_body.get("email")).encode())
        invitation_hash = hash_data.hexdigest()
        organization_invitation.invitation_hash = invitation_hash
        organization_invitation.email = event_body.get("email")
        organization_invitation.user_role_id = event_body.get("user_role_id")
        organization_invitation.accessible_modules = accessible_modules
        organization_invitation.user_rights = user_rights
        organization_invitation.is_admin = event_body.get("is_admin")
        session = get_database_session()
        session.add(organization_invitation)
        commit_database_session(session)
        send_email(event_body.get("email"), event_body.get("organization_name"), event_body.get("user_name"),
                   invitation_hash)
        return {"status_code": 201, "message": "Organization invitation sent successfully"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred in creating organization"}
    finally:
        if session is not None:
            close_database_session(session)


def get_org_invite_details(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        invitation_hash = path_parameter.get("invitation_hash")
        session = get_database_session()
        if invitation_hash is not None:
            result = session.query(OrganizationInvitation).filter(
                OrganizationInvitation.invitation_hash == invitation_hash,
                OrganizationInvitation.is_deleted == 0).first()
            if not result:
                return {"status_code": 404,
                        "message": "Organization invitation for id {} does not exist".format(invitation_hash)}
            return {"status_code": 200, "organization_invitation": json.dumps(result, cls=AlchemyEncoder)}
        return {"status_code": 404, "message": "Please pass invitation id"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting organization invitation"}
    finally:
        if session is not None:
            close_database_session(session)


def get_org_invite_details_by_id(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        invitation_hash = path_parameter.get("id")
        session = get_database_session()
        if invitation_hash is not None:
            result = session.query(OrganizationInvitation).filter(
                OrganizationInvitation.id == invitation_hash,
                OrganizationInvitation.is_deleted == 0).first()
            if not result:
                return {"status_code": 404,
                        "message": "Organization invitation for id {} does not exist".format(invitation_hash)}
            if result.accessible_modules is not None:
                result.accessible_modules = result.accessible_modules.split("^")
            if result.user_rights is not None:
                result.user_rights = result.user_rights.split("^")
            return {"status_code": 200, "organization_invitation": json.dumps(result, cls=AlchemyEncoder)}
        return {"status_code": 404, "message": "Please pass invitation id"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting organization invitation"}
    finally:
        if session is not None:
            close_database_session(session)


def delete_invitation(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        record_id = path_parameter.get("id")
        session = get_database_session()
        result = session.query(OrganizationInvitation).filter(OrganizationInvitation.id == record_id).first()
        if not result:
            return {"status_code": 404,
                    "message": "Organization invitation for id {} does not exist".format(record_id)}
        session.delete(result)
        session.commit()
        return {"status_code": 200, "message": "Organization invitation with id {} "
                                               "deleted successfully".format(record_id)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while deleting organization invitation"}
    finally:
        if session is not None:
            close_database_session(session)


def get_org_invites(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        record_id = path_parameter.get("id")
        session = get_database_session()
        result = session.query(OrganizationInvitation).filter(OrganizationInvitation.organization_id == record_id,
                                                              OrganizationInvitation.is_deleted == 0).all()

        if not result:
            return {"status_code": 404, "message": "Organization invitations with id "
                                                   " {} does not exist".format(record_id)}
        return {"status_code": 200, "organization_invitations": json.dumps(result, cls=AlchemyEncoder)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred getting users organization"}
    finally:
        if session is not None:
            close_database_session(session)


def get_user_org_invites(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["email"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}
        session = get_database_session()

        result = session.query(OrganizationInvitation.organization_id, OrganizationInvitation.email,
                               Organization.name, OrganizationInvitation.id).filter(
            OrganizationInvitation.email == event_body.get("email"),
            OrganizationInvitation.is_deleted == 0).join(
            Organization, OrganizationInvitation.organization_id == Organization.id).all()

        user_organizations = []
        for obj in result:
            data = {"organization_id": obj[0], "email": obj[1], "name": obj[2], "invite_id": obj[3]}
            user_organizations.append(data)

        if not result:
            return {"status_code": 200, "invitations": []}
        return {"status_code": 200, "invitations": user_organizations}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting organization invitation"}
    finally:
        if session is not None:
            close_database_session(session)


def send_email(recipient, organization_name, user_name, invitation_hash):
    # If necessary, replace us-west-2 with the AWS Region you're using for Amazon SES.
    AWS_REGION = "us-east-2"
    # The subject line for the email.
    SUBJECT = "KOSA :: Organization Invite"

    # The email body for recipients with non-HTML email clients.
    BODY_TEXT = ("KOSA :: Organization Invite\r\n"
                 "You have invited by {} to the organization. \r\n "
                 "To create account, please visit - https://{}.kosa.ai/#/signup?invite={}".format(
        user_name, organization_name, os.getenv("STAGE"), invitation_hash))

    # The HTML body of the email.
    BODY_HTML = """<html>
                    <head></head>
                    <body>
                    <h1>KOSA :: Organization Invite</h1>
                    <p>You have invited by {} to the organization - {} created on KOSA</p>
                    <p>To create account, please visit - <a href="https://{}.kosa.ai/#/signup?invite={}">
                    KOSA</a></p>
                    </body>
                    </html>""".format(user_name, organization_name, os.getenv("STAGE"), invitation_hash)

    # The character encoding for the email.
    CHARSET = "UTF-8"

    # Create a new SES resource and specify a region.
    client = boto3.client('ses', region_name=AWS_REGION)

    # Try to send the email.
    try:
        # Provide the contents of the email.
        response = client.send_email(
            Destination={
                'ToAddresses': [
                    recipient,
                ],
            },
            Message={
                'Body': {
                    'Html': {
                        'Charset': CHARSET,
                        'Data': BODY_HTML,
                    },
                    'Text': {
                        'Charset': CHARSET,
                        'Data': BODY_TEXT,
                    },
                },
                'Subject': {
                    'Charset': CHARSET,
                    'Data': SUBJECT,
                },
            },
            Source="team@kosa.ai"
        )
    # Display an error if something goes wrong.
    except ClientError as e:
        logging.error(e)
    else:
        logging.debug("Email sent! Message ID:"),
        logging.debug(response['MessageId'])


def validate_org_invite(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        invitation_hash = path_parameter.get("invitation_hash")
        session = get_database_session()
        result = session.query(OrganizationInvitation).filter(OrganizationInvitation.invitation_hash == invitation_hash,
                                                              OrganizationInvitation.is_deleted == 0).first()

        if not result:
            return {"status_code": 404, "message": "Organization invitations with id "
                                                   " {} does not exist".format(invitation_hash)}

        if abs(datetime.datetime.now() - result.created_at) > datetime.timedelta(hours=8):
            return {"status_code": 404, "message": "Organization invitation is expired", "is_valid": True}

        organization = session.query(Organization).filter(Organization.id == result.organization_id).first()
        if organization is None:
            return {"status_code": 404, "message": "Organization details does not exists", "is_valid": True}
        invite_details = {"email": result.email, "organization_name": organization.name,
                          "user_role": result.user_role_id, "organization_id": organization.id,
                          "is_admin": result.is_admin}
        return {"status_code": 200, "is_valid": True, "invite_details": invite_details}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred validating organization invitation"}
    finally:
        if session is not None:
            close_database_session(session)


def accept_org_invite(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["invitation_id", "user_id"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}
        session = get_database_session()

        result = session.query(OrganizationInvitation).filter(
            OrganizationInvitation.id == event_body.get("invitation_id"),
            OrganizationInvitation.is_deleted == 0).first()

        if not result:
            return {"status_code": 404, "message": "Organization invitation with id "
                                                   " {} does not exist".format(event_body.get("invitation_id"))}

        organization_user = OrganizationUser()
        organization_user.organization_id = result.organization_id
        organization_user.user_id = event_body.get("user_id")
        organization_user.user_role_id = result.user_role_id
        organization_user.user_rights = result.user_rights
        organization_user.accessible_modules = result.accessible_modules
        session.add(organization_user)
        commit_database_session(session)

        org_invitation = session.query(OrganizationInvitation).filter(OrganizationInvitation.id == result.id).first()
        if org_invitation:
            session.delete(org_invitation)
            session.commit()

        return {"status_code": 200, "invitation_accepted": True}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting organization invitation"}
    finally:
        if session is not None:
            close_database_session(session)


def update_organization_invite(event, context):
    """This is method for sending invitation to the users for organization

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["id", "organization_id", "user_role_id", "accessible_modules",
                            "user_rights", "is_admin"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        data_to_update = {}
        if event_body.get("accessible_modules") is not None:
            accessible_modules = event_body.get("accessible_modules")
            if len(accessible_modules) == 0:
                return {"status_code": 50001, "error": "Please provide accessible_modules, it should not be empty",
                        "error_field_name": "accessible_modules"}
            accessible_modules = '^'.join(accessible_modules)
            data_to_update["accessible_modules"] = accessible_modules
        if event_body.get("user_rights") is not None:
            user_rights = event_body.get("user_rights")
            if len(user_rights) == 0:
                return {"status_code": 50001, "error": "Please provide user_rights, it should not be empty",
                        "error_field_name": "user_rights"}
            user_rights = '^'.join(user_rights)
            data_to_update["user_rights"] = user_rights
        data_to_update["is_admin"] = event_body.get("is_admin")
        data_to_update["user_role_id"] = event_body.get("user_role_id")

        session = get_database_session()
        if len(data_to_update) > 0:
            session.query(OrganizationInvitation).filter(OrganizationInvitation.id == event_body.get("id")).update(
                data_to_update)
            commit_database_session(session)
            return {"status_code": 200, "updated": True}
        return {"status_code": 200, "updated": False}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred in updating organization invitation"}
    finally:
        if session is not None:
            close_database_session(session)
