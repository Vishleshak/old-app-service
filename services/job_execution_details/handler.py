import hashlib
import json

import boto3

from models.after_mitigation_job_execution_status import AfterMitigationJobExecutionStatus
from models.after_mitigation_job_output import AfterMitigationJobOutput
from models.audit_tool_job import AuditToolJobDetail
from models.audit_tool_job_execution_status import AuditToolJobExecutionStatus
from models.audit_tool_job_output import AuditToolJobOutput
from models.before_mitigation_job_execution_status import BeforeMitigationJobExecutionStatus
from models.before_mitigation_job_output import BeforeMitigationJobOutput
from models.data_editor_job import DataEditorJobDetails
from models.dataset_target_variable import DatasetTargetVariableDetails
from models.json_converter import AlchemyEncoder
from utils.db_util import get_database_session, close_database_session
from utils.event_util import get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def list_jobs(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        job_type = path_parameter.get("job_type")
        page_number = path_parameter.get("page_number")
        project_id = path_parameter.get("project_id")
        session = get_database_session()
        page_number = int(page_number) - 1
        if job_type == "data_editor":
            result = session.query(DataEditorJobDetails).filter(
                DataEditorJobDetails.project_id == project_id).order_by(
                DataEditorJobDetails.created_at.desc()).limit(10).offset(page_number).all()
            if result is None:
                return {"status_code": 200, "jobs": []}
            final_result = []
            for obj in result:
                res = {"id": obj.id, "file_path": obj.file_path, "job_type": obj.job_type,
                       "created_at": obj.created_at.isoformat(), "dataset_name": obj.file_path.split("/")[-1]}
                final_result.append(res)
            return {"status_code": 200, "jobs": final_result}
        if job_type == "audit_tool":
            result = session.query(AuditToolJobDetail).filter(
                AuditToolJobDetail.project_id == project_id).order_by(
                AuditToolJobDetail.created_at.desc()).limit(10).offset(page_number).all()
            if result is None:
                return {"status_code": 200, "jobs": []}
            final_result = []
            for obj in result:
                res = {"id": obj.id, "file_path": obj.file_path, "created_at": obj.created_at.isoformat(),
                       "dataset_name": obj.file_path.split("/")[-1]}
                final_result.append(res)
            return {"status_code": 200, "jobs": final_result}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": str(ex)}
    finally:
        if session is not None:
            close_database_session(session)


def get_job_details(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        job_type = path_parameter.get("job_type")
        job_id = path_parameter.get("job_id")
        session = get_database_session()
        job_parameters = get_job_parameters(job_id, job_type)
        job_execution_status = None
        job_output = None
        target_variables = []
        favorable_classes = None
        priviledged_classes = None
        if job_type == "data_editor":
            job_execution_status = get_job_execution_status(job_id, job_parameters.job_type)
            job_output = get_job_output(job_id, job_parameters.job_type)
            job_output = json.dumps(job_output, cls=AlchemyEncoder)

            # Read target variables
            dataset_target_variable_details = session.query(DatasetTargetVariableDetails).filter(
                DatasetTargetVariableDetails.project_id == job_parameters.project_id,
                DatasetTargetVariableDetails.dataset_path == job_parameters.file_path).all()

            if dataset_target_variable_details is not None:
                for obj in dataset_target_variable_details:
                    result = {"column_name": obj.target_variable,
                              "column_values": read_column_values(job_parameters.file_path, job_parameters.project_id,
                                                                  obj.target_variable)}
                    target_variables.append(result)
            # Read favorable classes
            favorable_classes = read_column_values(job_parameters.file_path, job_parameters.project_id,
                                                   job_parameters.target_variable)

            # Read priviledged classes
            priviledged_classes = read_column_values(job_parameters.file_path, job_parameters.project_id,
                                                     job_parameters.protected_attribute_names)

            if job_parameters.job_type == "after_mitigation" and job_parameters.before_mitigation_job_id is not None:
                before_mitigation_job_output = session.query(BeforeMitigationJobOutput).filter(
                    BeforeMitigationJobOutput.job_id
                    == job_parameters.before_mitigation_job_id).first()
                if before_mitigation_job_output is not None:
                    output = {"job_parameters": json.dumps(job_parameters, cls=AlchemyEncoder),
                              "job_execution_status": json.dumps(job_execution_status, cls=AlchemyEncoder),
                              "job_output": json.dumps(job_output, cls=AlchemyEncoder),
                              "before_mitigation_job_output": json.dumps(before_mitigation_job_output,
                                                                         cls=AlchemyEncoder),
                              "target_variables": target_variables,
                              "favorable_classes": favorable_classes,
                              "priviledged_classes": priviledged_classes}
                    return {"status_code": 200, "job_details": output}
        else:
            job_execution_status = get_job_execution_status(job_id, job_type)
            job_output = get_job_output(job_id, job_type)
        output = {"job_parameters": json.dumps(job_parameters, cls=AlchemyEncoder),
                  "job_execution_status": json.dumps(job_execution_status, cls=AlchemyEncoder),
                  "job_output": json.dumps(job_output, cls=AlchemyEncoder),
                  "target_variables": target_variables,
                  "favorable_classes": favorable_classes,
                  "priviledged_classes": priviledged_classes}
        return {"status_code": 200, "job_details": output}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": str(ex)}
    finally:
        if session is not None:
            close_database_session(session)


def read_column_values(file_name, project_id, variable_name):
    try:
        s3 = boto3.resource('s3')
        hash_data = hashlib.new('sha256')
        hash_data.update(file_name.encode())
        hashed_file_name = hash_data.hexdigest()
        if " " in variable_name:
            variable_name = variable_name.replace(" ", "_")
        object_key = '{}/{}/column_values/{}.json'.format(project_id, hashed_file_name,
                                                          variable_name)
        obj = s3.Object("kosa-metadata", object_key)
        return json.load(obj.get()['Body'])
    except Exception as ex:
        logging.error(ex)
    return None


def get_job_parameters(job_id, job_type):
    session = None
    try:
        session = get_database_session()
        if job_type == "data_editor":
            result = session.query(DataEditorJobDetails).filter(DataEditorJobDetails.id == job_id).first()
            return result
        if job_type == "audit_tool":
            result = session.query(AuditToolJobDetail).filter(AuditToolJobDetail.id == job_id).first()
            return result
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": str(ex)}
    finally:
        if session is not None:
            close_database_session(session)


def get_job_execution_status(job_id, job_type):
    session = None
    try:
        session = get_database_session()
        if job_type == "before_mitigation":
            result = session.query(BeforeMitigationJobExecutionStatus).filter(
                BeforeMitigationJobExecutionStatus.job_id == job_id).order_by(
                BeforeMitigationJobExecutionStatus.created_at.desc()).all()
            return result
        if job_type == "after_mitigation":
            result = session.query(AfterMitigationJobExecutionStatus).filter(
                AfterMitigationJobExecutionStatus.job_id == job_id).order_by(
                AfterMitigationJobExecutionStatus.created_at.desc()).all()
            return result
        if job_type == "audit_tool":
            result = session.query(AuditToolJobExecutionStatus).filter(
                AuditToolJobExecutionStatus.job_id == job_id).order_by(
                AuditToolJobExecutionStatus.created_at.desc()).all()
            return result
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": str(ex)}
    finally:
        if session is not None:
            close_database_session(session)


def get_job_output(job_id, job_type):
    session = None
    try:
        session = get_database_session()
        if job_type == "before_mitigation":
            result = session.query(BeforeMitigationJobOutput).filter(BeforeMitigationJobOutput.job_id
                                                                     == job_id).first()
            return result
        if job_type == "after_mitigation":
            result = session.query(AfterMitigationJobOutput).filter(AfterMitigationJobOutput.job_id
                                                                    == job_id).first()
            return result
        if job_type == "audit_tool":
            result = session.query(AuditToolJobOutput).filter(AuditToolJobOutput.job_id == job_id).first()
            return result
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": str(ex)}
    finally:
        if session is not None:
            close_database_session(session)
