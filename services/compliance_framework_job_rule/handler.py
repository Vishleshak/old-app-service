import json

import boto3
from botocore.client import Config

from models.compliance_framework_rule import ComplianceFrameworkRule
from models.compliance_job import ComplianceJob
from models.compliance_job_rule import ComplianceJobRule
from models.compliance_job_rule_activity import ComplianceJobRuleActivity
from models.json_converter import AlchemyEncoder
from utils.db_util import get_database_session, commit_database_session, close_database_session
from utils.event_util import get_body, get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def get_compliance_job_rule(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        project_id = path_parameter.get("project_id")
        if project_id is None:
            return {"status_code": 500, "message": "Field project_id is mandatory"}

        rule_id = path_parameter.get("rule_id")
        if rule_id is None:
            return {"status_code": 500, "message": "Field rule_id is mandatory"}

        session = get_database_session()

        compliance_job_rules = session.query(
            ComplianceFrameworkRule.id,
            ComplianceFrameworkRule.title,
            ComplianceJobRule.compliance_rule_id,
            ComplianceJobRule.status,
            ComplianceJobRule.assigned_to_user,
            ComplianceJobRule.id,
            ComplianceFrameworkRule.section_name,
            ComplianceFrameworkRule.description,
            ComplianceFrameworkRule.audit_procedure_details).join(ComplianceJobRule,
                                                                  ComplianceJobRule.compliance_rule_id == ComplianceFrameworkRule.id,
                                                                  isouter=True).filter(
            ComplianceFrameworkRule.id == rule_id).all()
        rules = []
        for compliance_job_rule in compliance_job_rules:
            details = {}
            details["id"] = compliance_job_rule[0]
            details["title"] = compliance_job_rule[1]
            details["compliance_rule_id"] = compliance_job_rule[2]
            details["status"] = compliance_job_rule[3]
            details["assigned_to_user"] = compliance_job_rule[4]
            details["compliance_job_rule_id"] = compliance_job_rule[5]
            details["section_name"] = compliance_job_rule[6]
            details["description"] = compliance_job_rule[7]
            details["audit_procedure_details"] = compliance_job_rule[8]
            rules.append(details)
        return {"status_code": 200, "compliance_job_rule": json.dumps(rules, cls=AlchemyEncoder)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while adding compliance framework"}
    finally:
        if session is not None:
            close_database_session(session)


def update_compliance_job_rule(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        id = event_body.get("id")
        project_id = event_body.get("project_id")
        if project_id is None:
            return {"status_code": 500, "message": "Field project_id is mandatory"}

        session = get_database_session()
        compliance_job = session.query(ComplianceJob).filter(ComplianceJob.project_id == project_id).first()
        if not compliance_job:
            return {"status_code": 500, "message": "Compliance job is not present for this project"}

        compliance_job_id = compliance_job.id
        compliance_rule_id = event_body.get("compliance_rule_id")
        if compliance_rule_id is None:
            return {"status_code": 500, "message": "Field compliance_rule_id is mandatory"}

        status = event_body.get("status")
        assigned_to_user = event_body.get("assigned_to_user")
        if status is None and assigned_to_user is None:
            return {"status_code": 500, "message": "Either of field status or assigned_to_user is mandatory"}

        compliance_job_rule = session.query(ComplianceJobRule).filter(
            ComplianceJobRule.compliance_rule_id == compliance_rule_id,
            ComplianceJobRule.compliance_job_id == compliance_job_id).first()
        if not compliance_job_rule:
            compliance_job_rule = ComplianceJobRule()
            compliance_job_rule.compliance_job_id = compliance_job_id
            compliance_job_rule.compliance_rule_id = compliance_rule_id
            if status is not None:
                compliance_job_rule.status = status
            if assigned_to_user is not None:
                compliance_job_rule.assigned_to_user = assigned_to_user
            session.add(compliance_job_rule)
            commit_database_session(session)
            result = session.query(ComplianceJobRule).filter(ComplianceJobRule.id == compliance_job_rule.id).first()
            compliance_job_rule_activity = ComplianceJobRuleActivity()
            compliance_job_rule_activity.compliance_job_rule_id = compliance_job_rule.id
            compliance_job_rule_activity.activity = "1"
            session.add(compliance_job_rule_activity)
            commit_database_session(session)
            return {"status_code": 200, "compliance_job_rule": json.dumps(result, cls=AlchemyEncoder)}
        else:
            data_to_update = {}
            if status is not None:
                data_to_update["status"] = event_body.get("status")
            if assigned_to_user is not None:
                data_to_update["assigned_to_user"] = event_body.get("assigned_to_user")
            session.query(ComplianceJobRule).filter(ComplianceJobRule.id == compliance_job_rule.id).update(
                data_to_update)
            commit_database_session(session)
            compliance_job_rule_activity = ComplianceJobRuleActivity()
            compliance_job_rule_activity.compliance_job_rule_id = compliance_job_rule.id
            compliance_job_rule_activity.activity = "1"
            session.add(compliance_job_rule_activity)
            commit_database_session(session)
            result = session.query(ComplianceJobRule).filter(ComplianceJobRule.id == compliance_job_rule.id).first()
            return {"status_code": 200, "compliance_job_rule": json.dumps(result, cls=AlchemyEncoder)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while adding compliance framework"}
    finally:
        if session is not None:
            close_database_session(session)


def get_document_presigned_url(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        user_id = event_body.get("user_id")
        project_id = event_body.get("project_id")
        compliance_rule_id = event_body.get("compliance_rule_id")
        file_name = event_body.get("file_name")
        s3 = None
        if user_id is None:
            return {"status_code": 503, "data": {"message": "Please provide user_id"}}
        if project_id is None:
            return {"status_code": 503, "data": {"message": "Please provide project_id"}}
        if compliance_rule_id is None:
            return {"status_code": 503, "data": {"message": "Please provide compliance_rule_id"}}
        if file_name is None:
            return {"status_code": 503, "data": {"message": "Please provide file_name"}}

        session = get_database_session()
        compliance_job = session.query(ComplianceJob).filter(ComplianceJob.project_id == project_id).first()
        if not compliance_job:
            return {"status_code": 500, "message": "Compliance job is not present for this project"}

        logging.info("Creating presined url against kosa default storage")
        bucket_name = os.environ['DEFAULT_STORAGE_BUCKET']
        s3 = boto3.client('s3', region_name='us-east-2', aws_access_key_id=os.environ['S3_ACCESS_KEY'],
                          aws_secret_access_key=os.environ['S3_SECRET_KEY'], config=Config(signature_version='s3v4'))
        logging.info("Creating presined url in user storage")
        object_key = "{USER_ID}/{PROJECT_ID}/compliance_job_documents/{COMPLIANCE_JOB_RULE_ID}/{FILE_NAME}".format(
            USER_ID=user_id, PROJECT_ID=project_id, COMPLIANCE_JOB_RULE_ID=compliance_rule_id, FILE_NAME=file_name)
        url = s3.generate_presigned_url(ClientMethod='put_object', Params={'Bucket': bucket_name, 'Key': object_key},
                                        ExpiresIn=3600)
        return {"url": url}
    except Exception as ex:
        logging.error(ex)
    finally:
        if session is not None:
            close_database_session(session)


def get_compliance_job_documents(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        user_id = event_body.get("user_id")
        project_id = event_body.get("project_id")
        compliance_rule_id = event_body.get("compliance_rule_id")
        s3 = None
        if user_id is None:
            return {"status_code": 503, "data": {"message": "Please provide user_id"}}
        if project_id is None:
            return {"status_code": 503, "data": {"message": "Please provide project_id"}}
        if compliance_rule_id is None:
            return {"status_code": 503, "data": {"message": "Please provide compliance_rule_id"}}

        sesssion = boto3.Session(os.environ['S3_ACCESS_KEY'], os.environ['S3_SECRET_KEY'])
        bucket_name = os.environ['DEFAULT_STORAGE_BUCKET']
        bucket_key = "{USER_ID}/{PROJECT_ID}/compliance_job_documents/{COMPLIANCE_JOB_RULE_ID}".format(USER_ID=user_id,
                                                                                                       PROJECT_ID=project_id,
                                                                                                       COMPLIANCE_JOB_RULE_ID=compliance_rule_id)

        s3 = sesssion.resource("s3")
        bucket = s3.Bucket(bucket_name)
        result = {}
        dataset_list = []
        for obj in bucket.objects.filter(Prefix=bucket_key):
            data_details = {}
            data_details["bucket_name"] = obj.bucket_name
            data_details["key"] = obj.key.split("/")[-1]
            data_details["s3_url"] = "s3://{}/{}".format(obj.bucket_name, obj.key)
            dataset_list.append(data_details)
        result["datasets"] = dataset_list
        return {"status_code": 200, "result": result}
    except Exception as ex:
        logging.error(ex)


def get_compliance_job_rule_activity(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        compliance_job_rule_id = path_parameter.get("compliance_job_rule_id")
        if compliance_job_rule_id is None:
            return {"status_code": 503, "data": {"message": "Please provide compliance_job_rule_id"}}
        session = get_database_session()
        compliance_job_rule_activity = session.query(ComplianceJobRuleActivity).filter(
            ComplianceJobRuleActivity.compliance_job_rule_id == compliance_job_rule_id).order_by(
            ComplianceJobRuleActivity.activity_done_on.desc()).first()
        return {"status_code": 200, "activity_done_on": compliance_job_rule_activity.activity_done_on}
    except Exception as ex:
        logging.error(ex)
    finally:
        if session is not None:
            close_database_session(session)
