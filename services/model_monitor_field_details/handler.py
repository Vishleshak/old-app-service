import json
import secrets

from models.json_converter import AlchemyEncoder
from models.model_monitor_field_details import ModelMonitorFieldDetails
from utils.db_util import get_database_session, commit_database_session, close_database_session
from utils.event_util import get_body, get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def add_model_monitor_field(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["project_id", "field_name",  "threshold_value", "is_sensitive_attribute"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        session = get_database_session()
        # Add model monitor field details
        model_monitor_field_details = ModelMonitorFieldDetails()
        model_monitor_field_details.project_id = event_body.get("project_id")
        model_monitor_field_details.field_name = event_body.get("field_name")
        model_monitor_field_details.is_sensitive_attribute = event_body.get("is_sensitive_attribute")
        model_monitor_field_details.threshold_value = event_body.get("is_sensitive_attribute")
        session.add(model_monitor_field_details)
        commit_database_session(session)
        return {"status_code": 201, "data": json.dumps(model_monitor_field_details, cls=AlchemyEncoder)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "data": {"error": "Error occurred in adding model monitor field details"}}
    finally:
        if session is not None:
            close_database_session(session)


def get_model_monitor_fields(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        project_id = path_parameter.get("project_id")
        session = get_database_session()
        if project_id is not None:
            results = session.query(ModelMonitorFieldDetails).filter(ModelMonitorFieldDetails.project_id == project_id).all()
            if not results:
                return {"status_code": 200, "data": None}
            return {"status_code": 200, "data": json.dumps(results, cls=AlchemyEncoder)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "data": {"error": "Error occurred in getting model monitor field details"}}
    finally:
        if session is not None:
            close_database_session(session)


def update_model_monitor_field(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        # Validate Data
        mandatory_fields = ["id", "project_id", "field_name",  "threshold_value", "is_sensitive_attribute"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        # Set data to update
        data_to_update = {}
        fields_to_update = ["project_id", "field_name", "threshold_value", "is_sensitive_attribute"]
        for field in fields_to_update:
            data_to_update[field] = event_body.get(field)
        session = get_database_session()
        session.query(ModelMonitorFieldDetails).filter(ModelMonitorFieldDetails.id == event_body.get("id")).update(data_to_update)
        commit_database_session(session)
        return {"status_code": 200, "updated": True}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "updated": False, "error": "Error occurred in updating model monitor field details"}
    finally:
        if session is not None:
            close_database_session(session)
