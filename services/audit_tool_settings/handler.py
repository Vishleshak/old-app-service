import json

from models.audit_tool_global_settings import AuditToolGlobalSetting
from models.audit_tool_user_settings import AuditToolUserSetting
from models.json_converter import AlchemyEncoder
from models.project_detail import ProjectDetail
from utils.db_util import get_database_session, commit_database_session, close_database_session
from utils.event_util import get_path_parameters, get_body
from utils.log_util import get_logger

logging = get_logger(__name__)


def get_user_settings(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        user_id = path_parameter.get("user_id")
        project_id = path_parameter.get("project_id")

        mandatory_fields = ["user_id", "project_id"]
        for mandatory_field in mandatory_fields:
            if path_parameter.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        session = get_database_session()

        audit_tool_user_settings = session.query(AuditToolUserSetting).filter(
            AuditToolUserSetting.user_id == user_id, AuditToolUserSetting.project_id == project_id).first()
        if audit_tool_user_settings is not None:
            result = []
            for key in audit_tool_user_settings.metrics.keys():
                metric_value = {"metric": key, "value": audit_tool_user_settings.metrics.get(key)}
                result.append(metric_value)
            audit_tool_user_settings.metrics = result
            return {"user_settings": json.dumps(audit_tool_user_settings, cls=AlchemyEncoder), "global_settings": None}
        else:
            project_details = session.query(ProjectDetail).filter(ProjectDetail.id == project_id).first()
            if not project_details:
                return {"status_code": 404, "message": "Project with ID {} does not exist".format(project_id)}

            audit_tool_global_settings = session.query(AuditToolGlobalSetting).filter(
                AuditToolGlobalSetting.use_case_id == project_details.use_case).first()
            if not audit_tool_global_settings:
                return {"status_code": 404, "message": "Audit tool user and global settings are missing"}

            result = []
            for key in audit_tool_global_settings.metrics.keys():
                metric_value = {"metric": key, "value": audit_tool_global_settings.metrics.get(key)}
                result.append(metric_value)
            audit_tool_global_settings.metrics = result
            return {"global_settings": json.dumps(audit_tool_global_settings, cls=AlchemyEncoder),
                    "user_settings": None}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting audit tool user settings"}
    finally:
        if session is not None:
            close_database_session(session)


def add_user_settings(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["metrics", "user_id", "project_id"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        metrics = {}
        for value in event_body.get("metrics"):
            metrics[value.get("metric")] = value.get("value")

        session = get_database_session()
        audit_tool_user_setting = AuditToolUserSetting()
        audit_tool_user_setting.metrics = metrics
        audit_tool_user_setting.user_id = event_body.get("user_id")
        audit_tool_user_setting.project_id = event_body.get("project_id")
        session = get_database_session()
        session.add(audit_tool_user_setting)
        commit_database_session(session)
        return {"status_code": 201}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred in creating audit tool user setting"}
    finally:
        if session is not None:
            close_database_session(session)


def update_user_settings(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["metrics", "user_id", "project_id"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        metrics = {}
        for value in event_body.get("metrics"):
            metrics[value.get("metric")] = value.get("value")

        session = get_database_session()
        data_to_update = {"metrics": metrics}
        audit_tool_user_settings = session.query(AuditToolUserSetting).filter(
            AuditToolUserSetting.user_id == event_body.get("user_id"),
            AuditToolUserSetting.project_id == event_body.get("project_id")).first()

        if not audit_tool_user_settings:
            return {"status_code": 404, "error": "Audit tool user settings did not found"}
        session.query(AuditToolUserSetting).filter(
            AuditToolUserSetting.user_id == event_body.get("user_id"),
            AuditToolUserSetting.project_id == event_body.get("project_id")).update(data_to_update)
        commit_database_session(session)
        return {"status_code": 201, "message": "Audit tool user settings updated successfully"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while updating audit tool user settings"}
    finally:
        if session is not None:
            close_database_session(session)
