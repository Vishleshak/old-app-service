import json

from models.json_converter import AlchemyEncoder
from models.model_dataset_details import ModelDatasetDetails
from models.organization import Organization
from models.prediction_dataset_details import PredictionDatasetDetails
from models.project_detail import ProjectDetail
from models.project_users_invitation import ProjectUsersInvitation
from models.training_dataset_details import TrainingDatasetDetails
from models.user import User
from utils.db_util import get_database_session, commit_database_session, close_database_session
from utils.event_util import get_body, get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def add_project_detail(event, context):
    """This is method for adding Project Detail

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        project_detail = ProjectDetail()
        mandatory_fields = ["project_name", "description", "user_id", "region", "country", "industry", "use_case",
                            "organization_id"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        regions = event_body.get("region")
        countries = event_body.get("country")
        industries = event_body.get("industry")
        use_cases = event_body.get("use_case")

        if len(regions) == 0:
            return {"status_code": 50001, "error": "Please provide region, it should not be empty",
                    "error_field_name": "region"}
        if len(countries) == 0:
            return {"status_code": 50001, "error": "Please provide country, it should not be empty",
                    "error_field_name": "country"}
        if len(industries) == 0:
            return {"status_code": 50001, "error": "Please provide industry, it should not be empty",
                    "error_field_name": "industry"}
        if len(use_cases) == 0:
            return {"status_code": 50001, "error": "Please provide use_case, it should not be empty",
                    "error_field_name": "use_case"}

        region = '^'.join(regions)
        country = '^'.join(countries)
        industry = '^'.join(industries)
        use_case = '^'.join(use_cases)

        project_detail.project_name = event_body.get("project_name")
        project_detail.description = event_body.get("description")
        project_detail.user_id = event_body.get("user_id")
        project_detail.use_kosa_storage = event_body.get("use_kosa_storage")
        project_detail.organization_id = event_body.get("organization_id")
        project_detail.region = region
        project_detail.country = country
        project_detail.industry = industry
        project_detail.use_case = use_case

        session = get_database_session()
        result = session.query(ProjectDetail).filter(ProjectDetail.project_name == event_body.get("project_name"),
                                                     ProjectDetail.user_id == event_body.get("user_id")).first()
        if result is not None:
            return {"status_code": 50003, "error": "This project name is already taken"}
        else:
            session.add(project_detail)
            commit_database_session(session)
            return {"status_code": 201, "project_detail": json.dumps(project_detail, cls=AlchemyEncoder)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred in creating project detail"}
    finally:
        if session is not None:
            close_database_session(session)


def update_project_detail(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        path_parameter = get_path_parameters(event)
        mandatory_fields = ["project_name", "description", "user_id", "region", "country", "industry", "use_case"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        if path_parameter.get("id") is not None:
            session = get_database_session()
            data_to_update = {}
            if event_body.get("project_name") is not None:
                data_to_update["project_name"] = event_body.get("project_name")
            if event_body.get("description") is not None:
                data_to_update["description"] = event_body.get("description")
            if event_body.get("region") is not None:
                regions = event_body.get("region")
                if len(regions) == 0:
                    return {"status_code": 50001, "error": "Please provide region, it should not be empty",
                            "error_field_name": "region"}
                region = '^'.join(regions)
                data_to_update["region"] = region
            if event_body.get("country") is not None:
                countries = event_body.get("country")
                if len(countries) == 0:
                    return {"status_code": 50001, "error": "Please provide country, it should not be empty",
                            "error_field_name": "country"}
                country = '^'.join(countries)
                data_to_update["country"] = country
            if event_body.get("industry") is not None:
                industries = event_body.get("industry")
                if len(industries) == 0:
                    return {"status_code": 50001, "error": "Please provide industry, it should not be empty",
                            "error_field_name": "industry"}
                industry = '^'.join(industries)
                data_to_update["industry"] = industry
            if event_body.get("use_case") is not None:
                use_cases = event_body.get("use_case")
                if len(use_cases) == 0:
                    return {"status_code": 50001, "error": "Please provide use_case, it should not be empty",
                            "error_field_name": "use_case"}
                use_case = '^'.join(use_cases)
                data_to_update["use_case"] = use_case
            result = session.query(ProjectDetail).filter(ProjectDetail.project_name == event_body.get("project_name"),
                                                         ProjectDetail.user_id == event_body.get("user_id"),
                                                         ProjectDetail.id != path_parameter.get("id")).first()
            if result is not None:
                return {"status_code": 50003, "error": "This project name is already taken"}
            session.query(ProjectDetail).filter(ProjectDetail.id == path_parameter.get("id")).update(data_to_update)
            commit_database_session(session)
            result = session.query(ProjectDetail).filter(ProjectDetail.id == path_parameter.get("id")).first()
            if result.region is not None:
                result.region = result.region.split("^")
            if result.country is not None:
                result.country = result.country.split("^")
            if result.use_case is not None:
                result.use_case = result.use_case.split("^")
            if result.industry is not None:
                result.industry = result.industry.split("^")
            return json.dumps(result, cls=AlchemyEncoder)
        return {"status_code": 500, "error": "Please provide correct project id"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while updating Project detail"}
    finally:
        if session is not None:
            close_database_session(session)


def get_project_detail(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        id = path_parameter.get("id")
        session = get_database_session()
        if id is not None:
            result = session.query(ProjectDetail).filter(ProjectDetail.id == id).first()
            if not result:
                return {"status_code": 404, "message": "Project Detail {} does not exist".format(id)}
            if result.region is not None:
                result.region = result.region.split("^")
            if result.country is not None:
                result.country = result.country.split("^")
            if result.use_case is not None:
                result.use_case = result.use_case.split("^")
            if result.industry is not None:
                result.industry = result.industry.split("^")
            return json.dumps(result, cls=AlchemyEncoder)
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting project detail"}
    finally:
        if session is not None:
            close_database_session(session)


def get_all_projects(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        user_id = path_parameter.get("user_id")
        session = get_database_session()
        if user_id is not None:
            user = session.query(User).filter(User.auth_id == user_id).first()
            if not user:
                return {"status_code": 404, "message": "User for organization :  {} does not exist".format(user_id)}
            result = session.query(ProjectDetail.id, ProjectDetail.project_name, ProjectDetail.description).filter(
                ProjectDetail.user_id == user_id).all()
            projects = []
            if result is not None:
                for obj in result:
                    data = {"id": obj[0], "project_name": obj[1], "description": obj[2]}
                    projects.append(data)
            pending_invitations = session.query(ProjectUsersInvitation).filter(
                ProjectUsersInvitation.user_email == user.email, ProjectUsersInvitation.accepted == False,
                ProjectUsersInvitation.is_deleted == False).all()
            project_invitations = False
            if pending_invitations is not None:
                project_invitations = True
            if not result:
                return {"status_code": 200, "projects": [], "pending_invitations": project_invitations}
            else:
                return {"status_code": 200, "projects": projects, "pending_invitations": project_invitations}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting project detail"}
    finally:
        if session is not None:
            close_database_session(session)


def get_all_projects_by_organization(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        organization_id = path_parameter.get("organization_id")
        session = get_database_session()
        if organization_id is not None:
            organization = session.query(Organization).filter(Organization.id == organization_id).first()
            if not organization:
                return {"status_code": 404, "message": "Organization :  {} does not exist".format(organization_id)}
            result = session.query(ProjectDetail.id, ProjectDetail.project_name, ProjectDetail.description).filter(
                ProjectDetail.organization_id == organization_id, ProjectDetail.is_deleted == 0,
                ProjectDetail.is_active == 1).all()
            projects = []
            if result is not None:
                for obj in result:
                    data = {"id": obj[0], "project_name": obj[1], "description": obj[2]}
                    projects.append(data)
            return {"status_code": 200, "projects": projects}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting project detail"}
    finally:
        if session is not None:
            close_database_session(session)


def get_active_projects(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        user_id = path_parameter.get("user_id")
        session = get_database_session()
        if user_id is not None:
            user = session.query(User).filter(User.auth_id == user_id).first()
            if not user:
                return {"status_code": 404, "message": "User for organization :  {} does not exist".format(user_id)}
            result = session.query(ProjectDetail.id, ProjectDetail.project_name, ProjectDetail.description).filter(
                ProjectDetail.user_id == user_id).all()
            invited_projects = session.query(ProjectDetail.id, ProjectDetail.project_name, ProjectDetail.description,
                                             ProjectUsersInvitation.project_id).filter(
                ProjectUsersInvitation.user_email == user.email, ProjectUsersInvitation.accepted == True,
                ProjectUsersInvitation.is_deleted == False).join(ProjectDetail,
                                                                 ProjectUsersInvitation.project_id == ProjectDetail.id).distinct()
            project_invites = []
            if result is not None:
                for obj in result:
                    data = {}
                    data["id"] = obj[0]
                    data["project_name"] = obj[1]
                    data["description"] = obj[2]
                    project_invites.append(data)
            if invited_projects is not None:
                for obj in invited_projects:
                    data = {}
                    data["id"] = obj[0]
                    data["project_name"] = obj[1]
                    data["description"] = obj[2]
                    project_invites.append(data)
            return {"status_code": 200, "projects": project_invites}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting project detail"}
    finally:
        if session is not None:
            close_database_session(session)


def delete_project_detail(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        id = path_parameter.get("id")
        session = get_database_session()
        project = session.query(ProjectDetail).filter(ProjectDetail.id == id).first()
        if not project:
            return {"status_code": 404, "message": "Project Detail {} does not exist".format(id)}
        session.delete(project)

        prediction_dataset_details = session.query(PredictionDatasetDetails).filter(
            PredictionDatasetDetails.project_id == id).first()
        if prediction_dataset_details is not None:
            session.delete(prediction_dataset_details)

        model_dataset_details = session.query(ModelDatasetDetails).filter(ModelDatasetDetails.project_id == id).first()
        if model_dataset_details is not None:
            session.delete(model_dataset_details)

        training_dataset_details = session.query(TrainingDatasetDetails).filter(
            TrainingDatasetDetails.project_id == id).first()
        if training_dataset_details is not None:
            session.delete(training_dataset_details)
        commit_database_session(session)
        return {"message": "Project Detail{} deleted successfully".format(id)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while deleting project detail"}
    finally:
        if session is not None:
            close_database_session(session)


def get_project_users(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        project_id = path_parameter.get("project_id")
        session = get_database_session()
        users = session.query(User.id, User.username, ProjectUsersInvitation.user_email).filter(
            ProjectUsersInvitation.project_id == project_id, ProjectUsersInvitation.accepted == True,
            ProjectUsersInvitation.is_deleted == False).join(User, ProjectUsersInvitation.user_email == User.email,
                                                             isouter=True).distinct()
        project_users = []
        if users is not None:
            for obj in users:
                data = {"id": obj[0], "name": obj[1]}
                project_users.append(data)
        return {"status_code": 200, "project_users": project_users}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting project users"}
    finally:
        if session is not None:
            close_database_session(session)
