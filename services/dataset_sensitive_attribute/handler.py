import hashlib
import json

import boto3

from models.dataset_sensitive_attribute import DatasetSensitiveAttributeDetails
from models.project_dataset_details import ProjectDatasetDetails
from utils.db_util import get_database_session, commit_database_session, close_database_session
from utils.event_util import get_body
from utils.log_util import get_logger
logging = get_logger(__name__)


def add_sensitive_attribute(event, context):
    """This is method for adding User

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["dataset_path", "project_id", "sensitive_attributes", "dataset_type"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        session = get_database_session()
        sensitive_attributes = event_body.get("sensitive_attributes")
        for sensitive_attribute in sensitive_attributes:
            dataset_target_attribute_details = DatasetSensitiveAttributeDetails()
            dataset_target_attribute_details.dataset_path = event_body.get("dataset_path")
            dataset_target_attribute_details.project_id = event_body.get("project_id")
            dataset_target_attribute_details.sensitive_attribute = sensitive_attribute
            # dataset_target_attribute_details.sensitive_attribute_value = event_body.get("sensitive_attribute_value")
            dataset_target_attribute_details.dataset_type = event_body.get("dataset_type")
            session.add(dataset_target_attribute_details)
            commit_database_session(session)
        if sensitive_attributes is not None and len(sensitive_attributes) >= 1:
            project_dataset_details = session.query(ProjectDatasetDetails).filter(
                ProjectDatasetDetails.project_id == event_body.get("project_id"),
                ProjectDatasetDetails.dataset_path == event_body.get("dataset_path")).first()
            if project_dataset_details is not None:
                data_to_update = {"sensitive_attribute_detected": True}
                session.query(ProjectDatasetDetails).filter(
                    ProjectDatasetDetails.id == project_dataset_details.id).update(data_to_update)
                commit_database_session(session)
        return {"status_code": 201}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred in creating user"}
    finally:
        if session is not None:
            close_database_session(session)


def get_sensitive_attribute(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        session = get_database_session()
        mandatory_fields = ["project_id", "dataset_path"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        sensitive_attributes = []
        dataset_sensitive_attribute_details = session.query(DatasetSensitiveAttributeDetails).filter(
            DatasetSensitiveAttributeDetails.project_id == event_body.get(
                "project_id"), DatasetSensitiveAttributeDetails.dataset_path == event_body.get(
                "dataset_path")).all()

        if dataset_sensitive_attribute_details is not None:
            for dataset_sensitive_attribute_detail in dataset_sensitive_attribute_details:
                obj = {"id": dataset_sensitive_attribute_detail.id,
                       "dataset_path": dataset_sensitive_attribute_detail.dataset_path,
                       "sensitive_attribute": dataset_sensitive_attribute_detail.sensitive_attribute,
                       "sensitive_attribute_values": read_column_values(event_body.get("dataset_path"),
                                                                        event_body.get("project_id"),
                                                                        dataset_sensitive_attribute_detail.sensitive_attribute)}
                sensitive_attributes.append(obj)
            return {"status_code": 200, "sensitive_attributes": sensitive_attributes}
        return {"status_code": 404, "target_variable": None,
                "message": "Dataset sensitive attribute for project:  {} does not exist".format(
                    event_body.get("project_id"))}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting sensitive attributes"}
    finally:
        if session is not None:
            close_database_session(session)


def get_sensitive_attributes_data(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        session = get_database_session()
        mandatory_fields = ["project_id", "dataset_path"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        if event_body.get("project_id") is not None and event_body.get("dataset_path") is not None:
            dataset_sensitive_attribute_details = session.query(DatasetSensitiveAttributeDetails).filter(
                DatasetSensitiveAttributeDetails.project_id == event_body.get(
                    "project_id"), DatasetSensitiveAttributeDetails.dataset_path == event_body.get(
                    "dataset_path")).all()
            logging.debug(dataset_sensitive_attribute_details)
            logging.debug("-------------------------------")
            small_dataset = read_small_dataset(event_body.get("dataset_path"), event_body.get("project_id"))
            logging.debug("-------------------------------")
            logging.debug(small_dataset)
            logging.debug("-------------------------------")
            dataset_sensitive_attributes = []
            if dataset_sensitive_attribute_details is not None:
                for dataset_sensitive_attribute_detail in dataset_sensitive_attribute_details:
                    dataset_sensitive_attributes.append(dataset_sensitive_attribute_detail.sensitive_attribute)

            indexes = []
            if len(dataset_sensitive_attributes) > 0:
                for sensitive_attribute in dataset_sensitive_attributes:
                    indexes.append(small_dataset['columns'].index(sensitive_attribute))

            for index in indexes:
                del small_dataset['columns'][index]
                del small_dataset['rows'][index]
            return {"status_code": 200, "data": small_dataset}
    except Exception as ex:
        logging.debug("-----------------------------")
        logging.debug("Error is here...")
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting sensitive attributes data"}
    finally:
        if session is not None:
            close_database_session(session)


def read_small_dataset(file_name, project_id):
    try:
        s3 = boto3.resource('s3')
        hash_data = hashlib.new('sha256')
        hash_data.update(file_name.encode())
        hashed_file_name = hash_data.hexdigest()
        object_key = '{}/{}/dataset_small.json'.format(project_id, hashed_file_name)
        obj = s3.Object("kosa-metadata", object_key)
        return json.load(obj.get()['Body'])
    except Exception as ex:
        logging.error("", ex)
    return None


def get_sensitive_attributes_values(event, context):
    try:
        logging.debug(event)
        event_body = get_body(event)
        mandatory_fields = ["project_id", "dataset_path", "sensitive_attribute"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        column_values = read_column_values(event_body.get("dataset_path"), event_body.get("project_id"),
                                           event_body.get("sensitive_attribute"))
        return {"status_code": 200, "data": column_values}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting sensitive attributes values"}


def delete_sensitive_attribute(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)

        mandatory_fields = ["project_id", "dataset_path", "sensitive_attribute"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}
        session = get_database_session()
        dataset_sensitive_attribute_details = session.query(DatasetSensitiveAttributeDetails).filter(
            DatasetSensitiveAttributeDetails.project_id == event_body.get(
                "project_id"), DatasetSensitiveAttributeDetails.dataset_path == event_body.get(
                "dataset_path"), DatasetSensitiveAttributeDetails.sensitive_attribute == event_body.get(
                "sensitive_attribute")).all()
        if dataset_sensitive_attribute_details is not None and len(dataset_sensitive_attribute_details) > 0:
            for dataset_sensitive_attribute_detail in dataset_sensitive_attribute_details:
                session.delete(dataset_sensitive_attribute_detail)
                session.commit()
            return {"status_code": 200, "deleted": True}
        return {"status_code": 404, "deleted": False, "message": "Sensitive attributes not found"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while deleting sensitive attribute"}
    finally:
        if session is not None:
            close_database_session(session)


def read_column_values(file_name, project_id, variable_name):
    try:
        s3 = boto3.resource('s3')
        hash_data = hashlib.new('sha256')
        hash_data.update(file_name.encode())
        hashed_file_name = hash_data.hexdigest()
        if " " in variable_name:
            variable_name = variable_name.replace(" ", "_")
        object_key = '{}/{}/column_values/{}.json'.format(project_id, hashed_file_name, variable_name)
        obj = s3.Object("kosa-metadata", object_key)
        return json.load(obj.get()['Body'])
    except Exception as ex:
        logging.error(ex)
    return None
