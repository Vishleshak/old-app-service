import hashlib
import json
import os

import boto3
from botocore.exceptions import ClientError
from jinja2 import Environment, FileSystemLoader

from models.json_converter import AlchemyEncoder
from models.organization import Organization
from models.organization_invitation import OrganizationInvitation
from models.project_detail import ProjectDetail
from models.project_users_invitation import ProjectUsersInvitation
from models.user import User
from utils.db_util import get_database_session, commit_database_session, close_database_session
from utils.event_util import get_body, get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def invite_users_to_project(event, context):
    """This is method for invite_users_to_project

    Args:
        event ([dict]): pass the json for event
        context ([object]): pass the context

    Returns:
        [dict]: Return status code
    """
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        project_id = event_body.get("project_id")
        user_emails = event_body.get("user_emails")
        user_name = event_body.get("user_name")
        mandatory_fields = ["project_id", "user_emails", "user_name"]
        for mandatory_field in mandatory_fields:
            if event_body.get(mandatory_field) is None:
                return {"status_code": 50001,
                        "error": "Please provide {}, it should not be empty".format(mandatory_field),
                        "error_field_name": mandatory_field}

        session = get_database_session()
        project = session.query(ProjectDetail).filter(ProjectDetail.id == project_id).first()
        if not project:
            return {"status_code": 404, "message": "Project details for ID - {} does not exist".format(id)}

        for user_email in user_emails:
            project_users_invitation = ProjectUsersInvitation()
            project_users_invitation.project_id = project_id
            project_users_invitation.user_email = user_email
            session.add(project_users_invitation)
            send_email(project_users_invitation.user_email, project.project_name, user_name)

        for user_email in user_emails:
            user_details = session.query(User).filter(User.email == user_email).first()
            if not user_details:
                organization_invitation = OrganizationInvitation()
                organization_invitation.organization_id = project.organization_id
                hash_data = hashlib.new('sha256')
                hash_data.update("{}{}".format(project.organization_id, user_email).encode())
                invitation_hash = hash_data.hexdigest()
                organization_invitation.invitation_hash = invitation_hash
                organization_invitation.email = user_email
                organization_invitation.user_role_id = "DEVELOPER"
                organization_invitation.accessible_modules = "DATA_EDITOR"
                organization_invitation.is_admin = False
                organization = session.query(Organization).filter(Organization.id == project.organization_id).first()
                if organization is not None:
                    send_signup_invitation(project_users_invitation.user_email, project.project_name, user_name, invitation_hash)
                    session.add(organization_invitation)

        commit_database_session(session)
        return {"status_code": 201, "accesses": "Invites users to project successfully"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred in inviting users to project"}
    finally:
        if session is not None:
            close_database_session(session)


def get_project_invitations(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        id = path_parameter.get("id")
        session = get_database_session()
        if id is not None:
            result = session.query(ProjectUsersInvitation).filter(ProjectUsersInvitation.project_id == id).distinct(
                ProjectUsersInvitation.user_email).group_by(ProjectUsersInvitation.user_email).all()
            if not result:
                return {"status_code": 200, "invites_users": []}
            return {"status_code": 200, "invites_users": json.dumps(result, cls=AlchemyEncoder)}
        return {"status_code": 50001, "error": "Please provide project id"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting project user invitations"}
    finally:
        close_database_session(session)


def get_user_project_invites(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        user_id = event_body.get("user_id")
        session = get_database_session()
        if user_id is not None:
            user = session.query(User).filter(User.auth_id == user_id).first()
            if not user:
                return {"status_code": 404, "message": "User for organization :  {} does not exist".format(user_id)}
            result = session.query(ProjectUsersInvitation.project_id, ProjectDetail.project_name,
                                   ProjectDetail.description, ProjectUsersInvitation.id).filter(
                ProjectUsersInvitation.user_email == user.email, ProjectUsersInvitation.accepted == False,
                ProjectUsersInvitation.is_deleted == False).join(ProjectDetail,
                                                                 ProjectUsersInvitation.project_id == ProjectDetail.id).distinct()
            if not result:
                return {"status_code": 404, "message": "No any user invited for current project"}
            project_invites = []
            for obj in result:
                data = {"project_id": obj[0], "name": obj[1], "description": obj[2], "invitation_id": obj[3]}
                project_invites.append(data)
            return {"status_code": 200, "project_invites": project_invites}
        return {"status_code": 500, "error": "User Id is not provided"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting project user invitations"}
    finally:
        close_database_session(session)


def get_invitation_accepted_projects(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameters = get_path_parameters(event)
        user_id = path_parameters.get("id")
        session = get_database_session()
        if user_id is not None:
            user = session.query(User).filter(User.auth_id == user_id).first()
            if not user:
                return {"status_code": 404, "message": "User for organization :  {} does not exist".format(user_id)}
            result = session.query(ProjectUsersInvitation.project_id, ProjectDetail.project_name,
                                   ProjectDetail.description, ProjectUsersInvitation.id).filter(
                ProjectUsersInvitation.user_email == user.email, ProjectUsersInvitation.accepted == True,
                ProjectUsersInvitation.is_deleted == False).join(ProjectDetail,
                                                                 ProjectUsersInvitation.project_id == ProjectDetail.id).distinct()
            if not result:
                return {"status_code": 404, "message": "No any user invited for current project"}
            project_invites = []
            for obj in result:
                data = {"project_id": obj[0], "name": obj[1], "description": obj[2], "invitation_id": obj[3]}
                project_invites.append(data)
            return {"status_code": 200, "projects": project_invites}
        return {"status_code": 500, "error": "User Id is not provided"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting invited projects"}
    finally:
        close_database_session(session)


def accept_project_invite(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        invitation_id = event_body.get("invitation_id")
        if invitation_id is None:
            return {"status_code": 500, "error": "Invitation Id is not provided"}
        session = get_database_session()
        if invitation_id is not None:
            result = session.query(ProjectUsersInvitation).filter(ProjectUsersInvitation.id == invitation_id).first()
            if not result:
                return {"status_code": 500, "error": "You are not invited to the project"}
            else:
                data_to_update = {"accepted": True}
                session.query(ProjectUsersInvitation).filter(ProjectUsersInvitation.id == invitation_id).update(
                    data_to_update)
                commit_database_session(session)
                return {"status_code": 200, "invitation_accepted": True, "project_id": result.project_id}
        return {"status_code": 500, "error": "User Id is not provided"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while accepting project invitation"}
    finally:
        close_database_session(session)


def reject_project_invite(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        invitation_id = event_body.get("invitation_id")
        if invitation_id is None:
            return {"status_code": 500, "error": "Invitation Id is not provided"}
        session = get_database_session()
        if invitation_id is not None:
            result = session.query(ProjectUsersInvitation).filter(ProjectUsersInvitation.id == invitation_id).first()
            if not result:
                return {"status_code": 500, "error": "You are not invited to the project"}
            else:
                data_to_update = {"is_deleted": True, "is_active": False}
                session.query(ProjectUsersInvitation).filter(ProjectUsersInvitation.id == invitation_id).update(
                    data_to_update)
                commit_database_session(session)
                return {"status_code": 200, "invitation_rejected": True, "project_id": result.project_id}
        return {"status_code": 500, "error": "User Id is not provided"}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while rejecting project invitation"}
    finally:
        close_database_session(session)


def send_email(recipient, project_name, user_name):
    # If necessary, replace us-west-2 with the AWS Region you're using for Amazon SES.
    aws_region = "us-east-2"
    subject = "KOSA - New project invitation"
    env = Environment(loader=FileSystemLoader(os.path.join(os.path.dirname(__file__), "templates"), encoding="utf8"))
    filename = "project_invitation.j2"
    body_html = env.get_template(filename).render(
        USER_NAME=user_name,
        PROJECT_NAME=project_name,
        PROJECT_URL="{}.kosa.ai/#/listproject".format(os.getenv("STAGE"))
    )
    # The character encoding for the email.
    charset = "UTF-8"

    # Create a new SES resource and specify a region.
    client = boto3.client('ses', region_name=aws_region)

    # Try to send the email.
    try:
        # Provide the contents of the email.
        response = client.send_email(
            Destination={
                'ToAddresses': [
                    recipient,
                ],
            },
            Message={
                'Body': {
                    'Html': {
                        'Charset': charset,
                        'Data': body_html,
                    }
                },
                'Subject': {
                    'Charset': charset,
                    'Data': subject,
                },
            },
            Source="team@kosa.ai"
        )
    # Display an error if something goes wrong.
    except Exception as e:
        import traceback
        logging.error(e)
        # logger.error(e.response['Error']['Message'])
    else:
        logging.info("Email sent! Message ID:"),
        logging.debug(response['MessageId'])


def delete_project_invite(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameters = get_path_parameters(event)
        invitation_id = path_parameters.get("id")
        if invitation_id is None:
            return {"status_code": 50001, "error": "Invitation Id is not provided"}
        session = get_database_session()
        if invitation_id is not None:
            result = session.query(ProjectUsersInvitation).filter(ProjectUsersInvitation.id == invitation_id).first()
            if not result:
                return {"status_code": 404, "error": "No invitation found"}
            else:
                data_to_update = {}
                data_to_update["is_deleted"] = True
                session.query(ProjectUsersInvitation).filter(ProjectUsersInvitation.id == invitation_id).update(
                    data_to_update)
                commit_database_session(session)
                return {"status_code": 200, "invitation_deleted": True}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while deleting project invitation"}
    finally:
        close_database_session(session)


def send_signup_invitation(recipient, organization_name, user_name, invitation_hash):
    # If necessary, replace us-west-2 with the AWS Region you're using for Amazon SES.
    AWS_REGION = "us-east-2"
    # The subject line for the email.
    SUBJECT = "KOSA :: Organization Invite"

    # The email body for recipients with non-HTML email clients.
    BODY_TEXT = ("KOSA :: Project Invite\r\n"
                 "You have invited by {} to the organization. \r\n "
                 "To create account, please visit - https://{}.kosa.ai/#/signup?invite={}".format(
        user_name, organization_name, os.getenv("STAGE"), invitation_hash))

    # The HTML body of the email.
    BODY_HTML = """<html>
                    <head></head>
                    <body>
                    <h1>KOSA :: Organization Invite</h1>
                    <p>You have invited by {} to the organization - {} created on KOSA</p>
                    <p>To create account, please visit - <a href="https://{}.kosa.ai/#/signup?invite={}">
                    KOSA</a></p>
                    </body>
                    </html>""".format(user_name, organization_name, os.getenv("STAGE"), invitation_hash)

    # The character encoding for the email.
    CHARSET = "UTF-8"

    # Create a new SES resource and specify a region.
    client = boto3.client('ses', region_name=AWS_REGION)

    # Try to send the email.
    try:
        # Provide the contents of the email.
        response = client.send_email(
            Destination={
                'ToAddresses': [
                    recipient,
                ],
            },
            Message={
                'Body': {
                    'Html': {
                        'Charset': CHARSET,
                        'Data': BODY_HTML,
                    },
                    'Text': {
                        'Charset': CHARSET,
                        'Data': BODY_TEXT,
                    },
                },
                'Subject': {
                    'Charset': CHARSET,
                    'Data': SUBJECT,
                },
            },
            Source="team@kosa.ai"
        )
    # Display an error if something goes wrong.
    except ClientError as e:
        logging.error(e)
    else:
        logging.info("Email sent! Message ID:"),
        logging.debug(response['MessageId'])
