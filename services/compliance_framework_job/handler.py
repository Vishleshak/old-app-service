import json

from models.compliance_framework import ComplianceFramework
from models.compliance_framework_country import ComplianceFrameworkCountry
from models.compliance_framework_industry import ComplianceFrameworkIndustry
from models.compliance_framework_region import ComplianceFrameworkRegion
from models.compliance_framework_rule import ComplianceFrameworkRule
from models.compliance_framework_use_case import ComplianceFrameworkUseCase
from models.compliance_job import ComplianceJob
from models.compliance_job_framework import ComplianceJobFramework
from models.compliance_job_rule import ComplianceJobRule
from models.json_converter import AlchemyEncoder
from models.project_detail import ProjectDetail
from utils.db_util import get_database_session, commit_database_session, close_database_session
from utils.event_util import get_body, get_path_parameters
from utils.log_util import get_logger

logging = get_logger(__name__)


def add_compliance_job(event, context):
    session = None
    try:
        logging.debug(event)
        event_body = get_body(event)
        project_id = event_body.get("project_id")
        if project_id is None:
            return {"status_code": 500, "message": "Field project_id is mandatory"}
        session = get_database_session()
        compliance_job = ComplianceJob()
        compliance_job.project_id = project_id
        session.add(compliance_job)
        commit_database_session(session)

        project_details = session.query(ProjectDetail).filter(ProjectDetail.id == project_id).first()
        if not project_details:
            return {"status_code": 404, "message": "Project Detail {} does not exist".format(project_id)}

        compliance_frameworks = []
        industrywise_compliance_frameworks = session.query(ComplianceFrameworkIndustry).filter(
            ComplianceFrameworkIndustry.industry_id == project_details.industry).all()
        for industrywise_compliance_framework in industrywise_compliance_frameworks:
            compliance_frameworks.append(industrywise_compliance_framework.compliance_framework_id)

        regionwise_compliance_frameworks = session.query(ComplianceFrameworkRegion).filter(
            ComplianceFrameworkRegion.region_name == project_details.region).all()
        for regionwise_compliance_framework in regionwise_compliance_frameworks:
            compliance_frameworks.append(regionwise_compliance_framework.compliance_framework_id)

        countrywise_compliance_frameworks = session.query(ComplianceFrameworkCountry).filter(
            ComplianceFrameworkCountry.country_name == project_details.country).all()
        for countrywise_compliance_framework in countrywise_compliance_frameworks:
            compliance_frameworks.append(countrywise_compliance_framework.compliance_framework_id)

        usecasewise_compliance_frameworks = session.query(ComplianceFrameworkUseCase).filter(
            ComplianceFrameworkUseCase.use_case_id == project_details.use_case).all()
        for usecasewise_compliance_framework in usecasewise_compliance_frameworks:
            compliance_frameworks.append(usecasewise_compliance_framework.compliance_framework_id)

        for compliance_framework in compliance_frameworks:
            compliance_job_and_framework = ComplianceJobFramework()
            compliance_job_and_framework.compliance_framework_id = compliance_framework
            compliance_job_and_framework.compliance_job_id = compliance_job.id
            session.add(compliance_job_and_framework)
            commit_database_session(session)

        return {"status_code": 201, "compliance_job": json.dumps(compliance_job, cls=AlchemyEncoder)}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while adding compliance job"}
    finally:
        if session is not None:
            close_database_session(session)


def get_compliance_job_details(event, context):
    session = None
    try:
        logging.debug(event)
        path_parameter = get_path_parameters(event)
        project_id = path_parameter.get("project_id")
        if project_id is None:
            return {"status_code": 500, "message": "Field project_id is mandatory"}

        session = get_database_session()
        compliance_job = session.query(ComplianceJob).filter(ComplianceJob.project_id == project_id).first()
        if not compliance_job:
            compliance_job = add_new_compliance_job(project_id)

        compliance_job_frameworks = session.query(ComplianceJobFramework).filter(
            ComplianceJobFramework.compliance_job_id == compliance_job.id).all()
        result = []
        compliance_frameworks_result = []
        for compliance_job_framework in compliance_job_frameworks:
            framework_rule_data = {}
            compliance_framework = session.query(ComplianceFramework).filter(
                ComplianceFramework.id == compliance_job_framework.compliance_framework_id).first()
            compliance_framework_details = {}
            compliance_framework_details["compliance_framework_id"] = compliance_framework.id
            compliance_framework_details["compliance_framework_name"] = compliance_framework.name
            compliance_framework_details["compliance_framework_description"] = compliance_framework.description
            compliance_frameworks_result.append(compliance_framework_details)
            compliance_job_rules = session.query(ComplianceFrameworkRule.id, ComplianceFrameworkRule.title,
                                                 ComplianceJobRule.compliance_rule_id, ComplianceJobRule.status,
                                                 ComplianceJobRule.assigned_to_user, ComplianceJobRule.id,
                                                 ComplianceFrameworkRule.section_name).join(ComplianceJobRule,
                                                                                            ComplianceJobRule.compliance_rule_id == ComplianceFrameworkRule.id,
                                                                                            isouter=True).filter(
                ComplianceFrameworkRule.compliance_framework_id == compliance_job_framework.compliance_framework_id).all()
            rules = []
            for compliance_job_rule in compliance_job_rules:
                details = {}
                details["id"] = compliance_job_rule[0]
                details["title"] = compliance_job_rule[1]
                details["compliance_rule_id"] = compliance_job_rule[2]
                details["status"] = compliance_job_rule[3]
                details["assigned_to_user"] = compliance_job_rule[4]
                details["compliance_job_rule_id"] = compliance_job_rule[5]
                details["section_name"] = compliance_job_rule[6]
                rules.append(details)
            rules_by_section = {}
            for rule in rules:
                if rules_by_section.get(rule.get("section_name")) is not None:
                    rules_list = rules_by_section.get(rule.get("section_name"))
                    rules_list.append(rule)
                    rules_by_section[rule.get("section_name")] = rules_list
                else:
                    rules_by_section[rule.get("section_name")] = [rule]
            final_rules_data_list = []
            for key in rules_by_section.keys():
                final_rules_data = {}
                final_rules_data["section_name"] = key
                final_rules_data["section_rules"] = rules_by_section.get(key)
                final_rules_data["compliance_framework_id"] = compliance_framework.id
                final_rules_data["compliance_framework_name"] = compliance_framework.name
                final_rules_data["compliance_framework_description"] = compliance_framework.description
                final_rules_data_list.append(final_rules_data)
            # framework_rule_data["rules"] = final_rules_data_list
            result.append(final_rules_data_list)
        return {"compliance_frameworks": compliance_frameworks_result, "rules": result}
    except Exception as ex:
        logging.error(ex)
        return {"status_code": 500, "error": "Error occurred while getting compliance job details"}
    finally:
        if session is not None:
            close_database_session(session)


def add_new_compliance_job(project_id):
    try:
        session = get_database_session()
        compliance_job = ComplianceJob()
        compliance_job.project_id = project_id
        session.add(compliance_job)
        commit_database_session(session)

        project_details = session.query(ProjectDetail).filter(ProjectDetail.id == project_id).first()
        if not project_details:
            return {"status_code": 404, "message": "Project Detail {} does not exist".format(project_id)}

        compliance_frameworks = []
        industrywise_compliance_frameworks = session.query(ComplianceFrameworkIndustry).filter(
            ComplianceFrameworkIndustry.industry_id == project_details.industry).all()
        for industrywise_compliance_framework in industrywise_compliance_frameworks:
            compliance_frameworks.append(industrywise_compliance_framework.compliance_framework_id)

        regionwise_compliance_frameworks = session.query(ComplianceFrameworkRegion).filter(
            ComplianceFrameworkRegion.region_name == project_details.region).all()
        for regionwise_compliance_framework in regionwise_compliance_frameworks:
            compliance_frameworks.append(regionwise_compliance_framework.compliance_framework_id)

        countrywise_compliance_frameworks = session.query(ComplianceFrameworkCountry).filter(
            ComplianceFrameworkCountry.country_name == project_details.country).all()
        for countrywise_compliance_framework in countrywise_compliance_frameworks:
            compliance_frameworks.append(countrywise_compliance_framework.compliance_framework_id)

        usecasewise_compliance_frameworks = session.query(ComplianceFrameworkUseCase).filter(
            ComplianceFrameworkUseCase.use_case_id == project_details.use_case).all()
        for usecasewise_compliance_framework in usecasewise_compliance_frameworks:
            compliance_frameworks.append(usecasewise_compliance_framework.compliance_framework_id)

        for compliance_framework in compliance_frameworks:
            compliance_job_and_framework = ComplianceJobFramework()
            compliance_job_and_framework.compliance_framework_id = compliance_framework
            compliance_job_and_framework.compliance_job_id = compliance_job.id
            session.add(compliance_job_and_framework)
            commit_database_session(session)
        return compliance_job
    except Exception as ex:
        logging.error(ex)
    finally:
        if session is not None:
            close_database_session(session)
